<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFakultasJurusan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

   

    public function up()
    {
        Schema::create('fakultas', function (Blueprint $table) {
            $table->unsignedInteger('id_fakultas');
            $table->primary('id_fakultas');
            $table->string('kode_fakultas', 3);
            $table->string('nama', 150);
            $table->string('ketua', 50)->nullable();
            $table->text('photo')->nullable();
            $table->enum('status', ['aktif', 'tidak aktif'])->default('aktif');
            $table->string('format_surat', 8)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('jurusan', function(Blueprint $table){
            $table->unsignedInteger('id_jurusan');
            $table->primary('id_jurusan');
            $table->string('kode_jurusan', 5);
            $table->string('nama', 150);
            $table->string('ketua', 50)->nullable();
            $table->text('photo')->nullable();
            $table->enum('program', ['D1', 'D2', 'D3', 'D4', 'S1', 'S2', 'S3']);
            $table->enum('status', ['aktif', 'tidak aktif'])->default('aktif');
            $table->string('id_feeder_jurusan', 6)->nullable();
            $table->unsignedInteger('id_fakultas');
            $table->foreign('id_fakultas')->references('id_fakultas')->on('fakultas')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jurusan');
        Schema::dropIfExists('fakultas');
    }
}
