<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerusahaanLowongan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

   

    public function up()
    {
        Schema::create('perusahaan', function (Blueprint $table) {
            $table->increments('id_perusahaan');
            $table->string('image', 255)->nullable();
            $table->string('name', 255);
            $table->text('description');
            $table->timestamps();
        });

        Schema::create('lowongan', function (Blueprint $table) {
            $table->increments('id_lowongan');
            $table->string('jenjang', 255);
            $table->string('jenis_pekerjaan', 255);
            $table->string('penempatan', 255);
            $table->text('kualifikasi');
            $table->date('tgl_buka');
            $table->date('tgl_tutup');
            $table->timestamps();
        });

        Schema::create('lowongan_jurusan_pivot', function(Blueprint $table) {
            $table->integer('id_lowongan')->unsigned()->index();
            $table->foreign('id_lowongan')->references('id_lowongan')->on('lowongan')->onDelete('cascade');

            $table->integer('id_jurusan')->unsigned()->index();
            $table->foreign('id_jurusan')->references('id_jurusan')->on('jurusan')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lowongan_jurusan_pivot');
        Schema::dropIfExists('lowongan');
        Schema::dropIfExists('perusahaan');
    }
}
