-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 22, 2017 at 01:55 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cdc`
--

-- --------------------------------------------------------

--
-- Table structure for table `alumni`
--

CREATE TABLE `alumni` (
  `id_alumni` int(11) NOT NULL,
  `nim` varchar(10) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama_lengkap` varchar(255) NOT NULL,
  `tempat_lahir` varchar(255) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jk` varchar(10) NOT NULL,
  `agama` varchar(10) NOT NULL,
  `no_telp` varchar(14) NOT NULL,
  `no_selular` varchar(14) NOT NULL,
  `email` varchar(255) NOT NULL,
  `alamat1` text NOT NULL,
  `alamat2` text NOT NULL,
  `id_jurusan` int(11) NOT NULL,
  `tingkat` varchar(255) NOT NULL,
  `tahun_masuk_kuliah` year(4) NOT NULL,
  `tgl_lulus` date NOT NULL,
  `ipk` varchar(4) NOT NULL,
  `masa_tunggu_kerja` int(11) NOT NULL,
  `is_active` enum('need confirm','confirm','','') NOT NULL DEFAULT 'need confirm',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alumni`
--

INSERT INTO `alumni` (`id_alumni`, `nim`, `username`, `password`, `nama_lengkap`, `tempat_lahir`, `tgl_lahir`, `jk`, `agama`, `no_telp`, `no_selular`, `email`, `alamat1`, `alamat2`, `id_jurusan`, `tingkat`, `tahun_masuk_kuliah`, `tgl_lulus`, `ipk`, `masa_tunggu_kerja`, `is_active`, `created_at`, `updated_at`) VALUES
(6, '111111', 'putuanton', '$2y$10$w0dU4MTURjYHD.1jiAOC3OK.kIgRd3ecmCiPLHQBPgTz9Rbd6Poam', 'putu anton', 'bali', '2017-12-25', 'perempuan', 'islam', '111111', '11111', 'eka.parianthana@yahoo.com', 'sdsdsd', 'sfsf', 4301, 'SLTA/SMEA/SMK', 2017, '2017-12-07', '2', 33, 'need confirm', '2017-12-17 07:36:28', '2017-12-17 07:36:28'),
(8, '121212', 'eka12345', '$2y$10$gRbBARnh/xVb4L4lg/hR0ua/pUaX0wQwFyE6Fe4NRmEWKH/CyfHNS', 'subagia', 'wds', '2018-01-01', 'laki-laki', 'budha', '234', '23424', 'sdsdfff@gmail.com', '23234sdg dd', 'sdgf dd', 4301, 'SLTA/SMEA/SMK', 2017, '2017-12-07', '3', 11, 'need confirm', '2017-12-17 07:44:34', '2017-12-17 07:44:34'),
(12, '1315051043', 'adisparta', 'adisparta123', 'I Wayan Adi Sparta', 'Padangtegal', '1994-06-21', 'laki-laki', 'hindu', '085737109711', '23424345', 'adisparta@gmail.com', '23234sdg dd', 'sdf', 4301, 'Sarjana', 2018, '2017-12-19', '3.58', 2, 'confirm', '2017-12-19 17:30:47', '2017-12-21 06:48:05'),
(13, '1315051044', 'nyoman', 'nyoman', 'Nyoman', 'ds', '2017-12-26', 'laki-laki', 'hindu', '34', '345', 'eksdfea@gmail.com', 'Jembawan', 'Singaraja', 1503, 'Diploma', 2015, '2017-12-21', '3', 3, 'confirm', '2017-12-21 09:02:33', '2017-12-21 09:14:53'),
(14, '1315051045', 'rini', 'aaaaaaa', 'rini', 'ddd', '2017-11-27', 'laki-laki', 'hindu', '234', '234523', 'eka.ppp@gmail.com', 'did', 'dfgdfg', 1503, 'SLTA/SMEA/SMK', 2014, '2017-12-13', '3', 2, 'confirm', '2017-12-21 09:18:19', '2017-12-21 09:20:36'),
(16, '111144444', 'ketut', '$2y$10$TjvewJ5L3ENXVMw6LDgkl..cGZTucLELcGA0qnelJHYBRdxxfi3/G', 'ketut', 'add', '2017-12-18', 'laki-laki', 'hindu', '234', '234', 'dsf@sdf.com', 'sag', 'dog', 1501, 'Diploma', 2014, '2017-12-26', '3', 3, 'confirm', '2017-12-21 09:25:09', '2017-12-21 09:27:22'),
(17, '111144443', 'ekap123', '$2y$10$0o6opyxo6PUJvn8EwR0cVu/mRy.zp.lFQ19yq6rFp0TcasmmNE0Iy', 'okapi', 'ee', '2017-12-19', 'laki-laki', 'hindu', '555', '555', 'eka@gmai.com', 'ddd', 'ddd', 5502, 'Sarjana', 2015, '2017-12-20', '3', 2, 'confirm', '2017-12-21 17:45:25', '2017-12-21 17:46:07');

-- --------------------------------------------------------

--
-- Table structure for table `fakultas`
--

CREATE TABLE `fakultas` (
  `id_fakultas` int(10) UNSIGNED NOT NULL,
  `kode_fakultas` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ketua` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` text COLLATE utf8mb4_unicode_ci,
  `status` enum('aktif','tidak aktif') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'aktif',
  `format_surat` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fakultas`
--

INSERT INTO `fakultas` (`id_fakultas`, `kode_fakultas`, `nama`, `ketua`, `photo`, `status`, `format_surat`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '01', 'Fakultas Ilmu Pendidikan', NULL, NULL, 'aktif', NULL, NULL, NULL, NULL),
(2, '02', 'Fakultas Bahasa dan Seni', NULL, NULL, 'aktif', NULL, NULL, NULL, NULL),
(3, '03', 'Fakultas Matematika dan Ilmu Pengetahuan Alam', NULL, NULL, 'aktif', NULL, NULL, NULL, NULL),
(4, '04', 'Fakultas Hukum dan Ilmu Sosial', NULL, NULL, 'aktif', NULL, NULL, NULL, NULL),
(5, '05', 'Fakultas Teknik dan Kejuruan', NULL, NULL, 'aktif', NULL, NULL, NULL, NULL),
(6, '06', 'Fakultas Olahraga dan Kesehatan', NULL, NULL, 'aktif', NULL, NULL, NULL, NULL),
(7, '07', 'Pascasarjana', NULL, NULL, 'aktif', NULL, NULL, NULL, NULL),
(8, '08', 'Fakultas Ekonomi', NULL, NULL, 'aktif', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `image_slider`
--

CREATE TABLE `image_slider` (
  `id_image_slider` int(11) NOT NULL,
  `image` varchar(500) NOT NULL,
  `title` varchar(500) NOT NULL,
  `subtitle` varchar(500) NOT NULL,
  `theme_color` varchar(7) NOT NULL,
  `publish` enum('Yes','No') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `image_slider`
--

INSERT INTO `image_slider` (`id_image_slider`, `image`, `title`, `subtitle`, `theme_color`, `publish`, `created_at`, `updated_at`) VALUES
(2, 'undiksha-at-cdc-undiksha-xhu.jpeg', 'Undiksha', 'Universitas Pendidikan Ganesha', '#3493af', 'Yes', '2017-12-19 21:27:50', '2017-12-19 21:27:50');

-- --------------------------------------------------------

--
-- Table structure for table `informasi`
--

CREATE TABLE `informasi` (
  `id_informasi` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `id_kategori` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `informasi`
--

INSERT INTO `informasi` (`id_informasi`, `title`, `description`, `slug`, `image`, `user_id`, `id_kategori`, `created_at`, `updated_at`) VALUES
(1, 'Pre Job Training Memasuki Dunia Kerja 25 OKTOBER 2017', 'Siapkah anda menghadapi tantangan dalam bekerja? Mungkin merupakan pertanyaan yang banyak dihadapi oleh para alumni universitas. Seiring dengan perkembangan dunia kerja, tantangan kerja pun makin berkembang. Untuk mempersiapkan mahasiswa Universitas Negeri Yogyakarta (UNY) dalam berkarir, Pusat Pengembangan Karir LPPMP mengadakan Pre Job Training bertajuk “Siapkah Anda Memulai Karir” bagi Alumni dan Mahasiswa Tingkat Akhir UNY pada: eka<br>', 'pre-job-training-memasuki-dunia-kerja-25-oktober-2017', 'pre-job-training-memasuki-dunia-kerja-25-oktober-2017-at-cdc-undiksha-L5c.png', 1, 'Job Seeker', '2017-12-04 21:06:31', '2017-12-13 20:47:29'),
(4, 'UNY CAREER DAYS & JOB FAIR 2017', '<p>ini diperl<br></p>', 'uny-career-days-job-fair-2017', NULL, 1, 'Tips Karier', '2017-12-13 20:33:55', '2017-12-13 20:47:43');

-- --------------------------------------------------------

--
-- Table structure for table `jurusan`
--

CREATE TABLE `jurusan` (
  `id_jurusan` int(10) UNSIGNED NOT NULL,
  `kode_jurusan` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ketua` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` text COLLATE utf8mb4_unicode_ci,
  `program` enum('D1','D2','D3','D4','S1','S2','S3') COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('aktif','tidak aktif') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'aktif',
  `id_feeder_jurusan` varchar(6) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_fakultas` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jurusan`
--

INSERT INTO `jurusan` (`id_jurusan`, `kode_jurusan`, `nama`, `ketua`, `photo`, `program`, `status`, `id_feeder_jurusan`, `id_fakultas`, `deleted_at`, `created_at`, `updated_at`) VALUES
(2, '', 'PSKGJ Pendidikan Guru Sekolah Dasar (S1)', NULL, NULL, 'D1', 'aktif', '07', 7, NULL, NULL, NULL),
(1501, '01501', 'Bimbingan Konseling (S1)', NULL, NULL, 'D1', 'aktif', '01', 1, NULL, NULL, NULL),
(1502, '01502', 'Teknologi Pendidikan (S1)', NULL, NULL, 'D1', 'aktif', '01', 1, NULL, NULL, NULL),
(1503, '01503', 'Pendidikan Guru SD (S1)', NULL, NULL, 'D1', 'aktif', '01', 1, NULL, NULL, NULL),
(1504, '01504', 'Pendidikan Guru PAUD (S1)', NULL, NULL, 'D1', 'aktif', '01', 1, NULL, NULL, NULL),
(2301, '02301', 'Bahasa Inggris (D3)', NULL, NULL, 'D1', 'aktif', '02', 2, NULL, NULL, NULL),
(2307, '02307', 'Desain Komunikasi Visual (D3)', NULL, NULL, 'D1', 'aktif', '02', 2, NULL, NULL, NULL),
(2501, '02501', 'Pendidikan Bahasa dan Sastra Indonesia (S1)', NULL, NULL, 'D1', 'aktif', '02', 2, NULL, NULL, NULL),
(2502, '02502', 'Pendidikan Bahasa Inggris (S1)', NULL, NULL, 'D1', 'aktif', '02', 2, NULL, NULL, NULL),
(2503, '02503', 'Pendidikan Seni Rupa (S1)', NULL, NULL, 'D1', 'aktif', '02', 2, NULL, NULL, NULL),
(2504, '02504', 'Pendidikan Bahasa Bali (S1)', NULL, NULL, 'D1', 'aktif', '02', 2, NULL, NULL, NULL),
(2505, '02505', 'Pendidikan Bahasa Jepang (S1)', NULL, NULL, 'D1', 'aktif', '02', 2, NULL, NULL, NULL),
(3301, '03301', 'Analis Kimia (D3)', NULL, NULL, 'D1', 'aktif', '03', 3, NULL, NULL, NULL),
(3302, '03302', 'Budidaya Kelautan (D3)', NULL, NULL, 'D1', 'aktif', '03', 3, NULL, NULL, NULL),
(3501, '03501', 'Pendidikan Matematika (S1)', NULL, NULL, 'D1', 'aktif', '03', 3, NULL, NULL, NULL),
(3502, '03502', 'Pendidikan Fisika (S1)', NULL, NULL, 'D1', 'aktif', '03', 3, NULL, NULL, NULL),
(3503, '03503', 'Pendidikan Kimia (S1)', NULL, NULL, 'D1', 'aktif', '03', 3, NULL, NULL, NULL),
(3504, '03504', 'Pendidikan Biologi (S1)', NULL, NULL, 'D1', 'aktif', '03', 3, NULL, NULL, NULL),
(3505, '03505', 'Kimia (S1)', NULL, NULL, 'D1', 'aktif', '03', 3, NULL, NULL, NULL),
(3506, '03506', 'Biologi (S1)', NULL, NULL, 'D1', 'aktif', '03', 3, NULL, NULL, NULL),
(3507, '03507', 'Matematika (S1)', NULL, NULL, 'D1', 'aktif', '03', 3, NULL, NULL, NULL),
(3508, '03508', 'Akuakultur (S1)', NULL, NULL, 'D1', 'aktif', '03', 3, NULL, NULL, NULL),
(3510, '03510', 'Pendidikan IPA (S1)', NULL, NULL, 'D1', 'aktif', '03', 3, NULL, NULL, NULL),
(4301, '04301', 'Perpustakaan (D3)', NULL, NULL, 'D1', 'aktif', '04', 4, NULL, NULL, NULL),
(4302, '04302', 'Survey dan Pemetaan (D3)', NULL, NULL, 'D1', 'aktif', '04', 4, NULL, NULL, NULL),
(4502, '04502', 'Pendidikan Sejarah (S1)', NULL, NULL, 'D1', 'aktif', '04', 4, NULL, NULL, NULL),
(4503, '04503', 'Pendidikan Geografi (S1)', NULL, NULL, 'D1', 'aktif', '04', 4, NULL, NULL, NULL),
(4504, '04504', 'Pendidikan Kewarganegaraan (S1)', NULL, NULL, 'D1', 'aktif', '04', 4, NULL, NULL, NULL),
(4505, '04505', 'Pendidikan Sosiologi (S1)', NULL, NULL, 'D1', 'aktif', '04', 4, NULL, NULL, NULL),
(4510, '04510', 'Ilmu Hukum (S1)', NULL, NULL, 'D1', 'aktif', '04', 4, NULL, NULL, NULL),
(5301, '05301', 'Manajemen Informatika (D3)', NULL, NULL, 'D1', 'aktif', '05', 5, NULL, NULL, NULL),
(5302, '05302', 'Teknik Elektronika (D3)', NULL, NULL, 'D1', 'aktif', '05', 5, NULL, NULL, NULL),
(5501, '05501', 'Pendidikan Kesejahteraan Keluarga (S1)', NULL, NULL, 'D1', 'aktif', '05', 5, NULL, NULL, NULL),
(5502, '05502', 'Pendidikan Teknik Informatika (S1)', NULL, NULL, 'D1', 'aktif', '05', 5, NULL, NULL, NULL),
(5503, '05503', 'Pendidikan Teknik Elektro (S1)', NULL, NULL, 'D1', 'aktif', '05', 5, NULL, NULL, NULL),
(5504, '05504', 'Pendidikan Teknik Mesin (S1)', NULL, NULL, 'D1', 'aktif', '05', 5, NULL, NULL, NULL),
(6301, '06301', 'Pelatihan Olahraga Pariwisata (D3)', NULL, NULL, 'D1', 'aktif', '06', 6, NULL, NULL, NULL),
(6501, '06501', 'Pendidikan Jasmani Kesehatan dan Rekreasi (S1) ', NULL, NULL, 'D1', 'aktif', '06', 6, NULL, NULL, NULL),
(6502, '06502', 'Pendidikan Kepelatihan Olahraga (S1)', NULL, NULL, 'D1', 'aktif', '06', 6, NULL, NULL, NULL),
(6503, '06503', 'Ilmu Keolahragaan (S1)', NULL, NULL, 'D1', 'aktif', '06', 6, NULL, NULL, NULL),
(7601, '07601', 'Penelitian dan Evaluasi Pendidikan', NULL, NULL, 'D1', 'aktif', '07', 7, NULL, NULL, NULL),
(7602, '07602', 'Pendidikan Bahasa (S3)', NULL, NULL, 'D1', 'aktif', '07', 7, NULL, NULL, NULL),
(7603, '07603', 'Administrasi Pendidikan', NULL, NULL, 'D1', 'aktif', '07', 7, NULL, NULL, NULL),
(7604, '07604', 'Pendidikan Dasar', NULL, NULL, 'D1', 'aktif', '07', 7, NULL, NULL, NULL),
(7605, '07605', 'Pendidikan IPA', NULL, NULL, 'D1', 'aktif', '07', 7, NULL, NULL, NULL),
(7606, '07606', 'Pendidikan Matematika', NULL, NULL, 'D1', 'aktif', '07', 7, NULL, NULL, NULL),
(7607, '07607', 'Teknologi Pembelajaran', NULL, NULL, 'D1', 'aktif', '07', 7, NULL, NULL, NULL),
(7608, '07608', 'Pendidikan Bahasa Inggris (S2)', NULL, NULL, 'D1', 'aktif', '07', 7, NULL, NULL, NULL),
(7609, '07609', 'Pendidikan Ilmu Pengetahuan Sosial (S2)', NULL, NULL, 'D1', 'aktif', '07', 7, NULL, NULL, NULL),
(7610, '07610', 'Ilmu Komputer (S2)', NULL, NULL, 'D1', 'aktif', '07', 7, NULL, NULL, NULL),
(7611, '07611', 'Bimbingan Konseling (S2)', NULL, NULL, 'D1', 'aktif', '07', 7, NULL, NULL, NULL),
(7612, '07612', 'Pendidikan Olahraga (S2)', NULL, NULL, 'D1', 'aktif', '07', 7, NULL, NULL, NULL),
(7714, '07714', 'Pendidikan Dasar (S3)', NULL, NULL, 'D1', 'aktif', '07', 7, NULL, NULL, NULL),
(7715, '07715', 'Ilmu Pendidikan (S3)', NULL, NULL, 'D1', 'aktif', '07', 7, NULL, NULL, NULL),
(8301, '08301', 'Akuntansi (D3)', NULL, NULL, 'D1', 'aktif', '08', 8, NULL, NULL, NULL),
(8302, '08302', 'Perhotelan (D3)', NULL, NULL, 'D1', 'aktif', '08', 8, NULL, NULL, NULL),
(8501, '08501', 'Pendidikan EKonomi (S1)', NULL, NULL, 'D1', 'aktif', '08', 8, NULL, NULL, NULL),
(8502, '08502', 'Manajemen (S1)', NULL, NULL, 'D1', 'aktif', '08', 8, NULL, NULL, NULL),
(8503, '08503', 'Akuntansi (S1)', NULL, NULL, 'D1', 'aktif', '08', 8, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(500) NOT NULL,
  `keterangan` text,
  `slug` varchar(500) NOT NULL,
  `jenis` enum('informasi','lsp') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nama_kategori`, `keterangan`, `slug`, `jenis`, `created_at`, `updated_at`) VALUES
(2, 'Teknologi', NULL, 'teknologi', 'lsp', '2017-12-13 19:51:12', '2017-12-13 19:51:12'),
(3, 'Job Seeker', NULL, 'job-seeker', 'informasi', '2017-12-13 20:00:29', '2017-12-13 20:00:29'),
(4, 'Tips Karier', NULL, 'tips-karier', 'informasi', '2017-12-13 20:00:40', '2017-12-13 20:00:40'),
(5, 'Keguruan', NULL, 'keguruan', 'lsp', '2017-12-13 20:02:36', '2017-12-13 20:02:36');

-- --------------------------------------------------------

--
-- Table structure for table `kontak`
--

CREATE TABLE `kontak` (
  `id_kontak` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `alamat` text NOT NULL,
  `tentang_kami` text,
  `no_rek` varchar(255) DEFAULT NULL,
  `nama_bank` varchar(100) DEFAULT NULL,
  `fb` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  `gplus` varchar(255) DEFAULT NULL,
  `latitude` varchar(100) DEFAULT NULL,
  `longitude` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kontak`
--

INSERT INTO `kontak` (`id_kontak`, `email`, `no_telp`, `alamat`, `tentang_kami`, `no_rek`, `nama_bank`, `fb`, `twitter`, `gplus`, `latitude`, `longitude`, `created_at`, `updated_at`) VALUES
(1, 'ask@bluecreative.co.id', '085737109711', 'Singaraja, Banyuasri, Kec. Buleleng, Kabupaten Buleleng, Bali, Indonesia', 'Kami adalah Unit yang bertugas untuk mengembakan karier alumni dan sebagai penyedia sertifikasi profisi di Undiksha ss', '122334343', 'BCA', 'https://www.facebook.com/ekaparianthana', 'https://www.twitter.com/', 'https://www.googleplus.com/', '-8.117066767023335', '115.08823032275393', '2017-12-13 09:12:00', '2017-12-19 22:18:22');

-- --------------------------------------------------------

--
-- Table structure for table `lowongan`
--

CREATE TABLE `lowongan` (
  `id_lowongan` int(10) UNSIGNED NOT NULL,
  `nama_lowongan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_jurusan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenjang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_pekerjaan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `penempatan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `posisi_tersedia` int(11) NOT NULL,
  `kualifikasi` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_buka` date NOT NULL,
  `tgl_tutup` date NOT NULL,
  `files` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_perusahaan` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lowongan`
--

INSERT INTO `lowongan` (`id_lowongan`, `nama_lowongan`, `slug`, `nama_jurusan`, `jenjang`, `jenis_pekerjaan`, `penempatan`, `posisi_tersedia`, `kualifikasi`, `tgl_buka`, `tgl_tutup`, `files`, `id_perusahaan`, `created_at`, `updated_at`) VALUES
(1, 'Project Manager ERP System', 'project-manager-erp-system', 'Semua Jurusan', 'Doktor', 'Tetap', 'Seluruh Indonesia', 1, '<p>     Dibutuhkan Segera :    <br>OB / Office Girl</p><p>Persyaratan:<br>        Wanita<br>        Pendidikan min SMP<br>        Berpenampilan menarik, Supel, dan ramah<br>        Mampu bekerjasama dalam tim<br>        Memiliki sikap, kepribadian dan etos kerja yang baik<br>    Kirimkan lamaran dan CV lengkap Anda paling lambat tanggal 18 Desember 2017 ke:<br>    OZEVA SCHOOL<br>    Jl. Kapten Mulyadi 52A, Solo 57121<br>    atau berkas dikirim dalam bentuk PDF melalui email ke:<br>        hr@ozeva.com<br>    Tuliskan Nama_posisi yang anda lamar sebagai subject email (Contoh: Nita_Guru Matematika) File lampiran maks 500Kb<br>    untuk informasi lebih lanjut silahkan kunjungi: http://ozevaschool.com<br></p><p></p><p></p>', '2017-12-13', '2017-12-27', NULL, 1, '2017-12-13 08:11:29', '2017-12-19 18:18:13'),
(2, 'Programmer Laravel', 'programmer-laravel', 'Pendidikan Teknik Informatika', 'Sarjana', 'Tetap', 'Seluruh Indonesia', 0, '<p><img style=\"width: 222px;\" src=\"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/4QBgRXhpZgAASUkqAAgAAAACADEBAgAHAAAAJgAAAGmHBAABAAAALgAAAAAAAABHb29nbGUAAAMAAJAHAAQAAAAwMjIwAqAEAAEAAADeAAAAA6AEAAEAAADjAAAAAAAAAP/bAIQACQYHFBISFRQTFBQVFBUXFxYWGBgWFxYVGhcUFxkYFhwWFRgcKCAYGiUfFxghMSElKSsuLi4YHzM4NSw3KC0uKwEKCgoODQ4bEBAbLCQgJCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCws/8AAEQgA4wDeAwERAAIRAQMRAf/EABwAAAEFAQEBAAAAAAAAAAAAAAADBAUGBwIBCP/EAFMQAAIBAwEEBQYJBggMBwAAAAECAwAEERIFITFBBhNRYXEHIjKBkbEUQlJicqGywdEVIzOCkqIlNENjc5PS4RYkNURTdHWDwsTT8Bc2RlRks/H/xAAcAQEAAgMBAQEAAAAAAAAAAAAAAwQBAgUGBwj/xAA9EQACAQMCBAEJBwMEAQUAAAAAAQIDBBEhMQUSQVETFBUiMmFxgZGhBiMzQrHR8DRSwSQlkvFiFkNyguH/2gAMAwEAAhEDEQA/ANxoAoAoAoAoAoAoAoAoAoAoAoAoAoAoAoAoAoAoAoAoAoAoAoAoAoAoAoAoAoAoAoAoAoAoAoAoDygCsAKyAoAoAoAzQBQBmgPaAKAKAKAKAKAKAKAKAKAKAKAKA4kcKMn/ALzurEpYWWBs98By9pA+81XldU11N1TkIPtTHyPaT91Qyv6a6m6oyEm2t84epT+NQvicOht4DEm2t85vYv4VE+KLobeT9xJtqd7+3HuqJ8UZsrdHDbT+l+2341o+Jz7GfARwdoHsPrZjWj4jMz4CELnayxjMhjQcMs2BnxNI31abxFZHgxR6dpKGC4TUwyBuyQOJA51jy6tjOB4UTqS+CgkqgA3kncB4mseX1W8Iy6MUIQ7aiYgK0RJ4BXBJx2YO+t3d11q0zXw4vYdC+PYfUzD761XEahnwUeflUA4LEE8B1pz6gTmpFxGpjODXwYiy7TPa/wC1n31t5zl1Hk6FF2oflP7EP3VIuKmroCi7WPyh60/AipFxSPUx4DFV2sfmH1lfxqWPEqbNXQkKrtTtXP0WB9+KmV9TZq6Ujp9rIoy+pQOJIyB4lc7qlVzTfU15JD9Tmp08mh7WQFAFAFAFAFAFAMNtn80fpR//AGLVa8eKTN6frFE6S3M6dW0MbSqHPWojKspTScdXq7GIJA3/AF156iozlLneO2di7NtYwe9G9prcRFlMmVdldZUCSo25tDqN2QGG8cRjnmo7mk6ctlt06+02hLmWg16W7OknVRDI8cw1tEVYqDIoDoHA3Mp0kYIPGpLSpGLfMsrTPxNaibK5tPaLT/k26kVotVykbRtuA3ESEqeWsEb+SLwq7TpxgqlOOumSNtvEjQa4xYKd0ollJS8t8t8FkI0Bt0kZ82caBxYnGO5ARxrpWygk6U/zL69CCefWRbbWcSIrrnSwBGQQd/aDvB7Qa58ouLcX0J1qNNuW8ckfVyqHR3QMpBII1AnOOGOOeWM1LbylGfMuhrNZWCI6P2E9tP8AB3JktkRzbyEkuoZowYn71xuPMeGBPXq06lPnWjb1/c0jFqWCwX5xFJx9BuG8+icYA4mqlL117ySWzKp0FVo7W2SUNnSqpEYmVo5NcutmJG4FWG843DAznf0Lz0py5fi87ojp7FvlB0nTjVg4zwzjdnuzXOjhP0iR7aFJ2HcrbbOY3ERkuAzm4jfBmkdnK8wesyunB3jA3GunWg6tdcksR6NbEMXyweS5WKYjQBdICKAuc6QFAAJPEgVzanrsnjsVPak0h2r1cYLabMuU1sqljKFDEL6TAH+8V0KMYq25pf3dvYQyb5y2WcaqmFYuoLgFmLn02yCxJJ0nzd/ya5823LLWCZFd6SbWeG8tUEjpC0dxJOFUOdMS5U40k7jnhV22oqdKTxrlJEU5YkkPOjG0JpbZppiuC0jQsNOWgG+NnVfND8cgY4cBUdxTjCqox9mff1wZg202QG1dszXGxGuHIVpFJwmoAKGKjiTxxViFOMLrw08pdzXLcMmz7NOYo/oL7hXoUURzWQFAFAFAFAFAFARvSCQLAxJwAYyT2DrFqreJujI3p+sZ7tTpFYkjXOCUYsuhpAQcFTvj47iRjhXn40K2vKty65xI6PpzYR6hGXYsSzYVizMQBlmc5Y4AGTyAHIVu7OvPcx4kVsD9M0cgpY3MpXepEOoj6JAJHqqSPDau2TXx4io2/eP6Gy7g/TVl+0gqRcMl/cYddHfw/a59HZmPGWMe9xW64SurNfKBRPy23+ZRJ9KWM+6U1t5qj3MeUMU/J+3D/J2q+LfhmtvNUB47AbI24f8A2Y9Z/smtlwumY8dh+Q9ufLs/a3/TrPmunsPHYfkPbnyrP2t/06ea6Y8eQfknbn/xD+sf7NY810x5RI8NhtwfyVq3g4+8itfNcehnyhnDDba/5lE3hLF98tY81x6MeUM4+HbXHpbMPqljPuc1q+EpbMyrgbz7UuAcy7IlLcyIusPtCH31r5uqJYUmZ8eGdjyHpisShTY3UKqMAdSUUDuBAwKinw6rnLeTZV4iJ6X7OaZZnEizKpVWYPkKeI0hsYPhWnktxGPIttzPiQby9xfZ+09nLDJDDP1UcuvIyw0mQEP1esEIN/AbgeArWpC4clOSy0E4Y0GXSaS3j2S0EMyuqIFXzlLkZ544n1VJRVSVz4ko4yYlyqGEbPsv9DH9BfsivRIpDqsgKAKAKAKAKAKARu4FkRkcBkYEMDwIPEHurDSe4RXIujOzITut7UHvRGP15Na+ijdRk+g8W9s4hhQij5qaR9QApzI3VCo9kNrjpjbJxIHiyj761dREsbOoxkOnsB9AK30SW+yta+MSrh8urD/DGRjhLeY+FtOR+1pArHis3VjBetNL4o7O3bw+jazf1YX7bCnPU7GVbWy3qI5bam0P/bSDxa3Hvesc1XsbeFZLea+pydo35/k8eMtv9zGsfe/xmyjw/wDu+jPBtO87Yx/v4KYq/wAY/wBv7v5P9z38pXnyov6+GmKv8Zj/AG/u/k/3D8o3p4BG8JoKfe/xm3+3938mH5R2h/omPhJbH/ipmsOTh7/N9GdflXaA420vshP2Xpz1exh0LJ7TX1PH6R3a+lay/wBQ7fYJrPiVOxr5JbPaovmJf4bOPTt5B9KGdPtLRVn1Rh8Pj+Wa+aOh5QoAcOFH64B9jAVt4y6kb4dLoxwnSqzlHnICD2hHHvrbxEyJ2VRCMltsib0oLfPfAF+vT99Z5osidvVXQaydBdjSnIjjHcs8ijw0CTGO7FYSgaOE10Ze41AAA4cqlRGd0AUAUAUAUAUAUBF9KJCtpcMpwRFIQe8Ka1nsyWgs1Ip9zJZ9sRQ2kd1dtdytNLJEscLRxoOq7TgMMjvNQwSccsv3NWVOryU0uhBzdOrYfotmBu+4upZP3DkU5qaMKF5PZfohAeUO6H6G12fD2abfJ9pY1r40V0JVw+7nu/qJSdP9qt/negdiRQqPb1efrrXynHQkXBakt5DSbpDtCT07+5/Vdk+wRWruX2J48CS9aY1d539O7um8ZpD72qN3cuxaj9nqXWTOPyfn0pJT4uT760d3Mnh9n6HVv6HI2FFz1H1j8K18rmTx+z1t7RQbAh+SfaPwrXyyoSL7O2ntF26LRCNZNPms7IN+/KBSeXDzvqNPLKmcFaPB7KVeVHD0SeciB6Pw9h9o/CnllQs/+nLT2ng2JGvolx4ED7qyruZFL7OW3Rs9Fky+jNOPCQj3VuruZBL7OUekmKx3V1H6F7dLj+elx7NWK3V3LsV5fZ6C2mx3H0q2mno38/rIb7QNSK69hXnwBraf0HMflD2ovpXEco7JIIj9lAfrrdXCe6IJcGrR9WR1/wCIEzfprHZ03+5KN+0G+6s+LB9CJ2N5DZ/UXi6bWh/SbMdD2wXcq/u7hW3NTNGr2G6+iDpdcwXGymurZrpALgW7RzNGwz1fWEgqCeY59u6tuVaGkbqo4yi0tmfQln6C/RHuqVbHOe4tWQFAFAFAFAFAFARHS3+JXP8AQyfZNaVPVZNb/ix95hPS/wDyXYf63cVEvw2Xq39Yvh+hT6qHozpawSI7MgG7n2DefZWVTbI6l5Tp+i9X2WrErps6QfN84cWXO/njO7FS048uTk31xKs4acqT7knEwO8b/Df9dUpxaPUUalOSxF5HC1GWoiq1qTIUFakiJ+8h/g6Bv56Q+0MP+EVFGX3rXsODQn/uc17MEFUp3xNqyaMSat0QyeFkYQXOvVgbgQB7KsVKfIkcezvvKpzxsnhHFy+AT2ClOOXgzfVXRoucegjqBGRwNbNYeCOjWVamprqJtWTMjkVsQyWUWD/09L/tH/lkq7HZHlM+nP4n0vZHMafRHuqRFVi9ZAUAUAUAUAUAUBEdLf4lc/0Mn2TWlT1WTW/4sfeYT0v/AMl2H+tXNQr8Nl+r/WL4foU6qx6IGflnHMnsH41tGPcpXNd58ODx3fZfuSux9jNLv9FOfafE8z3V0LSxqXL0279PgeX4nxulZfd01mXb93uSw2XCBjSWxght2M8VIUDh35qWrC0oydOWc40l0POVOMXlRrmkl7Mf5OZ9lxtvxpPaOXr/AL64ik843J7b7Q3VOfp4kvk/g1j6keVaNtLb88D9zDkazWoOO6w+x9K4LxqF1BJvTb2p9n/hjhaps9ShQVqSIuG0ocbJhPYyt+2zf2qrQea8keVtZ54o37Zfoyo1ZPVCbVlGjGd7ciMZIJGcbvA/hU9Gnzs5nEr2NrTUpLKehHbJPmt9Ie6rNz0OHwB5VR+0UvfQbw++o6PrIvcV/pZjS1lBAXmB9/8AfU9SOuTmcLuouCoparIo1RHUZwK2I3sWA/8Al6b/AGl/yyVdjsjyf55/E+mLP0F+iPdUi2Kr3FqyAoAoAoAoAoAoCI6XfxK5/oZPsmtZ+qya3/Fj7zCemH+S7D/W7moI/hF6r/Wr4foU8Cqx6GTwmxXZ0HWOq/KOT7fNHu9lWKdN1KiguuEecvbl0LV1Ory/2RoUcIVdI4AYr3EKMYU+RbYwfK51ZTnzy3yMDbsmdwIAHnZxkb+A7fxry1zwaquaSforVZLfiRqY117HkTZZQBneCewaTk5PKqvDLapKvGWNE+o5eWLk9D3b1oHjJ5jnzwT7wcH1V6HjNup0PEW8f0LvAL2VC6jHpLT9n8GQds+VBPHG/wARuP1g14WrHlk0j7zY1fFoRkxwKiLyNC2xB/BYHyY4T7ClUYP/AFHzPE2k/wDWqXeX6mfVePbje5nCFQeLMFHrqWnTcs+woXd5C3cFLeTwv3OPgstxILeCJpZJAdIGNOBnUSSQBgY3ntHrs21LL5ji8bvvCg6PLut3t/2S9j5NdpRqwa35jhJGeX0qsV6MpYwjlcHvqNupKo8ZE77oHtHS3+KSHdyMZ9zVHSozUtUXL/iFtVt5RhLLIqDoftBBhrG58RGT7ccqnqU2zk8PvYUMxkt+owv7OSFgksbxsRkB1KkgHBIyN9QcjSydryunKp4ae6G1YJnsT5/8vTf7S/5ZKux2R5P88/ifTFn6C/RHuqRbFV7i1ZAUAUAUAUAUAUBEdLv4lc/0Mn2TWlT1WTW/4sfeYT0v/wAl2H+t3NQr8Nl6r/WL4foU4Cqy3O/VWabXsH3R58TRk9g+ojNXrOSjcQb7nkeNwc7DTsvo3kvF05CkjtHqycV6viFWpRoSnT3R85oxTlqMJCSMMWIO7GT7q8c766ryUOZvJai8P0Vgkos6RqxnnjhXuaKkqaU8Z64KdRpy9FiO0mxE3hj2kCq/EZqNtNvsWOHQc7qEV3RV7P0fEufUXYivnlf1z9BcJi1bL4jhuB8KhR028Js1PbsX+IyD5MP2FB+6uZSf3y954S1eLmD/APJfqZfXSR74hekSbkPYSPbjHuq9ZvCZ5L7TReacs9zSfIFC0slzO4z1apEjd8mWf1gKv7VXaVFRfMjz9zxCtXpKnUecPc2jFWDnnuKAr3T3bfwGwnuAAWVQqBs4LyMEUEAg4y2/HIGsMHzpt/pTPtCSNptAESsEWNSqgNjUTqYkk4Xny3YqvUehf4fH7+JG1WPTvYsH/p6b/aX/ACyVcjsjyf55/E+mLP0F+iPdUq2Kr3FqyAoAoAoAoAoAoCN6SxhrS4B4GGXP7BrWfqskpPE0/aYH0pX+CLLni8mGfpJqqGP4Z0bjS8XwKfVU9EwjYq27iDqXv+UKlz1ONXoetRktHlr2rqi9bH2msyDf52N4PP1e8V62wvoV48kn6S3Xc+Y8T4bUs6uV6vRnRhxIFHDc3gAfxGPXXO83KHEIuC9Hf3FZTzT5/h8SQr0jaWrKK1ZXdu7R14jjPHn9RPgPrOK8txa/jU+7g/RW77s919mOCVHUVWa1fq/5fw/UaRKAABwAAHqrysnl5PsNGCpwUFshZRmtTeu8Upe5/oa9tWPVbzL2xSD9w1yKf4i954Km8VE/ajIs11j6GtiH6QRZUNnh5uO3V/8AlXbSW8Tyv2kt8pVubbTBtnkotfgeyI5ChZp263SuAW61wke9iB6OnjXUWiPGvVlpHSNdwMM3WatJjARmU+ed5VtPBCePMVnJgTTpVHjJjnB3burJADDWDrHm+gQ2M55cd1Y5gZ35d9vK9raxRscTOZjuIykQ0jIPDznB/VrE3oZRjdqN/HGPrqvN6HQsKbnWTT21HVQHpXsT0xx0eb520mx6rZPwq5HZHlPzT+J9M2noL9Ee6pUVWLVkwFAFAFAFAFAFAN7+PVG69qMPapFYlsbQ0kj552152xIT/o7/AAe7VbZ95qvB+gzqXixdRfu/Up9Vj0J6VB3H+/1UUsEdWhGrHD+Z1G7IcjPiBx+kvb3ipVLXKeDk3NpJx5akeZd8Zz71+xIp0klUDzlJ3DecnH6wzV+nf3MfzZPOXPA7N7LGuuv/AEeXG2nk3as9y5b6gAvrzUFe7r1VipPT5IvWnB7OjJOnDmf/AC+m31OrRd2SMEnfk5J8SPdyrl1Xroz3XDaLpwzKOG/joOlquzqxF4PSXxHvrBrc/gT/APi/0NeuWfSwKD0W4N3HurlQUeda9T5+nqZAvAeFdQ+jx9VEPtiJ5ZIoU9KQhVHz3bSv1mujZxW5437TVZ88IZ0xn4n0zdWIjto4U1BECRqEYo2FGlNJHDBCnwBro4PKj2CyARVfDkDexAySQQx3DcTk8O2mAcy7Oj04VVXGdONwB6vqwcDduXd4UwD5y8rV5r2g0Q9G1jjgHZkDW5H6zkfq1HM3iVG2bDDv3VFJZRbs6jhVWOug6quenn6rLJdRZ2HZp/pr+U+wdXV1LRI8pF+sz6YQbqlKp1QBQBQBQBQBQBQHLDNAtD572hF/BO0Y+cF5Cx7vznUn3VWp7NHUv96cvYij1WZ6GDzFM9FYJEKrWuMkjlyrIznuyTjGACCM8cjhnNWowSPK3V9OpLDWMPOMa/EfWF0X3EcOY4f3Gq9amlqmd3hV/Kr6M4690tCRSqbPQoWWtWTIXhO8dxB9hzWDW4/Bn7n+hsV62I5D2I59ik1x6f4i954FLLMcHCuufRVsi/dEOiFjqtrx7pDOuJNBdQoI9EFSc5Un2iutbpRivSPB8XnUq15rwnponr0+hpyXyHhLEfAj+1VvmRw3SqLeL+Qqs3YUPrpk05WN7y/6sEsBpUambJwAM92OR50csLJmMXJpI+WtpsJppZWzqlkeQ7+bsW++qLrNs9OuD0EtcjTqVHL66z4jZhcMoxeVn5nrHca1W5arPFNv2F2a2Jg6PQDjJK0xHdJcIc+wE1d6nlIepL4H0OlSEB1QBQBQBQBQBQBQHhoDEtp2v57bttje8LzqO0xlZxj+szVeGkmjqXPp2tOfbQy2I5UeFVpLU7dpLmoxfsOxWrLSFVrGGbeJBaNo76xeZX1kffWMT9pjnt3u4/QUW5QfHT9ofjWrpzfRksbu2hpzJfIVju0O4Op8Dn3Vr4FTsb+dLSO80OVnHf8Ast78Vh29TsbR4zZf3/RnUV2jei6k9gIz7ONaOjNbosq+tq0XGE0/ibFtaXFrK38y59sZriU194veeNpLNWK9v+TIJLpAcF1z2ZGfZxrtKjOWyPa1OIWtPSU0cs47G/Yf+zW6t6i6FWXGrL+9fUayXEfNkHjge+s+DUXRmPOVpPaaPFkHxWH6p/CmJLoHWt5dYv5BLcOQQXcg8QWON3aK3TZG4U85SQzeso1Yka3IWI3LYRj3VJTWZIo38uWhI1a0sc7c2Xb8rOxj1DsYRuST35dPqq51PNbU0bWK3IT2gCgCgCgCgCgCgCgMv6SQ9Tt63ZsdXdwmFu8lWjIPtiqvLSojp0vvLOUf7XkxJ7UxPJE2dUTvGfGNivvFRVF6Ze4fVfkzwtUOrDZ8s7xRwqHkmOmNdXdklt24DeT4ViEE5Y7G9e9q06UanKsS21NCm6KRbGvLOS4VLm0uF6id5Y1ZY5ychwGB0qeXcGzyq2opHnpTlPVvU12Po7aLwtbceEMY/wCGtzQcJsqAcIYh4RoPuoBdLZBwVR4KBQHegdgoCK210ZtLtSs9vFIDuyVAcZ+S4wynvBrBlNorRsVkhWBwSj9XC4yQSjMsbjI3g6c7xXlKMP8AW4/8i+393ktWythW1sumCCKIfNQAnxbGSe816s5+X1JDTWQcSWyNuZFI71B99YBFXfRKxl/SWds3eYUz7QM0wEyHufJhsx9/wbQfmSzR/UrgfVWOVdiRVai2b+Zk+0eiJuZ7w7MRjb2eEIZ2dppV/SiNm5gZwOekfKqOVJMtUb+tTkm5NrsU34urDaeZx2HFVuTDwdxX0ZU/E5Xjv0HGybH4RdW0GM9bPGhHzS4157tOT6q3or0irxSrmlFLqa55P/8AGtubTuuKx4gQ8uOnd6ofr76so4tTRJGsCtyEKAKAKAKAKAKAKAKAzjy0WxWC3u0HnW06n1MRj99U9tQVlpk6PDpLndN/mRlnlItwm0pJF/R3UcV0nhKuG/eVq0rLKyT8MlyVZU3/ADBD7NuXRgYjplicTQnsZTkjwzuI5hqi5uWSn8GXZUPFhUt+q9KP+UfQjCDbmy+QWePxMcq/erj1jxq9ueb2EPJbt557Zre43Xdk3UTg+kdOQj9+QDv5lSaIMutZMBQBQHhoCn2cb/DAhZOrErkLoPWbtbDLa8YyD8XlXEpU4eXPG+5ZlJ+EXAV2yse0AUAUBUPKTt97e3WG333d23UW6jiC2537goPHkSKwDuzgg2HszefMgTU5+NJKx3472c4A5ZA5UG589bVuWkdmcKJJnaeUKAFBc5VQByH/AA5qi5c0nP4I9LCi6cIW/V+lL/CJryc4S5mu2GVsbaWb/eMpWMeJy3sqaisRyc/iVTnrKPY0/wAhOzTHs8zPnXcyvLk8dIwi/ZJ/WqeJzajzI0itjQKAKAKAKAKAKAKAKAieleyhd2k8HORCF7nHnIfUwBrWazHBLRqeHUUuxgW3kM+yracj85Yytayjn1cnnR58G8z21CvShjsdCu/BuVUWz1/cqqyaCHHxTk/RO5qrpZzE61ebpuFxH8r+j3NK8kXSD4LeNaO35i78+LsWcDeB9MfWq9tT21TMeV7o5PFrZUq3PD1ZaotnTRPybfw7UQEQS4tr4DgFYjqpSO0HAJ7gOdWDlGiq2RkbweB5VkHVAFAeGgKrZlfypJHnekfW9/nbhnuJkf2DsqjC3xcyqewlcvQwWoVeIj2gCgOZHCgkkAAZJO4ADiSaAz/oXEdoXku1ZAeqXVBYKRwiUkSS4PNzkeBI7KwZKf5W+kouboWqH8xaHXKRwafG4d4QZ9eeyq9xUxHlW7Opwm2VSq6k/Vhq/wDBmjSaiXPFjnwA3D6vfUDWMRXQ6tCTkp3E/wA2vwWxZo4mi2OqIPz+1bkKg4MYYWCp34Mh9jVbSwkjzzm5TlNn0PsHZy21vDAvoxRqg/VAGfqqVFbckKAKAKAKAKAKAKAKA8oANAY5tXZaw7VurJ/Nt9qxHQeSzNkoQeGRIG3fPWoF6M/edJ/fWuesf0MmMLRs0cgw8bNG47Cp0ke+oKi5ZHTsairUOR+4cQZaPCkiSFgyEbjld6Ed+76hWjfh1ObozaNPymznQl69N6e4+hejG1IttbM/OgHrEMNwo5SAedjs5MviK6G6PM7EP0D6Vi3gezuetkls5Gtw8ME04kSM4QgxK2CANJBOQRvoYLQvSkN6FpfN427R/VKVoDw9JXHGwvsdyRH6hKTTIEx04tVOJuvtuWbiCWFOz9Ky6P3qAhbG9jXbd3KZEEXwCBusLKI9PWN52vOMd+awCYbp3an9D8Iue+3t5pk/rFXQfbWQe/4XHlY7Q/qAPqL5oDw9NI19O12gnebOZh7Y1agK90y6TR3wg2faysj3r6JWdHiZIVGqQBZVB1sPNAxvyaAlenW3o9kbOAhAV9Igtk+cFwDjsVQWPbgDnRvCMpZeD59uVKoIySXkJaQniebEnt5e2uepc83Poj09Wi7e2har1p6y9xxaWD3E0VvF6czrGvPGTvOOwDJPcDUlJZlki4lVVKkqcf4jUujlil7tsdWM2myokgi7C6AqD2Z1a2z8xatI4UvRikbKK3IT3NAFAFAFAFAFAFAMdp3vVgDm2fUFGWPuHiwqCvVVOGWbQjzPBF9BJuss0kySZHmdsnUQzTPlSfm+h+rW9N5imYejLFUhgo3la2E09n18WRPaHrkI46V3uB6gGHegqOpHKyW7OryVMPZ6GSdPIxOINpxgBLoCO4A4JdRDDZ7AygEeBPOoprnjktW8vJbjlezK1by6JFbkfNb18D/32VXa5oNHWc/JruFbpLRlh6PbZuLF5lhLCK5CiQrgvGQd7RBiB1mksBnd5wPxcVJRuI8vpMp8R4TVVdypRzF6r4mg7O8p1vbQpDBY3CxxrpUF4hw5k6ySTvJJ3kmpvKIdyl5puv7fqeS+WIj0bH9q5RfchrHlMPb8h5qrrdx+aGknlmm5WkA8brPuiFPKF2fyMebKnWUf+SER5Y52OPg1oy/GQTksRzAJXHtFPHXVP5GVw2UvVnFvtkq1jd2ke0JLr4FKbfSHjtsx6BNuPnedp6sNqYDBwWG7dTx6fcx5rutuUtf/AIyTKcG2tV7F+EMCByB8z7qeOuifyMvhso6SnBPtzIcQ+WaTnZwn6N3+MVPHXZ/IebJvacf+SHkXlhHxrJ/1Z4294FZ8ogPNVfpyv/7Ia9IPKBaX0BhmtbtckMjqYS8ci70dD1mQwNY8pp9x5quv7fqigba2tc380Ul0ciCPq03ABiOLlQThmwGPIYAqGvXTjiLOjwzhdSNbxK0cKOuvVkA0utmfl6K+A/HjWmOWKiWKE/HrzuZbbL3IsvRV/gdrc7Ubc+Da2QPOWQYkkH0Vz+8KtU48sThXNbx6zl0RsPkn6NGysUDj89Meulzx1OBhT9FQB457amRTm8sutZNSF6Q38kOhowG3OdJ3BtADYz8UkBgDyOONQ1aqppNm0Y5JDZl+k8Uc0Zykiq6nGDhhkZHI91Sp5WTUdVkBQBQDbaIYxP1fp6W078edjcM8t9YewIXYfSRZNCybjKA0L8FlDDUB8yUDinPBI5ha9K5jNuPVG8oNLJ70lUiW3bkRLEfFlWQfVE1V+JJullG9D1iEkuxZhpkdY1JHWI+RC5YhQSQCY34DWAe8HG7mWF7Ui1Dcnq0k9SzbL6QwzBfOCM3AMRhvoOCVkH0Se/Fd+FWMtSm4tEqwzxqUwYnfbGSzvJ9mz+bZbRGq3flFLn82RyyrYU92jNQerLHc6Mv9RRz+aP6Ga3lk8MklvOuJIm0OOW7gQeYIwQeYIqConCWUdOxqwuqXhVNcHoQHiM+sn76hdSR0VYUOqfzO1tk+QvsBrV1Z9yaPD7frEcR26fIX9kfhUbqz7liFhbL/ANtfIcpGOwewVo5yfUswtKC2gvkKNErDDKpHYQCK1VSSejJZWlCaxKCfwAbJTSX0LoUqpGDxbPDlgYAP01qVVpuO+pyK1vZ07yNBx0a+vQ7SFVGAqgdgAAqF1Jt6s7ULOhBcqgvkJvEvyV9grZTl3I52dB7wXyG8lsnyE/ZH4Vt4s+5WlYW39i+Q3a1T5C+wVIq0+5Wlw626QQk0Q7PrP41uqkiCXD6Hb6imytlPdzxWsO55DjPEIg3uzdwGT38OdWKUXN5ZyuI1YW9PwKen7GhbA2Ym0tpRRQj+DdlAKnZLKDnUTwJZxqPHIX51WkcN+jHBtqjFbkInNchd3E9g4/3DvNaOaXUylkq/SjaIWGVzglY3xjgoI34PMnA31xbu8VSSpxLNOk0ss98loP5Lti3xlZh9F3Zl+oiu1FYRWe5Kba2z1TRRppaSWVIwCd2M6pMY4kIGbuxv7Dhz9LlQxpkmBUhg9oDygM66RR/AJmLqGsbhyTkZWKVzllYckZjqU8Mkjd5ueVe2subxae5YpT/KyXhuwyDTlh8Ul2fG7lqJxuOPXXKrX9SUHBk8aMU8oiJh1t0i/Ft16xv6WQMkY9S9Y36yGoI/d0XJby0+Bu1mQ0uNg9WWe2AXUcvFnQrdpRgCFPPSwZCeS5LVYo3z9Wb+P7msqXYX2btSQZWPJZMaotRt50B7Y/OifO/DAKp5GrqvZQ1lt0a1X7kXhJkZ02v1vI4rSdJFeVwsEjoFaOY7kDsmUdHJCkqcjIODxF2jceMsZRmlJ0ZqXzKpta3faVuXKkbTsF0XEZHnzwIcascWkTfntz3rU+OZcr3JJvwairU9n/MFOt5AwBFU5xcXhnqrWvGtBSiOkqFl2IulaMniLLWhNEWXuySdwA4kngB31hLOhtOoqcHOWyWTRdm7BVYltnG8xyPORyebSiY7wFIB/m81UncYzNbbL4bng7i4nVrur1393YoV3A0btG/pIxVvEcx3EYI7iKtaPVbHubO5VxRjP5+8bNWSaQi9bEMhu9boryGl1LpHfyHaanpQcmc69uY0IOT+BbY7GSwgW0iGdq7SAVwDvt7dt4Qn4rMN7HkAfkg10EsLCPHubnJ1ZmkdE5bbZ1pHbwtqI85isbySSO3pPoXgp3YLHgBUVS4jAi5XJi20OlLghdLqzZ0ISDM2OawRH0fnNIAOeKqO+5k+X59DdUSOl2bLdA/CnYRkEdTG+F35GZHXGpscgMA8241Qr8Te0CaNHuL7PYzQmObBdCYZu9lxk45BgVcdziqNX7upzx2eqJI6rDJK0vksrVUaQiGFAi+aAwRBhQXz5xxgZwCa69PibmuVLLKzoJbkV0DSS/um2jKpSFFaG0TlpYjrJPE6dOeeW7jXSoU2lmW7IZvXCNIFWTQKAKARu7VJUaORVdHBVlYBlIPEEHcRQGc7T6I3FgTLZP1lsMs9vI3nIo3kxO3pAfJYg954Vz7qxhVWVoyanVcdBxsGFhFrcYkmJmccxrA0Ke9UCJ+rXnbl+nyraOi/z83qXYbZJKq/uNyIklt7mR4s5lgbGoZV0YqGPVyrvBwd4B8RjjaSq0YqfR/zVEfoyeBkmxJPh0E85a4ggyUCqokD5GlnTIEgG85UA5C+acV0bC5oReMYb+RBWhNocdOdis7rtTZjA3MH6VV3mRFG8MnHWBuKneRu4gV2W01zRFvUS9CezM66R7MjniO0rFMRnfeW67zBIeLqOcR3nPLwzp0lFVEWqNSpZVe8X9SAhcMMiqEotbnrbetCrFSixylRMtxF1rUmiPtj9aZ0EKhpPi7gcNjOrB3eaMnJzjceIrLUVBuRyuJzU1yzeILfG7fRFmtth3+WeO4GdR1EzSAPIvmtpGkhwNOnLBfRxjABqvOvQWFJfRHBVxbN48LT3vJAbbvHkl/PLpmUaJNwAJTGCQNwbB5biFUipo04qnmOx3eFYpSxB5hPVd01umR7VhHckIvWxDIZ3UwQZO6pqcHJ6FC7uYUIc82T+x7NbCJNoXaa7iT+IWpBJLH0ZpF46RuKjmSDxxjpQgoaHiri4ndTc5bI0nycdEmg131+dV7c5Lat5RW+IPnHdnHAYUbhvk0WrKk58zwhSTZ86AQrIEhUkK+NUxjHoKFPmoVGF1HUTpzpGd3nL1UoVG9/Z0Rap8zj2F7HZ8cOdA3tvdiSzuRwLud7dm/hyqhUrTqbv9iZRSHVQmdyt9ItpCykE+lnWYCJkXJJlTJhIXvBZTzOI+yr9vSdxHw+q1Xue/8APeRVJcjycbI6IXW0nWbaAMNuDlLcEhm/pPkDu9I/N5961soUV7SpOq5GqW1usahEUKqgBQAAAAMAADgB2VeIhWgCgCgCgGm1LITxSRFmUOpUlcZGezII9RFatZWAZttC/utnyEXw6yBmwlygwm/gJF/k25b9x5E1w7vhj9amW6dfox1tm5me3JsyhZ8DXknQrEBnVQDrZRk6d2cczuPMoQhGpir/AN+wnk216JD9E9jjXrxOsUEji3WQaDIzoBLNIpUOzFi4BY8Dwq1eV8R5dMta46dkuhpTgWie9RHRGYB5CQi82KjUcDsAB31QhCbTlHoStrOByMag2MMODqdL47NQ3kdx3VPRvZ0nuaSpKRV9qbFnhufhViYwxz1kbDQJA3pBgPNbPgu/fXQp8ShnLWCxGpmn4dVZXTuindKOjCgiezQwu++Wyk81lbmbZvRlT5qkkAjA5Dp81KvH0WivbXVS0qZjqitWlyr8OPMHjVKpScT19lfUriOYvXsPFqE6cdS2eTd1Fw4PpFGC9vphiB36cHwBqG9TdLTbQ81xRPCftl/+F/soCgKkgrqYqeeGYthvAnGeYrl1JKWH16nEjsZ506dTeNpx5qRhvp+cT69LIK6VumqKT9p6rgUH4cpPvp8iuNUyO4xjeXYTdxY8FHE9lWKVKU2ci/4jSto769i0dGujixH4Rcxi6uQfzNnGQ6o3ENdOPMTHyCfHfuF2U6dvHLaPHVbid3V5qj0LLsHo9O94b6+ZGl+IuOsKdmnPmIRvAxqxx47650uKQTeNSWvyyioQjhL5li2p0tgi15f9Hp60gGQxhzhTIRnQM+yopV7itpHTO3t9xWUIQ3PNr2nXwvHk5dfNYHBDDzkZW5EMAQe6ubSqShUUmTSimtCI2Dd3SsscyGaNgWjuBoVwoxgXKZwG37iuSeYBzizc06LTlB4a3X7GkHJaMc9IOksNoPPOpz6KLvYk8PCoaFpOtpHY2nUURnsToxebQljuLwtbQxsskUS7pSVOVLH4g3Dj53cvP0drZxorTcpVKvMauoq8RHtAFAFAFAFAFAJXECupV1VlYEMrAFSDxBB3EVgGe7V6CTWzGXZj4HFrWQ/mz29U59A9x3d6iqlxZ06q1WpJGo4kdadKwSYZ0Ntcgfo5sqpPLzhnK94zXCrcPnSed0W4VlLY4gtWbaSO2pljt3YOVwDJKyqVRhuIVV4ctfMnNJzirZpaZaWPYjKT5ssf9Kr+SKNFhOJ5pY4ozgEAscuxB4qEDE1BaU4yk3PZJtm1SWNiZUbu3v4fVVZ6vKN0Mra9inMsYGrqm6uRWQ6dWM4yRpbcQdxPGpZU501GW2dtehqmpENtHoDZSnUIupb5UJMf7nofVViHEK0dG8r2/wAyZjHllzQ0ZD3Hk+df0U6t2CRSp/bTI/dqVXtOXrRwdajxivT9bDGB6K3sRDKgZlIIMUqggjmC5Q1Iq9GX5vmizLi1GrFxq03qSpvdrFdPVMDjGoC3D+0yafYKjVO1TzlfUof6LOczfs0I6PojePvKIpO8mSUE5O8klNZJ762dej1l8kdKPG6dKChSp4S7sew+T92/SXAXuiTJ/bf+zWjvIR9WOfeU6/GLiqsLEfduLW/RrZVqzCQdY6ANIZdcoVWO5nGnQq/OI5GtncXdWKcdE9saZ93U4suTmzLV9cl0gVQAFChcbguAuOWAN2PCua23L0iTCS0Ivo/tn4QHV10TRHDp80745F7VZcHuORyqe5t/DaktU9v2MQnnQhum2y2aSORC6iYG0n0IJGMb5aNurIOoKw34GdLNw41asayUXGXT0l/2R1I65XUlbfarW8Ob5oUZTgGNjhwODCMjKE/J31WqUo1J5o5efobKTS9IhbS/u7/MezYeqhLHVcONKZY5YrzZjkncCc8cca6lDhrk+aq8kE6/RF16J+T+C0brZCbi54mWTeQf5teCeO9u+uxCnGCwis5N7lxAqQwe0AUAUAUAUAUAUAUAUBGbc2Bb3iaLiJJF5ZGGUnmjjDIe8EVhpdQUa76B3dqS2z7kunHqJzn1JKB9oeuqVewp1emCWNWSK1tHbLxzxSX0E1rLCHEetOstsyAKW1IRlt2NznGeG+qDsZ04OEVlPfuTKqpPLLVsrbEMkYImSVgMsV4k8TpjG/wG88ONcmpRmpYxhFiM1jca9CoWFuXkVlkmlmmdWBDAySEgEHmFCipb2S8VJbJJfQ1prCJ+qZL1Kkt00N7MmttNyjNCCSVV4H6uXSDuHmtrIHyTXTUIzoxfZ6/HYgy1Jok+hcjNY27uzO7xh2Z2LMdWSMk9xFVbxKNaSjstDenlxTYgdrEbRWIsvVyRMijIyJYcSMSOWVcj/d1J4K8mclunn4MxzPnwPukVkJoGj1MmooA6Eq65dRlSOe81DbT5KmcZ3NprKwRvRS9d5ZknTTcwpFHKwUASDMuiRTzDA57iMVYu6cVBTg/Rb09nsNKby8Pc42sFTaClgSk9nLE4ALZMbhkGACckO4HbWaHNK303Uk0Jev70SPRa3eCyt0m810iRWBI3EDgTw3cPVUF1JTrylHZs2gmorJXL7aFlbyRTNPieMMjdSdfWKx3JIzAgqOQ3YPAirkYVqkXBR0ffp7iJuK1HcN7tO/3WlsbeI/y03m7vmgjJ7sKR31aocJS1mzSdx2LBsTyXwqwlvJHu5ePn7oge6PPnD6RI7hXVp0YQWEiu5N7l9hhVQAoAA3AAYAHcKmNRSgCgCgCgCgCgCgCgCgCgCgCgCgE5YVYEMAwPEEAg+IPGgKjtXyabPmOoQmF/lQsYt/boHmE+K1pKEZbmU8EJL5PL2H+KbRcgfFnQN++vD9mqtSwoz3RIqskNJbbbcPpW8FwO2ORVPjhynuqnLhEH6rwSK4fUjJNo3Ef6bZd2D+dGpOslI645kwwDYzjt3csVq+H1Vs+302M+NE62d0xgt4ki+DXcaRqFUPGchV3AEnGcDdVetw6vOTk8ZZvGtFLA1m6XWD9Xk3DNDIZUxjUHbUTnB3jzm3Hdv8KzG0uVnCWqwHUgx5c9NopVKra3kgOPQRwdxyN8e8cOVa0+GVk85DrxFE23eyMXg2XPqIC6pAYzpGSATIFyMk8+ZqZcKk9JSNfKFukO4tl7cn5W9qDxywZx6k1j66sQ4VTXramjrtjy38lzykG9vp5u1UxGh7jnUfZirsLWnDZETqSZathdCbK0wYYEDD47Zkk/rHJb2Gp0jXJYQtbGD2gCgCgCgCgCgCgCgCgCgCgCgCgCgCgCgCgCgPMUAaaA8KCgOeoXsFYwDoIOysg9C0B7igCgCgCgCgCgCgCgCgCgCgCgCgCgCgCgCgCgCgCgCgCgCgCgCgCgCgCgCgCgCgCgCgCgCgCgCgCgCgCgP//Z\" data-filename=\"bali mandara.jpg\"><br></p>', '2017-12-17', '2018-01-01', NULL, 1, '2017-12-13 08:35:22', '2017-12-13 08:35:22');

-- --------------------------------------------------------

--
-- Table structure for table `lsp`
--

CREATE TABLE `lsp` (
  `id_lsp` int(11) NOT NULL,
  `nama_lsp` varchar(500) NOT NULL,
  `description_lsp` longtext NOT NULL,
  `slug` varchar(500) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `id_kategori` varchar(500) NOT NULL,
  `harga` varchar(100) DEFAULT NULL,
  `harga_pendaftaran` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lsp`
--

INSERT INTO `lsp` (`id_lsp`, `nama_lsp`, `description_lsp`, `slug`, `image`, `user_id`, `id_kategori`, `harga`, `harga_pendaftaran`, `created_at`, `updated_at`) VALUES
(1, 'Pelatihan Oracle', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod<br>tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,<br>quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo<br>consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse<br>cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non<br>proident, sunt in culpa qui officia deserunt mollit anim id est laborum.<br></p>', 'pelatihan-oracle', 'pelatihan-oracle-at-cdc-undiksha-hsc.png', 1, 'Keguruan', '100000000', '120000', '2017-12-13 08:54:54', '2017-12-19 18:18:42'),
(2, 'Pelatihan Kimia Terapan', '<p>ssdf<br></p>', 'pelatihan-kimia-terapan', 'pelatihan-kimia-terapan-at-cdc-undiksha-blH.jpeg', 1, 'Keguruan', NULL, NULL, '2017-12-13 20:12:31', '2017-12-14 08:36:22'),
(3, 'Pelatihan Jaringan Komputer', '<p>pelatihan jariangan komputer<br></p>', 'pelatihan-jaringan-komputer', NULL, 1, 'Teknologi', NULL, NULL, '2017-12-13 20:50:07', '2017-12-13 20:50:07');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_11_26_123800_create_fakultas_jurusan', 1),
(4, '2017_11_26_123819_create_perusahaan_lowongan', 1),
(5, '2017_11_26_140334_create_informasi', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pekerjaan_alumni`
--

CREATE TABLE `pekerjaan_alumni` (
  `id_pekerjaan_alumni` int(11) NOT NULL,
  `nim` varchar(10) NOT NULL,
  `nama_pekerjaan` varchar(255) DEFAULT NULL,
  `latitude` varchar(100) DEFAULT NULL,
  `longitude` varchar(100) DEFAULT NULL,
  `alamat_instansi` text,
  `mulai_bekerja` year(4) DEFAULT NULL,
  `selesai_bekerja` year(4) DEFAULT NULL,
  `nama_instansi` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pekerjaan_alumni`
--

INSERT INTO `pekerjaan_alumni` (`id_pekerjaan_alumni`, `nim`, `nama_pekerjaan`, `latitude`, `longitude`, `alamat_instansi`, `mulai_bekerja`, `selesai_bekerja`, `nama_instansi`, `created_at`, `updated_at`) VALUES
(1, '1223', '11', '', '115.11530624686998', 'Tengkudak, Penebel, Kabupaten Tabanan, Bali, Indonesia', NULL, NULL, 'eka', '2017-12-17 06:39:00', '2017-12-17 06:39:00'),
(3, '111111', '33', '-8.3769043092532', '115.22516952812', 'Tengkudak, Penebel, Kabupaten Tabanan, Bali, Indonesia', NULL, NULL, '33', '2017-12-17 07:36:28', '2017-12-17 07:36:28'),
(4, '111111', '33', '-8.3769043092532', '115.22516952812', 'Tengkudak, Penebel, Kabupaten Tabanan, Bali, Indonesia', NULL, NULL, '33', '2017-12-17 07:37:24', '2017-12-17 07:37:24'),
(5, '121212', 'sd', '-8.3769043092532', '115.22516952812', 'Kemenuh, Sukawati, Kabupaten Gianyar, Bali, Indonesia', 2010, NULL, 'sd', '2017-12-17 07:44:34', '2017-12-21 06:42:09'),
(7, '35353535', NULL, '-8.3769043092532', '115.22516952812', NULL, NULL, NULL, NULL, '2017-12-17 07:52:14', '2017-12-17 07:52:14'),
(8, '35353535', NULL, '-8.3769043092532', '115.22516952812', NULL, NULL, NULL, NULL, '2017-12-17 07:53:15', '2017-12-17 07:53:15'),
(9, '1315051043', 'Programmer Blue Creative', '-8.3769043092532', '115.22516952812', 'Penglumbaran, Susut, Kabupaten Bangli, Bali, Indonesia', 2010, 2019, 'PT Blue Creative', '2017-12-21 06:51:10', '2017-12-21 07:11:36'),
(10, '1315051043', 'Programmer Blue Creative', '-8.409510290742055', '115.53827987968248', 'Jungutan, Bebandem, Kabupaten Karangasem, Bali, Indonesia', 2010, NULL, 'PT Ganesh', '2017-12-21 07:12:27', '2017-12-21 07:12:27'),
(11, '1315051044', 'Programmer Blue Creative', '-8.376904309253216', '115.18122421561998', 'Apuan, Baturiti, Kabupaten Tabanan, Bali, Indonesia', 2019, NULL, 'PT Blue Creative', '2017-12-21 09:02:33', '2017-12-21 09:02:33'),
(12, '1315051045', 'nsfn', '-8.507311736214756', '115.17573105155748', 'Batannyuh, Marga, Kabupaten Tabanan, Bali, Indonesia', 2014, 2018, 'sf', '2017-12-21 09:18:19', '2017-12-21 09:18:19'),
(13, '111144444', 'def', '-8.376904309253216', '115.02192245780748', 'Sanda, Pupuan, Kabupaten Tabanan, Bali, Indonesia', 2018, 2020, 'sag', '2017-12-21 09:24:10', '2017-12-21 09:24:10'),
(14, '111144444', 'def', '-8.376904309253216', '115.02192245780748', 'Sanda, Pupuan, Kabupaten Tabanan, Bali, Indonesia', 2018, 2020, 'sag', '2017-12-21 09:25:09', '2017-12-21 09:25:09'),
(15, '111144443', 'pdf', '-8.398641934507925', '115.10431991874498', 'Wongaya Gede, Penebel, Kabupaten Tabanan, Bali, Indonesia', 2014, 2018, 'seeds', '2017-12-21 17:45:25', '2017-12-21 17:45:25');

-- --------------------------------------------------------

--
-- Table structure for table `pendaftar_lsp`
--

CREATE TABLE `pendaftar_lsp` (
  `id_pendaftar_lsp` int(11) NOT NULL,
  `id_anggota` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `id_lsp` varchar(500) DEFAULT NULL,
  `status_pembayaran` enum('Belum Bayar','Menunggu Validasi','Sudah Bayar') NOT NULL DEFAULT 'Belum Bayar',
  `nama_file` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendaftar_lsp`
--

INSERT INTO `pendaftar_lsp` (`id_pendaftar_lsp`, `id_anggota`, `created_at`, `updated_at`, `id_lsp`, `status_pembayaran`, `nama_file`) VALUES
(5, 1, '2017-12-20 11:01:06', '2017-12-20 11:01:06', '3', 'Belum Bayar', NULL),
(6, 1, '2017-12-20 11:10:06', '2017-12-20 11:10:06', '2', 'Belum Bayar', NULL),
(7, 1, '2017-12-20 11:10:54', '2017-12-20 11:10:54', '1', 'Belum Bayar', NULL),
(8, 9, '2017-12-21 11:35:27', '2017-12-21 03:35:27', '1', 'Menunggu Validasi', 'bali-mandarajpg-at-cdc-undiksha-sdb.jpeg'),
(9, 16, '2017-12-21 15:27:39', '2017-12-21 07:27:39', '1', 'Menunggu Validasi', '27361-jpg-1477761380jpg-at-cdc-undiksha-cHN.jpeg'),
(10, 22, '2017-12-21 17:48:03', '2017-12-21 17:48:03', '3', 'Belum Bayar', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `perusahaan`
--

CREATE TABLE `perusahaan` (
  `id_perusahaan` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `perusahaan`
--

INSERT INTO `perusahaan` (`id_perusahaan`, `name`, `image`, `description`, `created_at`, `updated_at`) VALUES
(1, 'PT Blue Creative', 'pt-blue-creative-at-cdc-undiksha-g54.png', '<p>Perusahaan yang bergerak di industri kreatif&nbsp;&nbsp; Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Perusahaan yang bergerak di industri kreatif&nbsp;&nbsp; Lorem ipsum dolor sit \r\namet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut \r\nlabore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud \r\nexercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. \r\nDuis aute irure dolor in reprehenderit in voluptate velit esse cillum \r\ndolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non \r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.Perusahaan yang bergerak di industri kreatif&nbsp;&nbsp; Lorem ipsum dolor sit \r\namet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut \r\nlabore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud \r\nexercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. \r\nDuis aute irure dolor in reprehenderit in voluptate velit esse cillum \r\ndolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non \r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.Perusahaan yang bergerak di industri kreatif&nbsp;&nbsp; Lorem ipsum dolor sit \r\namet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut \r\nlabore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud \r\nexercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. \r\nDuis aute irure dolor in reprehenderit in voluptate velit esse cillum \r\ndolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non \r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.Perusahaan yang bergerak di industri kreatif&nbsp;&nbsp; Lorem ipsum dolor sit \r\namet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut \r\nlabore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud \r\nexercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. \r\nDuis aute irure dolor in reprehenderit in voluptate velit esse cillum \r\ndolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non \r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '2017-12-13 08:01:56', '2017-12-19 07:51:19');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_lengkap` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tempat_lahir` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `jk` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_telp` varchar(14) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_selular` varchar(14) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` enum('need confirm','confirm') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'need confirm',
  `level` enum('alumni','anggota','admin') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_equal` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `nama_lengkap`, `tempat_lahir`, `tgl_lahir`, `jk`, `no_telp`, `no_selular`, `is_active`, `level`, `password`, `remember_token`, `id_equal`, `created_at`, `updated_at`) VALUES
(1, 'admin123', 'admin@email.com', 'Putu Eka Parianthana, S.Pd', 'bali', '2017-12-05', 'laki-laki', '32435', '56456', 'confirm', 'admin', '$2y$10$nefd6E6qgc8Lqs9dpSDvxeuTgxVRJHz7UQmShyWisc3QDida8sLPO', '1oHR3rdloP6z53wqaX4mtKjpmHowbFgxCCFTgp87ySND6fdYAPRY2GFYZNoe', '1231', '2017-11-26 20:10:43', '2017-12-20 11:27:39'),
(2, 'eka', 'eka@gmail.com', 'anton', 'ddd', '2017-11-28', 'laki-laki', '1212', '1313', 'confirm', 'alumni', '$2y$10$j23L8PNHiPEIYxB8bP6YF.nbEN2xIdf/17kc4MrOApxmD4O1gQqC2', NULL, NULL, '2017-12-17 07:27:41', '2017-12-17 07:27:41'),
(4, 'ekayt122', 'ekdd@gmail.com', 'asdfadf', 'ee', '2017-11-27', 'laki-laki', '1213', '1313', 'confirm', 'anggota', '$2y$10$FFEoRkQxx8tKblkWjrwlQ.KLuu7f98oFX8FYj9nlZCQvr0634K2CS', NULL, NULL, '2017-12-17 07:29:50', '2017-12-19 09:08:07'),
(5, 'putuanton', 'eka.parianthana@yahoo.com', 'putu anton', NULL, '2017-12-25', 'perempuan', '111111', NULL, 'need confirm', 'alumni', '$2y$10$G0RjKsnNePBR/d0STaiZVenKrZRCzIUnFT4ekerCnTRCV/ZKuDAnS', NULL, NULL, '2017-12-17 07:37:24', '2017-12-17 07:37:24'),
(6, 'eka12345', 'sdsdfff@gmail.com', 'subagia', 'wds', '2018-01-01', 'laki-laki', '234', NULL, 'need confirm', 'alumni', '$2y$10$ymB8DGflugfE.Mtw1MbHmuH/i7EUHZuS/eyHKg32GjUWd4JIbLarK', NULL, NULL, '2017-12-17 07:44:34', '2017-12-17 07:44:34'),
(7, 'dsf', 'sdfd@kjjk.com', 'kre', 'kdkdd', '2017-12-26', 'laki-laki', '23892348', NULL, 'need confirm', 'alumni', '$2y$10$JeGw7D6dxq8tUCLkizune.7uMDL8nGYT9sEtaDSsNKQMPsPs.21j2', NULL, '565656', '2017-12-17 07:48:14', '2017-12-17 07:48:14'),
(8, 'ekaparianthana', 'ekadgg@gmail.com', 'yogi', 'ksdf', '2017-12-25', 'laki-laki', '23892348', '23424345', 'need confirm', 'alumni', '$2y$10$4.YzmTT98wJFVPWGEiEIYueY1MSYJKQyfJL.TalzcYciBjEQNGxxS', NULL, '35353535', '2017-12-17 07:53:15', '2017-12-19 17:51:08'),
(9, 'adisparta', 'adisparta@gmail.com', 'I Wayan Adi Sparta', 'Padangtegal', '1994-06-21', 'laki-laki', '085737109711', '23424345', 'confirm', 'alumni', '$2y$10$h5HuF.S8KpaO58zjVI1r6.x8drJVKPRtv0dMxSgAaN/.Jb7rFAZ0K', 'npTts2vSBlkJ1SVGpJcjXbjN3cl38wLRzP5yyJFJ54XywaAZ3j4CvtIaS96M', '1315051043', '2017-12-19 17:30:47', '2017-12-21 07:14:00'),
(10, 'admin', 'sds@sdgg.com', 'Bapak Santi', 'bali', '2017-11-27', 'laki-laki', '0888', '23424', 'confirm', 'admin', '$2y$10$P.iZKrCYKossZCLWwwfI0.1oZME3crcVIMUm9DVqz5xSPlHRHTUOS', NULL, NULL, '2017-12-19 18:11:47', '2017-12-19 18:11:47'),
(11, 'anton', 'antonsubagia@gmail.com', 'Anton', 'Jawa', '2017-12-06', 'laki-laki', '45346456', '546757', 'confirm', 'anggota', '$2y$10$4tD74M/VKMrPRbHW3du6O.YRddI2oGKXXNtELON339ChwQNxzowz6', 'lvbWNITkpjas1Lti5DwTryEtJ64JN09IVuI45pCqGEXrW5WQbGAZ59yvLMXP', NULL, '2017-12-20 11:12:26', '2017-12-20 11:15:10'),
(13, 'adisparta11', 'eka11@gmail.com', 'I Wayan Adi Sparta', 'bali', '2017-12-26', 'perempuan', '085737109711', '1313', 'confirm', 'anggota', '$2y$10$9WlxXsUIBLqplLFX/lZZeugR8aIXBWPG.f1NxwOGLgYnne79q5E2a', NULL, NULL, '2017-12-20 11:22:24', '2017-12-20 11:22:41'),
(14, 'admin321', 'admin@gmail.co', 'adminyyy', 'sad', '2017-11-27', 'laki-laki', '234234', '23452435', 'confirm', 'admin', '$2y$10$qXwuwCFJTI.PabtHxJFD5OQdltQNPg9gT6UMOp/T3LXYKRJmUEMAi', '64X561dgRlsDte1nrxZHrtcz9rrLObP2ioWywFxbtyugSSr7KeVXFX3noHW9', NULL, '2017-12-20 11:28:54', '2017-12-20 11:33:27'),
(15, 'eka123', 'eka123@gg.com', 'eka123', 'dd', '2017-12-11', 'laki-laki', '345', '3566', 'confirm', 'anggota', '$2y$10$IXynTkuQbgeOm4y7U4zXUOnM9uWCYgZO3Xz6oI2yGleBS3CqQpEE2', 'dNY9si5dihTlw1JsGuRNtUt7cpKWMTxyOQY18yjqspEtrocRRkJ9dnpVBsJ0', NULL, '2017-12-20 11:30:14', '2017-12-20 11:30:14'),
(16, 'anton123', 'anton123@gg.com', 'anton123', 'bali', '2017-12-18', 'laki-laki', '23892348', '1313', 'confirm', 'anggota', '$2y$10$NAvNB1CDBaPHqid4uOY7SOX4Bu/2uH7pQQda7ZcEdSP0yARbTZ9oq', 'y6BrE6w4l5XJURplaJzBPD33gcibfrcGhl5Skq86ArqtMXX31NINdohgE3xF', NULL, '2017-12-20 11:34:25', '2017-12-21 03:32:38'),
(17, 'yogisuputra', 'yogisuputra@gmail.com', 'Pande Ketut Yogi Suputra', 'asdf', '2017-12-19', 'laki-laki', '085737109711', '1313', 'confirm', 'anggota', '$2y$10$IK.eEiZG.rMLD2Crx1Bw5eKFG8Np6l0v96k7I8/XnWBXERjkQxKai', 'Q0w8KQdXD9yI4HTkFJuQAowQH68TVfVG8n0VHf2ttJAdcX3L76R20pl4TjI1', NULL, '2017-12-21 08:33:35', '2017-12-21 08:42:27'),
(18, 'yogiapriana', 'yogiapriana@gmail.com', 'Yogi Apriana', 'Bali', '2017-12-11', 'laki-laki', '3455454', '45645', 'confirm', 'anggota', '$2y$10$ZJ3ioUguIyDsPscSxQ26LeB/saF31EmCUAcs7uqCAhzTXma2EAcT.', '3FooYoDH2lWgOsWa2k1dH7SyAjqQBeQWhXlplAgzQ99usxW3gurKUvbIUNYm', NULL, '2017-12-21 08:44:44', '2017-12-21 08:45:56'),
(19, 'nyoman', 'eksdfea@gmail.com', 'Nyoman', 'ds', '2017-12-26', 'laki-laki', '34', '345', 'confirm', 'alumni', '$2y$10$z/r5lr95nTorCzaZnyXCYutIYGx0RWcdEyS89n31/xEuRC5k9fS2O', 'LLoKk7Cbg6xPEeA6nDVox1G3L4ORVLCbBWonWbJ73uCGYLBr9O17m81ZmJsP', '1315051044', '2017-12-21 09:02:33', '2017-12-21 09:15:22'),
(20, 'rini', 'eka.ppp@gmail.com', 'rini', 'ddd', '2017-11-27', 'laki-laki', '234', '234523', 'confirm', 'alumni', '$2y$10$7A5uvfm4vd91URq7R9VNQu0FXEmVNLTb3fi6MZDCYyPRE1PZpnnTq', NULL, '1315051045', '2017-12-21 09:18:19', '2017-12-21 09:18:36'),
(21, 'ketut', 'dsf@sdf.com', 'ketut', 'add', '2017-12-18', 'laki-laki', '234', '234', 'confirm', 'alumni', '$2y$10$TjvewJ5L3ENXVMw6LDgkl..cGZTucLELcGA0qnelJHYBRdxxfi3/G', 'wEp9c3eZbef1WpiZTjLN7uLPx7wDqoOJ2j1ZKDnMXhzGcGDTf2jTf9TsjTvZ', '111144444', '2017-12-21 09:25:09', '2017-12-21 09:27:22'),
(22, 'ekap123', 'eka@gmai.com', 'okapi', 'ee', '2017-12-19', 'laki-laki', '555', '555', 'confirm', 'alumni', '$2y$10$0o6opyxo6PUJvn8EwR0cVu/mRy.zp.lFQ19yq6rFp0TcasmmNE0Iy', NULL, '111144443', '2017-12-21 17:45:25', '2017-12-21 17:46:07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alumni`
--
ALTER TABLE `alumni`
  ADD PRIMARY KEY (`id_alumni`);

--
-- Indexes for table `fakultas`
--
ALTER TABLE `fakultas`
  ADD PRIMARY KEY (`id_fakultas`);

--
-- Indexes for table `image_slider`
--
ALTER TABLE `image_slider`
  ADD PRIMARY KEY (`id_image_slider`);

--
-- Indexes for table `informasi`
--
ALTER TABLE `informasi`
  ADD PRIMARY KEY (`id_informasi`),
  ADD KEY `informasi_user_id_foreign` (`user_id`);

--
-- Indexes for table `jurusan`
--
ALTER TABLE `jurusan`
  ADD PRIMARY KEY (`id_jurusan`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `kontak`
--
ALTER TABLE `kontak`
  ADD PRIMARY KEY (`id_kontak`);

--
-- Indexes for table `lowongan`
--
ALTER TABLE `lowongan`
  ADD PRIMARY KEY (`id_lowongan`);

--
-- Indexes for table `lsp`
--
ALTER TABLE `lsp`
  ADD PRIMARY KEY (`id_lsp`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pekerjaan_alumni`
--
ALTER TABLE `pekerjaan_alumni`
  ADD PRIMARY KEY (`id_pekerjaan_alumni`);

--
-- Indexes for table `pendaftar_lsp`
--
ALTER TABLE `pendaftar_lsp`
  ADD PRIMARY KEY (`id_pendaftar_lsp`);

--
-- Indexes for table `perusahaan`
--
ALTER TABLE `perusahaan`
  ADD PRIMARY KEY (`id_perusahaan`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alumni`
--
ALTER TABLE `alumni`
  MODIFY `id_alumni` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `fakultas`
--
ALTER TABLE `fakultas`
  MODIFY `id_fakultas` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `image_slider`
--
ALTER TABLE `image_slider`
  MODIFY `id_image_slider` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `informasi`
--
ALTER TABLE `informasi`
  MODIFY `id_informasi` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `jurusan`
--
ALTER TABLE `jurusan`
  MODIFY `id_jurusan` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8504;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `kontak`
--
ALTER TABLE `kontak`
  MODIFY `id_kontak` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `lowongan`
--
ALTER TABLE `lowongan`
  MODIFY `id_lowongan` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `lsp`
--
ALTER TABLE `lsp`
  MODIFY `id_lsp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pekerjaan_alumni`
--
ALTER TABLE `pekerjaan_alumni`
  MODIFY `id_pekerjaan_alumni` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `pendaftar_lsp`
--
ALTER TABLE `pendaftar_lsp`
  MODIFY `id_pendaftar_lsp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `perusahaan`
--
ALTER TABLE `perusahaan`
  MODIFY `id_perusahaan` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `informasi`
--
ALTER TABLE `informasi`
  ADD CONSTRAINT `informasi_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
