<?php

	return [
		'jenjang' => [
			'SLTA/SMEA/SMK' => 'SLTA/SMEA/SMK',
			'Diploma' => 'Diploma',
			'Sarjana' => 'Sarjana',
			'Master' => 'Master',
			'Doktor' => 'Doktor'
		
		],

		'jenis_pekerjaan' => [
			'Tetap' => 'Tetap',
			'Kontrak' => 'Kontrak',
		],
		'konfirmasi' => [
			'need_confirm' => 'need_confirm',
			'confirm' => 'confirm',
		],

		'agama' => [
			'hindu' => 'Hindu',
			'islam' => 'Islam',
			'budha' => 'Budha',
			'katolik' => 'Kristen Katolik',
			'kristen' => 'Kristen Protestan',
			// 'Konghucu' => 'Konghucu'
		],
		'jenis_kelamin' => [
			'laki-laki' => 'laki-laki',
			'perempuan' => 'perempuan'
		],
		'status' => [
			'single' => 'Single',
			'menikah' => 'Menikah'
		],
		'level' => [
			'admin' => 'admin',
			'alumni' => 'alumni',
		    'anggota' => 'anggota',
		],
		
		'ortu' => [
			'ayah' => 'Ayah',
			'ibu' => 'Ibu',
		],

		'tahun' => [
			'2017' => '2017',
			'2016' => '2016',
		],

		'TesKe' => [
			'1' => '1',
			'2' => '2',
		],

		'Jabatan' => [
			'Manager' => 'Manager',
			'Pelatih' => 'Pelatih',
			'Offesial' => 'Offesial',
		],


		'pendidikan' => [
			'SD' => 'SD', 
			'SMP' => 'SMP', 
			'SMA' => 'SMA', 
			'D3' => 'D3', 
			'S1' => 'S1',  
			'S2' => 'S2', 
			'S3' => 'S3'
		],


	];
