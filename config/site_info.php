<?php

return [
    'name'      => 'CDC Undiksha',
    'desc'      => 'CDC System of Undiksha',
    'owner'     => 'UNDIKSHA',
    'shortname' => 'CDC',
    'footer'    => 'Copyright &copy; ' .date('Y'). ' <a href="http://undiksha.ac.id" target="_blank">Universitas Pendidikan Ganesha</a>',
];
