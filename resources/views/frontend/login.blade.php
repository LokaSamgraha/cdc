@extends('partials.frontend.front')

@section('title')
    Login Anggota
@endsection

@section('style')

@endsection

@section('content')

  <div class="post-wrapper-top-shadow">
                <span class="s1"></span>
            </div>

    <section class="section1" style="background-color: #fff;">

    	<div class="container ">

             
        	<div class="content col-md-offset-3 col-md-6 col-md-offset-3" >

               
                {!! Form::open(['action' => 'backend\HomeC@loginAction', 'id' => 'login-form']) !!}
                
                <h3 class="title">
                    Login
                </h3>

                @if(Session::has('flash_message'))
                    <?php
                        $sessionObj = Session::get('flash_message');
                    ?>
                    <div class="alert alert-{{ $sessionObj['tipe'] }} alert-bordered">{{ $sessionObj['pesan'] }}</div>
                @endif

                <div class="form-group">
                    <label>Username</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        {!! Form::text('trigger1', null, ['class' => 'form-control', 'placeholder' => 'Username', 'required' => true]) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                        {!! Form::password('trigger2', ['class' => 'form-control', 'placeholder' => 'Password', 'required' => true]) !!}
                    </div>
                </div>

                <div class="form-group d-flex justify-content-between">
                    <label class="ui-checkbox ui-checkbox-info">
                        <input type="checkbox">
                            <span class="input-span">
                            </span>
                            Ingat Saya
                        </input>
                    </label>
                    &nbsp;
                    
                </div>
                <div class="form-group">
                    <button class="btn btn-primary btn-block" type="submit">Login</button>
                </div>
                <p style="text-align: center;"> Belum memiliki akun ? <a href="{{ action('backend\HomeC@registerAnggota')}}">
                        Pendaftaran Anggota
                </a></p>
                
            {!! Form::close() !!}
               

               
            </div><!-- end content -->
            
           
		</div><!-- end container -->
  
	</section><!-- end section -->

	@endsection