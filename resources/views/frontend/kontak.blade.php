@extends('partials.frontend.front')

@section('title')
    Kontak
@endsection

@section('content')

    <section class="post-wrapper-top" >
        <div class="container">
            <div class="post-wrapper-top-shadow">
                <span class="s1"></span>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <ul class="breadcrumb">
                    <li><a href="index.html">Beranda</a></li>
                    <li>Kontak</li>
                </ul>
                <h2>Kontak CDC Undiksha</h2>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <!-- search -->
                    <div class="search-bar">
                        <form action="" method="get">
                            <fieldset>
                                <input type="image" src="images/pixel.gif" class="searchsubmit" alt="" />
                                <input type="text" class="search_text showtextback" name="s" id="s" value="Search on this site..." />                           
                            </fieldset>
                        </form>
                    </div>
                    <!-- / end div .search-bar -->
            </div>
        </div>
    </section><!-- end post-wrapper-top -->

    <section class="section1" style="background-color: #fff;">
    	<div class="container ">
             <div id="sidebar" class="col-md-4" >
                <h4 class="title">Kontak Kami</h4>
                <ul class="contact_details">
                  <li><i class="fa fa-envelope-o"></i> {{ $kontak_kami->email }}</li>
                  <li><i class="fa fa-phone-square"></i> {{ $kontak_kami->no_telp }}</li>
                  <li><i class="fa fa-home"></i> {{ $kontak_kami->alamat }}</li>
                </ul><!-- contact_details -->

                
                
            </div><!-- end sidebar -->
        	<div class="content col-md-8" >

            
                <div id="googlemaps" style="width: 100%; height: 400px"></div>               

               
            </div><!-- end content -->
            
           
		</div><!-- end container -->
  
	</section><!-- end section -->

	@endsection

    @section('script')

    <script async defer
 src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD4wDLsrjPR-PhCj1g2rNZn1GJAMsHsAGI&callback=initMap"></script>
        <script>

    function initMap() {
        var uluru = {lat: {{ $kontak_kami->latitude }}, lng: {{ $kontak_kami->longitude }}};
        var map = new google.maps.Map(document.getElementById('googlemaps'), {
          zoom: 16,
          center: uluru
        });

            // Create our info window content   
    var infoWindowContent = '<div class="info_content">' +
       
        '<p>CDC Undiksha Office</p>' +
    '</div>';

    // Initialise the inforWindow
    var infoWindow = new google.maps.InfoWindow({
        content: infoWindowContent
    });

        var marker = new google.maps.Marker({
          position: uluru,
          map: map,
         
        });

         // Display our info window when the marker is clicked
    google.maps.event.addListener(marker, 'click', function() {
        infoWindow.open(map, marker);
    });
      }

        </script>

    @endsection