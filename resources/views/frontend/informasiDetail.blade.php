@extends('partials.frontend.front')

@section('title')
    Informasi
@endsection

@section('style')

<style type="text/css">
    
        .contentHalaman{
                padding: 12px 20px;
            }
            .contentHalaman ul, .contentHalaman ol{
                padding: 0 20px;
            }
</style>

@endsection

@section('content')

    <section class="post-wrapper-top" >
        <div class="container">
            <div class="post-wrapper-top-shadow">
                <span class="s1"></span>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <ul class="breadcrumb">
                    <li><a href="index.html">Beranda</a></li>
                    <li>Informasi</li>
                </ul>
                <h2>{{ $informasi->title }}</h2>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <!-- search -->
                    <div class="search-bar">
                        <form action="" method="get">
                            <fieldset>
                                <input type="image" src="images/pixel.gif" class="searchsubmit" alt="" />
                                <input type="text" class="search_text showtextback" name="s" id="s" value="Search on this site..." />                           
                            </fieldset>
                        </form>
                    </div>
                    <!-- / end div .search-bar -->
            </div>
        </div>
    </section><!-- end post-wrapper-top -->

    <section class="section1" style="background-color: #fff; padding-top: 0;">
    	<div class="container clearfix">
        	<div class="content col-lg-8 col-md-8 col-sm-8 col-xs-12 contentHalaman" >
        
                    
           <article>                        
                    
                    <header class="page-header blog-title">
                        <a href="#">
                            <!-- <h4 class="general-title">{{ $informasi->title }}</h4> -->
                        </a>
                        <div class="post-meta">
                            <p>
                             Oleh: <a href="#"> Admin</a><span class="sep">/</span>
                             Publikasi pada: <span class="publish-on">{{ \Carbon\Carbon::parse($informasi->created_at)->format('d M Y') }}</span> 

                            <span class="sep">/</span> Kategori: <a href="{{ action('frontend\FInfoC@infoBy',[$informasi->id_kategori] )}}">{{ $informasi->id_kategori }}</a> 

                            </p>
                        </div>
                    </header>
                    
                    <div class="post-desc">

                        @if($informasi->image !== NULL)
                        <div class="blog-media">
                                <div class="he-wrap tpl2" style="margin:25px 0;">
                                    <center> 
                                    <img alt="" style="max-height: 300px;" src="{{ url('img') }}/{{ $informasi->image }}">
                                    </center>
                                </div><!-- he wrap -->
                            </div>
                        @endif
                        {!! $informasi->description !!}
                       
                    </div>
                </article>
         

      

            </div><!-- end content -->
            
            <div id="sidebar" class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="padding: 0 10px">
                            

                <div class="widget">
                    <h4 class="title">
                        <span>Kategori</span>
                    </h4>
                    <ul class="pages">
                       @foreach($kategori as $key => $value)
                        <li><a href="{{ action('frontend\FInfoC@infoBy',[$value->nama_kategori] )}}">{{ $value->nama_kategori }}</a></li>
                       @endforeach
                    </ul>
                </div>

              
                
			</div><!-- end sidebar -->
		</div><!-- end container -->
  
	</section><!-- end section -->

	@endsection