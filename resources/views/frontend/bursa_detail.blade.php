@extends('partials.frontend.front')

@section('title')
    Bursa Kerja
@endsection

@section('style')

<style type="text/css">
    
     .contentHalaman{
                padding: 5 20px;
            }
            .contentHalaman ul, .contentHalaman ol{
                padding: 0 20px;
            }
</style>

@endsection

@section('content')

    <section class="post-wrapper-top" >
        <div class="container">
            <div class="post-wrapper-top-shadow">
                <span class="s1"></span>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <ul class="breadcrumb">
                    <li><a href="index.html">Beranda</a></li>
                    <li>Bursa Kerja</li>
                </ul>
                <h2>Bursa Kerja Terbuka</h2>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <!-- search -->
                    <div class="search-bar">
                        <form action="" method="get">
                            <fieldset>
                                <input type="image" src="images/pixel.gif" class="searchsubmit" alt="" />
                                <input type="text" class="search_text showtextback" name="s" id="s" value="Search on this site..." />                           
                            </fieldset>
                        </form>
                    </div>
                    <!-- / end div .search-bar -->
            </div>
        </div>
    </section><!-- end post-wrapper-top -->

    <section class="section1" style="background-color: #fff; padding-top: 0;">
    	<div class="container clearfix">
        	<div class="content col-lg-8 col-md-8 col-sm-8 col-xs-12 contentHalaman" >
        
                    
           <article>                        
                    
                    <header class="page-header blog-title">
                        <a href="#">
                            <h5 class="general-title"> <i> {{ $lowongan->name }} </i></h5>
                            <h4 class="general-title">{{ $lowongan->nama_lowongan }}</h4>
                        </a>
                        <div class="post-meta">
                            
                            <p>
                            Published at: <span class="publish-on">{{ $publish_date }}</span> 
                            <span class="sep"><i class="fa fa-clock-o"></i></span> <i>Tutup :  <b>{{ $tutup_date }} </b> ({{ $tgl_tutup_human}}) </i>
                            <span class="sep"><i class="fa fa-home"></i></span> <b>{{ $lowongan->name }}</b>
                            </p>
                        </div>
                    </header>
                    
                    <div class="post-desc">
                       <table border='0' style="margin-left: 30px; margin-top: 20px">
                           <tr>
                                <th width="100px" style="text-align:right;"> Jurusan</th>
                                <td>&nbsp;&nbsp;&nbsp;</td>
                                <td> {{ $lowongan->nama_jurusan }}</td>
                           </tr>
                           <tr>
                                <th style="text-align:right;"> Jenjang </th>
                                <td>&nbsp;&nbsp;&nbsp;</td>
                                <td>{{ $lowongan->jenjang }}</td>
                           </tr>
                           <tr>
                                <th style="text-align:right;"> Jenis Pekerjaan </th>
                                <td>&nbsp;&nbsp;&nbsp;</td>
                                <td>{{ $lowongan->jenis_pekerjaan }}</td>
                           </tr>
                           <tr>
                                <th style="text-align:right;"> Penempatan</th>
                                <td>&nbsp;&nbsp;&nbsp;</td>
                                <td>{{ $lowongan->penempatan }}</td>
                           </tr>
                           <tr>
                                <th valign="top" style="text-align:right; "> Kualifikasi</th>
                                <td>&nbsp;&nbsp;&nbsp;</td>
                                <td>{!! $lowongan->kualifikasi !!}</td>
                           </tr>
                       </table>
                    </div>
                </article>
         

      

            </div><!-- end content -->
            
            <div id="sidebar" class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="padding: 0 10px">
                            
                
               

                 <div class="custom-box">
                            <div class="servicetitle">
                                <h4><i class="fa fa-search"> </i>  Pencarian Lowongan</h4>
                                <hr>
                            </div>
                           <form id="cari-lowongan" action="mc.php" name="subscribe" method="post">
                                    <input type="text" name="pekerjaan" id="name" class="form-control" placeholder="Masukan Nama Pekerjaan"> 
                                    <input type="text" name="perusahaan" id="email" class="form-control" placeholder="Masukkan Nama Perusahaan"> 
                                    <select name="penempatan" class="form-control" placeholder="penempatan">
                                        <option value=""> </option>
                                        
                                    </select>
                                    <div class="pull-right">
                                        <input type="submit" value="Submit" id="submit" class="button">
                                    </div>  
                            </form>
                        </div><!-- end custombox -->

                

                 <div class="widget">
                    <h4 class="title">
                        <span>Lowongan Terkait </span>
                    </h4>

                    <h5> Berdasarkan Jurusan</h5>

                    @if(is_null($lowonganTerkaitJurusan))
                        <div style="background-color: white; border-color: #e3e4e5; color: black;" class="alert alert-info">
                            <center><i>Tidak Ada Lowongan Terkait</i></center>
                        </div>
                    @else 
                        @foreach($lowonganTerkaitJurusan as $key => $value)
                        <div style="background-color: white; border-color: #e3e4e5; color: black; padding-top: 2px; padding-bottom: 2px;" class="alert alert-info">
                            <a href="{{ url('bursa-kerja/view/'.$value->id_lowongan.'/'.$value->slug) }}"><h5>{{ $value->name }}</h5></a>
                            {{ $value->nama_lowongan}} <br>
                            {{ $value->posisi_tersedia}} posisi tersedia - <i class="fa fa-clock-o"></i> {{ \Carbon\Carbon::parse($value->tgl_tutup)->diffForHumans()}}
                        </div>
                        @endforeach

                    @endif

                    <h5> Berdasarkan Perusahaan</h5>

                    @if(is_null($lowonganTerkaitPerusahaan))
                        <div style="background-color: white; border-color: #e3e4e5; color: black;" class="alert alert-info">
                            <center><i>Tidak Ada Lowongan Terkait</i></center>
                        </div>
                    @else 
                        @foreach($lowonganTerkaitPerusahaan as $key => $value)
                        <div style="background-color: white; border-color: #e3e4e5; color: black; padding-top: 2px; padding-bottom: 2px;" class="alert alert-info">
                            <a href="{{ url('bursa-kerja/view/'.$value->id_lowongan.'/'.$value->slug) }}"><h5>{{ $value->name }}</h5></a>
                            {{ $value->nama_lowongan}} <br>
                            {{ $value->posisi_tersedia}} posisi tersedia - <i class="fa fa-clock-o"></i> {{ \Carbon\Carbon::parse($value->tgl_tutup)->diffForHumans()}}
                        </div>
                        @endforeach

                    @endif
                   
                </div><!-- end widget --> 

              
            
            	
                
			</div><!-- end sidebar -->
		</div><!-- end container -->
  
	</section><!-- end section -->

	@endsection