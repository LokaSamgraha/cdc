@extends('partials.frontend.front')

@section('title')
    Bursa Kerja
@endsection

@section('style')


@endsection

@section('content')

    <section class="post-wrapper-top" >
        <div class="container">
            <div class="post-wrapper-top-shadow">
                <span class="s1"></span>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <ul class="breadcrumb">
                    <li><a href="index.html">Beranda</a></li>
                    <li>Bursa Kerja</li>
                </ul>
                <h2>Bursa Kerja Terbuka</h2>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <!-- search -->
                    <div class="search-bar">
                        <form action="" method="get">
                            <fieldset>
                                <input type="image" src="images/pixel.gif" class="searchsubmit" alt="" />
                                <input type="text" class="search_text showtextback" name="s" id="s" value="Search on this site..." />                           
                            </fieldset>
                        </form>
                    </div>
                    <!-- / end div .search-bar -->
            </div>
        </div>
    </section><!-- end post-wrapper-top -->

    <section class="section1" style="background-color: #fff;">
    	<div class="container clearfix">
        	<div class="content col-lg-8 col-md-8 col-sm-8 col-xs-12 clearfix" >
                
                    
            @foreach($bursa_kerja as $key => $value)
				<article>                        
                    
					<div class="post-desc">
                        <div class="row">
                            
                            <div class="col-md-12">
                                <header class="page-header blog-title judul-custom">
                                    <a href="{{ url('bursa-kerja/view/'.$value->id_lowongan.'/'.$value->slug) }}"><h4 class="general-title">{{ $value->nama_lowongan }}</h4></a>
                                    <div class="post-meta">
                                        
                                        <p>
                                        Publikasi pada: <span class="publish-on">{{ $publish_date[$value->id_lowongan] }}</span> 
                                        <span class="sep"> <i class="fa fa-user"> </i></span><b>{{ $value->posisi_tersedia }} Posisi Tersedia</b>
                                        <span class="sep"><i class="fa fa-clock-o"></i></span> <i>Tutup :  {{ $tgl_tutup_human [$value->id_lowongan]}} </i>
                                        <span class="sep"><i class="fa fa-home"></i></span> <b>{{ $value->name }}</b>
                                        </p>
                                    </div>
                                </header>
                                
                                <div class="post-desc">
                                  
                                    {!! str_limit($value->description,200) !!}
                                    <br>
                                    <a class="btn btn-primary btn-sm pull-right" href="{{ url('bursa-kerja/view/'.$value->id_lowongan.'/'.$value->slug) }}" title="">Selengkapnya...</a>
                                </div>
                             </div>
                         </div>
					</div>
				</article>
					
				<hr>
            @endforeach

        <div class=" text-center">
            {{ $bursa_kerja->links() }}
        </div>


            </div><!-- end content -->
            
            <div id="sidebar" class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="padding: 0 10px">
                            
                
                <div class="widget">
                    <h4 class="title">
                    	<span>Informasi</span>
                    </h4>
                   <div class="tabbable">
                      <ul class="nav nav-tabs">
                        <li class="active"><a href="#recent" data-toggle="tab">Informasi Terbaru</a></li>
                        <li><a href="#lowongan" data-toggle="tab">Lowongan</a></li>
                      </ul>
                      <div class="tab-content">
                        <div class="tab-pane active" id="recent">
                            <ul class="recent_posts">
                                @foreach($informasi as $key => $value )
                                <li>
                                    <a href="{{ action('frontend\FInfoC@infoDetail',[$value->id_informasi,$value->slug])}}">
                                    @if($value->image !== NULL)
                                        <img src="{{ url('img') }}/{{ $value->image }}" alt="" />
                                    @else
                                        <img src="{{ url('img') }}/no-image.png" alt="" />

                                    @endif

                                    {{ $value->title }}
                                    </a>
                                    <a class="readmore" href="{{ action('frontend\FInfoC@infoDetail',[$value->id_informasi,$value->slug])}}">Selengkapnya...</a>
                                </li>

                                @endforeach 

                               
                            </ul><!-- recent posts -->
                        </div>

                        <div class="tab-pane" id="lowongan">
                            <ul class="recent_posts">
                                @foreach($lowongan as $key => $value )
                                <li>
                                    <a href="{{ action('frontend\FBursaC@detail',[$value->id_lowongan,$value->slug])}}">
                                    

                                 <b>{{ $value->nama_lowongan }}</b>
                                    </a><br>
                                    <a class="readmore" href="{{ action('frontend\FBursaC@detail',[$value->id_lowongan,$value->slug])}}">Selengkapnya...</a>
                                </li>

                                @endforeach 

                               
                            </ul><!-- recent posts -->
                        </div>
                        
                      </div>
                    </div><!-- tabbable -->
                </div><!-- end widget --> 

                <div class="widget">
                    <h4 class="title">
                        <span>Lowongan Perusahaan</span>
                    </h4>
                    <ul class="pages">
                        @foreach($perusahaan as $key => $value )
                            <li><a href="{{ action('frontend\FIndexC@lowonganBy',[$value->id_perusahaan]) }}">{{ $value->name }}</a></li>
                           
                        @endforeach
                    </ul>
                </div>
            
            	
                
			</div><!-- end sidebar -->
		</div><!-- end container -->
  
	</section><!-- end section -->

	@endsection