@extends('partials.frontend.front')

@section('title')
    Alumni
@endsection

@section('style')
    <link href="{{ url('front/frontDatatable/datatables/extensions/Responsive/css/dataTables.responsive.css') }}" rel="stylesheet" type="text/css" />

@endsection
@section('content')

    <section class="post-wrapper-top" >
        <div class="container">
            <div class="post-wrapper-top-shadow">
                <span class="s1"></span>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <ul class="breadcrumb">
                    <li><a href="index.html">Beranda</a></li>
                    <li>Alumni</li>
                </ul>
                <h2>Alumni Undiksha Terkini</h2>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <!-- search -->
                    <div class="search-bar">
                        <form action="" method="get">
                            <fieldset>
                                <input type="image" src="images/pixel.gif" class="searchsubmit" alt="" />
                                <input type="text" class="search_text showtextback" name="s" id="s" value="Search on this site..." />                           
                            </fieldset>
                        </form>
                    </div>
                    <!-- / end div .search-bar -->
            </div>
        </div>
    </section><!-- end post-wrapper-top -->

    <section class="section1" style="background-color: #fff;">
    	<div class="container clearfix">
            
        	<div class="content col-md-12" >
                    
				<table class="table table-hover" id="dataTable" style="width: 100%;">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Nama</th>
                      <th>Fakultas</th>
                      <th>Jurusan</th>
                      <th>Angkatan</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
        </table>


               
            </div><!-- end content -->
            
           
		</div><!-- end container -->
  
	</section><!-- end section -->

	@endsection

  @section('script')
    <script src="{{ url('front/frontDatatable/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('front/frontDatatable/datatables/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('front/frontDatatable/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
       $(document).ready(function(){
        $('#dataTable').DataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                ajax: '{{ action('frontend\FAlumniC@getData') }}',
                columns: [
                    { data: 'DT_Row_Index', orderable: false, searchable: false},
                    { data: 'nama_lengkap', name: 'nama_lengkap' },
                    { data: 'nama_fakultas', name: 'nama_fakultas' },
                    { data: 'nama_jurusan', name: 'nama_jurusan' },
                    { data: 'tahun_masuk_kuliah', name: 'tahun_masuk_kuliah' },
                   
                ]
            });
    });
    </script>
  @endsection