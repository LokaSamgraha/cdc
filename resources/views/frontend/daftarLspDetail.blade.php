@extends('partials.frontend.front')

@section('title')
    Lembaga Sertifikasi Profesi
@endsection

@section('style')

<style type="text/css">
    
        .contentHalaman{
                padding: 12px 20px;
            }
            .contentHalaman ul, .contentHalaman ol{
                padding: 0 20px;
            }
</style>

@endsection

@section('content')

    <section class="post-wrapper-top" >
        <div class="container">
            <div class="post-wrapper-top-shadow">
                <span class="s1"></span>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <ul class="breadcrumb">
                    <li><a href="index.html">Beranda</a></li>
                    <li>LSP</li>
                </ul>
                <h2>{{ $lsp->nama_lsp }}</h2>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <!-- search -->
                    <div class="search-bar">
                        <form action="" method="get">
                            <fieldset>
                                <input type="image" src="images/pixel.gif" class="searchsubmit" alt="" />
                                <input type="text" class="search_text showtextback" name="s" id="s" value="Search on this site..." />                           
                            </fieldset>
                        </form>
                    </div>
                    <!-- / end div .search-bar -->
            </div>
        </div>
    </section><!-- end post-wrapper-top -->

    <section class="section1" style="background-color: #fff; padding-top: 0;">
    	<div class="container clearfix">
        	<div class="content col-lg-12 col-md-12 col-sm-12 col-xs-12 contentHalaman" >
        
                    
           <article>   
           <div style="padding-left: 0px" class="row">
            <div class="col-md-6">
                <p> <strong>From : </strong><br>
                CDC UNDIKSHA 2017<br>
                {{ $kontak_kami->alamat }} <br>
                <strong> Pembayaran :</strong> <br>
                Nama Bank : {{ $kontak_kami->nama_bank }} <br>
                No Rekening : {{ $kontak_kami->no_rek }}
            </p> 

            </div>

            <div class="col-md-6"> 
                <p class="pull-right" style="text-align:right;">
                <strong>To : </strong><br>
                {{ $user['nama_lengkap']}}<br>
                {{ $user['email'] }}
            </p>
            </div>
            
        </div>


            
        @if(Session::has('pesan'))
          <div class="alert alert-{!! Session('tipe') !!}">
            
            {!! Session('pesan') !!}
              
          </div>          
          @endif
                    
                    
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Item</th>
                    <th>Harga</th>
                </tr>
                    
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Pendaftaran</td>
                        <td>Rp. {{ number_format($lsp->harga_pendaftaran,2) }}</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Training</td>
                        <td>Rp. {{ number_format($lsp->harga,2) }}</td>
                    </tr>
                    <tr>
                        <td colspan="2">Total</td>
                        <td>Rp. {{ number_format($lsp->harga_pendaftaran + $lsp->harga, 2 ) }}</td>
                    </tr>

                    
                </tbody>
                
           </table> 

            {!! Form::open(['action' => 'frontend\FLspC@storeDaftar', 'class' => 'form-horizontal']) !!}

            {!! Form::hidden('id_lsp', $lsp->id_lsp )!!}
            {!! Form::hidden('id_anggota', $user['user_id'] )!!}
            <div class="form-group">
                <div class="pull-right">
                    <button class="btn btn-info" type="submit">Lanjutkan  <i class="fa fa-arrow-right"> </i></button>
                </div>
            </div>
                        
            {!! Form::close() !!}

          
         </article>
         

      

            </div><!-- end content -->
            
            
		</div><!-- end container -->
  
	</section><!-- end section -->

	@endsection