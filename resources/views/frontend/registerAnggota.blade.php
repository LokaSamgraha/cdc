@extends('partials.frontend.front')

@section('title')
    Pendaftaran Anggota
@endsection

@section('style')


@endsection

@section('content')

  <div class="post-wrapper-top-shadow">
                <span class="s1"></span>
            </div>

    <section class="section1" style="background-color: #fff;">

    	<div class="container ">

             
        	<div class="content col-md-offset-1 col-md-10 col-md-offset-1" >

                
                

             {!! Form::open(['url' => '/store/anggota', 'class' => 'form-horizontal form-validate']) !!}
            <h3 class="title">
                    Pendaftaran Anggota
                </h3>

                 @if(Session::has('pesan'))

                <div class="alert alert-{!! Session('tipe') !!}">
                    {!! Session('pesan') !!}
                </div>

                @endif
                
                <blockquote style="border-left: 5px solid #d6d7d8; " id="pendaftaran">
                    <p style="font-size:14px; font-style: normal!important;">Anda alumni Undiksha? Gunakan formulir pendaftaran alumni. <br>
                     Selain terdaftar sebagai anggota Pusat Pengembangan Karir Undiksha, Anda juga terdaftar dalam arsip alumni. </p>
                     <a id="btn-anggota" class="btn btn-default btn-sm" style="font-size: 14px"> Tidak, saya bukan alumni</a>
                     <a href ="{{ url('pendaftaran/alumni') }}" class="btn btn-default btn-sm" style="font-size: 14px"> Pendaftaran alumni </a>
                </blockquote>

             
<div id="body-anggota" style="display: none">
             <h4 style="padding-bottom: 0px">
                    Profil Anggota
            </h4>
            <hr>

             {!! Form::open(['url' => '/store/anggota', 'class' => 'form-horizontal form-validate']) !!}


                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Lengkap <br><small style="color:grey;">tanpa gelar</small></label>
                    <div class="col-sm-10">
                        {!! Form::text('nama_lengkap', null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'Nama Lengkap' ]) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Tempat & Tanggal Lahir</label>
                    <div class="col-sm-5">
                        {!! Form::text('tempat_lahir', null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'Tempat lahir' ]) !!}
                    </div>
                    <div class="col-sm-5">
                        {!! Form::text('tgl_lahir', null, ['class' => 'form-control datepicker', 'required' => true, 'placeholder' => 'Tanggal lahir' ]) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Jenis Kelamin</label>
                    <div class="col-sm-3">

                        {!! Form::select('jk',\Config::get('dummy.jenis_kelamin'), null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'Jenis Kelamin' ]) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Nomer Telepon</label>
                    <div class="col-sm-5">
                        {!! Form::text('no_telp', null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'Nomer Telepon' ]) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nomer Selular</label>
                    <div class="col-sm-5">
                        {!! Form::text('no_selular', null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'Nomer Seluler' ]) !!}
                    </div>
                </div>
                <br>

                <h4 style="padding-bottom: 0px">
                    Informasi Akun 
                </h4>
                <hr>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Username</label>
                    <div class="col-sm-6">
                        {!! Form::text('username', null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'Username' ]) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-7">
                        {!! Form::text('email', null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'Email' ]) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Password</label>
                    <div class="col-sm-4">
                        {!! Form::password('password', ['class' => 'form-control ', 'required' => true, 'placeholder' => 'Password','id'=>'password' ]) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Ulangi Password</label>
                    <div class="col-sm-4">
                        {!! Form::password('passwordc', ['class' => 'form-control ', 'required' => true, 'placeholder' => 'Ulangi Password' ]) !!}
                    </div>
                </div>
                
               
        
              <div class="form-group">
                    <label class="col-sm-2 control-label" for="mathgroup">Hasil : {{ app('mathcaptcha')->label() }}</label>

                    <div class="col-sm-4">
                    {!! app('mathcaptcha')->input(['class' => 'form-control', 'id' => 'mathgroup']) !!}
                    </div>
                    @if ($errors->has('mathcaptcha'))
                        <span class="help-block">
                            <strong>Perhitungan Salah !</strong>
                        </span>
                    @endif
          
                    
                </div>
                  <hr>

                  <div class="form-group">
                    <div class="col-sm-offset-10 col-sm-2">
                        <button class="btn btn-info" type="submit">Simpan</button>
                    </div>
                </div>




             {!! Form::close() !!}

</div>
               

               
            </div><!-- end content -->
            
           
		</div><!-- end container -->
  
	</section><!-- end section -->

	@endsection

    @section('script')

 <script type="text/javascript">
     $(document).ready(function(){

        $('#btn-anggota').click(function(){
            $('#body-anggota').show();
        });
     });
 </script>

    

    @endsection
