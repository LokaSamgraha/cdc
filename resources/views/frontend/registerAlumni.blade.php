@extends('partials.frontend.front')

@section('title')
    Pendaftaran Alumni
@endsection

@section('style')
    <link href="{{ url('vendors/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('content')

  <div class="post-wrapper-top-shadow">
                <span class="s1"></span>
            </div>

    <section class="section1" style="background-color: #fff;">

    	<div class="container ">

             
        	<div class="content col-md-offset-1 col-md-10 col-md-offset-1" >

                
                <h3 class="title">
                    Pendaftaran Anggota
                </h3>

                <div style="background-color: white ; " class="alert alert-danger">
                    <p style="color: red;"> <strong> Informasi Alumni</strong> <br> Pastikan data sesuai dengan data pada SK</p>

                    
                </div>
               


             <h4 style="padding-bottom: 0px">
                    Data Alumni
            </h4>
            <hr>

             {!! Form::open(['url' => 'store/alumni', 'class' => 'form-horizontal form-validate']) !!}

                 @if(Session::has('pesan'))

                <div class="alert alert-{!! Session('tipe') !!}">
                    {!! Session('pesan') !!}
                </div>

                @endif



                <div class="form-group">
                    <label class="col-sm-2 control-label">NIM</label>
                    <div class="col-sm-4">
                        {!! Form::text('nim', null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'NIM Alumni' ]) !!}
                    </div>
                </div>
            
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Lengkap</label>
                    <div class="col-sm-10">
                        {!! Form::text('nama_lengkap', null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'Nama Lengkap' ]) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Tempat & Tanggal Lahir</label>
                    <div class="col-sm-5">
                        {!! Form::text('tempat_lahir', null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'Tempat lahir' ]) !!}
                    </div>
                    <div class="col-sm-5">
                        {!! Form::text('tgl_lahir', null, ['class' => 'form-control datepicker', 'required' => true, 'placeholder' => 'Tanggal lahir' ]) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Jenis Kelamin</label>
                    <div class="col-sm-3">

                        {!! Form::select('jk',\Config::get('dummy.jenis_kelamin'), null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'Jenis Kelamin' ]) !!}
                    </div>
                </div>

                 

                <div class="form-group">
                    <label class="col-sm-2 control-label">Agama</label>
                    <div class="col-sm-3">

                        {!! Form::select('agama',\Config::get('dummy.agama'), null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'Agama' ]) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Nomer Telepon</label>
                    <div class="col-sm-5">
                        {!! Form::text('no_telp', null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'Nomer Telepon' ]) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nomer Selular</label>
                    <div class="col-sm-5">
                        {!! Form::text('no_selular', null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'Nomer Seluler' ]) !!}
                    </div>
                </div>
               

                 <div class="form-group">
                    <label class="col-sm-2 control-label">Alamat</label>
                    <div class="col-sm-10">
                        {!! Form::text('alamat1', null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'Alamat Tinggal' ]) !!}
                    </div>
                </div>

                 <div class="form-group">
                    <label class="col-sm-2 control-label"></label>
                    <div class="col-sm-10">
                        {!! Form::text('alamat2', null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'Alamat Asal' ]) !!}
                    </div>
                </div>
                

                <div class="form-group">
                    <label class="col-sm-2 control-label">Universitas</label>
                    <div class="col-sm-6">
                        {!! Form::text('universitas', 'Universitas Pendidikan Ganesha', ['class' => 'form-control ', 'required' => true, 'disabled'=> true ]) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Jurusan</label>
                    <div class="col-sm-6">
                        {!! Form::select('id_jurusan',\App\Jurusan::pluck('nama','id_jurusan') ,null, ['class' => 'form-control select2', 'required' => true, 'placeholder' => 'Pilih jurusan']) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Tingkat</label>
                    <div class="col-sm-6">
                        {!! Form::select('tingkat',\Config::get('dummy.jenjang') ,null, ['class' => 'form-control ', 'required' => true,'placeholder' => 'Pilih tingkat' ]) !!}
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-sm-2 control-label">Tahun Masuk Kuliah</label>
                    <div class="col-sm-5">
                        {!! Form::text('tahun_masuk_kuliah', null, ['class' => 'form-control yearpicker', 'required' => true, 'placeholder' => 'Tahun Masuk Kuliah' ]) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Tanggal Lulus</label>
                    <div class="col-sm-5">
                        {!! Form::text('tgl_lulus', null, ['class' => 'form-control datepicker', 'required' => true, 'placeholder' => 'Tanggal Lulus' ]) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">IPK</label>
                    <div class="col-sm-2">
                        {!! Form::text('ipk', null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'IPK' ]) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-6">
                        {!! Form::text('email', null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'Email' ]) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Username</label>
                    <div class="col-sm-6">
                        {!! Form::text('username', null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'Username' ]) !!}
                    </div>
                </div>
               
                <div class="form-group">
                    <label class="col-sm-2 control-label">Password</label>
                    <div class="col-sm-4">
                        {!! Form::password('password', ['class' => 'form-control ', 'required' => true, 'placeholder' => 'Password','id'=>'password' ]) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Ulangi Password</label>
                    <div class="col-sm-4">
                        {!! Form::password('passwordc', ['class' => 'form-control ', 'required' => true, 'placeholder' => 'Ulangi Password' ]) !!}
                    </div>
                </div>
            <h4 style="margin-top: 20px">
                    Data Pekerjaan
            </h4>
            <hr>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Masa Tunggu Kerja <br><small style="color:grey;">dalam Bulan</small></label>
                    <div class="col-sm-6">
                        {!! Form::text('masa_tunggu_kerja', null, ['class' => 'form-control ', 'placeholder' => 'Lama waktu sebelum memperoleh pekerjaan ' ]) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Pekerjaan</label>
                    <div class="col-sm-6">
                        {!! Form::text('nama_pekerjaan', null, ['class' => 'form-control ', 'placeholder' => 'Nama Pekerjaan' ]) !!}
                    </div>
                   
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Instansi</label>
                    <div class="col-sm-6">
                        {!! Form::text('nama_instansi', null, ['class' => 'form-control ', 'placeholder' => 'Nama Pekerjaan' ]) !!}
                    
                    </div>
                   
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Tahun Bekerja</label>
                    <div class="col-sm-5">
                        {!! Form::text('mulai_bekerja',null, ['class' => 'form-control yearpicker', 'required' => true, 'placeholder' => 'Tahun Mulai ']) !!}
                    </div>
                    <div class="col-sm-5">
                        {!! Form::text('selesai_bekerja',null, ['class' => 'form-control yearpicker', 'placeholder' => 'Tahun Selesai']) !!}
                        <label style="color: grey;">Kosongkan tahun selesai jika masih bekerja</label>
                    </div>
                </div>

                
                 <div class="form-group">
                    <label class="col-sm-2 control-label">Alamat Instansi</label>
                    <div class="col-sm-6">
                        {!! Form::text('alamat_instansi', null, ['id'=>'alamat_instansi', 'class' => 'form-control ', 'placeholder' => 'Alamat Instansi terisi Otomatis' ]) !!}
                    </div>
                   
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="mathgroup">Hasil : {{ app('mathcaptcha')->label() }}</label>

                    <div class="col-sm-4">
                    {!! app('mathcaptcha')->input(['class' => 'form-control', 'id' => 'mathgroup']) !!}
                    </div>
                    @if ($errors->has('mathcaptcha'))
                        <span class="help-block">
                            <strong>Perhitungan Salah !</strong>
                        </span>
                    @endif
          
                    
                </div>

                <div class="alert alert-success" style="background-color: white; color:green;">
                    Silahkan posisikan marker pada map sesuai alamat tempat bekerja 
                </div>


                <div id="map" style="width: 100%; height: 250px"></div>

                    <input id="input_lat" name="latitude" type="hidden" value="-8.3769043092532">
                    <input id="input_lng" name="longitude" type="hidden" value="115.22516952812">

                <hr>
                <div class="form-group">
                    <div class="col-sm-offset-10 col-sm-2">
                        <button class="btn btn-info" type="submit">Simpan</button>
                    </div>
                </div>


             {!! Form::close() !!}

                
               

               
            </div><!-- end content -->
            
           
		</div><!-- end container -->
  
	</section><!-- end section -->

	@endsection

    @section('script')
    <script src="{{ url('vendors/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        
        $(document).ready(function(){
           
            $('.select2').select2();
        });
    </script>
  
    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyA49B12MUJIU6LL3_V3RDS81lMtEvmvRJk&language=id"></script>

    <script>

    


        var myCenter = new google.maps.LatLng($('#input_lat').val(), $('#input_lng').val());
        var marker;

        function initialize()
        {
            var mapProp = {
                center:myCenter,
                zoom:8,
                mapTypeId:google.maps.MapTypeId.ROADMAP
            };

            var map=new google.maps.Map(document.getElementById("map"),mapProp);

            var marker=new google.maps.Marker({
                position:myCenter,
                draggable:true,
            });
            var geocoder = new google.maps.Geocoder;


            marker.setMap(map);

            google.maps.event.addListener(marker, 'dragend', function() 
            {
                $('#input_lat').val(this.getPosition().lat());
                $('#input_lng').val(this.getPosition().lng());

                getcodeLatLng(geocoder, map, this.getPosition().lat(), this.getPosition().lng());

            });
        }

        function getcodeLatLng(geocoder, map, lat, lng)
        {   
            var latlng = {lat : parseFloat(lat), lng : parseFloat(lng)};
            geocoder.geocode({'location': latlng}, function(results, status) {
                    if (status === 'OK') {
                        if (results[1]) {

                            // infowindow.setContent(results[1].formatted_address);
                            $('#alamat_instansi').val(results[1].formatted_address);


                        } else {
                            window.alert('No results found');
                        }
                      } else {
                            window.alert('Geocoder failed due to: ' + status);
                      }
            });

        }

        google.maps.event.addDomListener(window, 'load', initialize);


    </script>  


        
    @endsection