@extends('partials.frontend.front')

@section('title')
    Beranda
@endsection

@section('style')
    <style>
            .post-wrapper-top{
                /*background-image: url('http://ftk.undiksha.ac.id/alumni/images/banner.jpg') !important;*/
                /*background-repeat: no-repeat;*/
                /*background-position: center center;*/
                /*background-attachment: fixed;*/
                /*background-size : cover;*/
                background: #FFF;
            }

            .post-wrapper-top h2{
                margin: 20px 0;
                border-bottom: none;
                margin-top: 40px;
                color: #FFF;
                font-size: 30px !important;
            }

            .post-wrapper-top .breadcrumb li, .post-wrapper-top .breadcrumb li a{
                color: #FFF !important;
                font-family: "Open Sans" !important;
                font-weight: 600;
                font-size: 14px;
                line-height: 24px;
                font-style: normal;
                margin-top: 20px;
            }

            .contentHalaman{
                padding: 0 30px;
            }
            .contentHalaman ul, .contentHalaman ol{
                padding: 0 20px;
            }
        </style>
@endsection
@section('content')
<div class="container">
<div class="post-wrapper-top-shadow">
        <span class="s1"></span>
    </div>
<section class="section1" style="background: #f8f8f8 !important; border-bottom: 1px solid #e5e4e4; margin-top: -20px; padding-bottom: 0px;">

        <div class="container clearfix" style="padding-bottom: 0px;">
            <div class="content clearfix" style="padding-bottom: 30px;">
                <div class="row">


                    <div class="col-sm-12">

                        <div id="slider-wrapper">
                            <div id="layerslider" style="width:100%;height:400px;">

                                @foreach($image_slider as $key => $value)
                                <div class="ls-slide" data-ls="slidedelay:8000;transition2d:75,79;">
                                        <img src="{{ url('img/'.$value->image) }}" class="ls-bg" alt="Slide background" >
                                        

                                        <p class="ls-l" style="
                                                top:190px;
                                                left:100px;
                                                font-weight: 300;
                                                height:40px;
                                                padding: 5px 14px 10px;
                                                font-size:34px;
                                                line-height:47px;
                                                color:#333;
                                                background:{{ $value->theme_color }};
                                                border-radius:0;
                                                white-space: nowrap;"
                                           data-ls="durationin:1500;delayin:300;rotatein:20;rotatexin:30;scalexin:1.5;scaleyin:1.5;transformoriginin:left 50% 0;durationout:750;rotateout:20;rotatexout:-30;scalexout:0;scaleyout:0;transformoriginout:left 50% 0;">
                                            {{ $value->title }}
                                        </p>

                                        <p class="ls-l" style="
                                                top:260px;
                                                left:105px;
                                                font-size:14px;
                                                font-weight: 300;
                                                padding:6px 10px;
                                                background: white;
                                                border-left:5px solid {{ $value->theme_color }};
                                                background: #151D25;
                                                color:#ffffff;
                                                white-space: nowrap;"
                                           data-ls="offsetxin:0;durationin:500;delayin:1800;rotateyin:90;skewxin:60;transformoriginin:25% 50% 0;offsetxout:100;durationout:750;skewxout:60;">
                                            {{ $value->subtitle }}
                                        </p>

                                    </div><!-- end first slide -->

                                    @endforeach
                                
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
</section>
</div>
    <section class="section1" style="padding-top: 0;">
    	<div class="container clearfix">
        	<div class="content col-lg-8 col-md-8 col-sm-8 col-xs-12 clearfix contentHalaman">
                
                    <h3 class="title">
                        <span>Bursa Kerja</span>
                    </h3>


                @foreach($bursa_kerja as $key => $value)
            
				<article>                        
					
                    
					<div class="post-desc">
                        <div class="row">
                            <div class="col-md-3">
                                <img src="{{ url('img') }}/{{ $value->image }}" alt="" />
                            </div>
                            <div class="col-md-9 judul-custom">
                              <a href="{{ action('frontend\FIndexC@lowonganBy',[$value->id_perusahaan]) }}"> <h4 class="general-title">{{ $value->name }}</h4>   </a>
                               {!!  str_limit($value->description,200) !!}
                                <h5 class="readmore" href="{{ url('login') }}" title=""><i class="fa fa-user"></i> &nbsp;&nbsp;{{ $count_low[$value->id_perusahaan] }}  Lowongan</h5>
                             </div>
                         </div>
					</div>
				</article>

                @if($key !== 2 )
                    <hr>
                @else

                <br>

                @endif


				

                @endforeach

              <div class="text-right">
                        <a href="{{ url('bursa-kerja') }}" class="btn btn-primary">Selengkapnya ...</a>
                </div> 
                


                <!-- start LSP info -->

                 <h3 class="title">
                        <span>Lembaga Sertifikasi Profesi</span>
                </h3>
            
                @foreach($Lsp as $key => $value)
                <article>                        
                    
                    
                   <div class="post-desc">
                        <div class="row">

                            <div class="col-md-3">
                                @if($value->image !== NULL)
                                <img src="{{ url('img') }}/{{ $value->image }}" alt="" />
                                @else
                                <img src="{{ url('img') }}/no-image.png" alt="" />

                                @endif
                            </div>
                             <div class="col-md-9">
                                <header class="page-header blog-title judul-custom">
                                   <a href="{{ action('frontend\FLspC@lspDetail',[$value->id_lsp,$value->slug])}}"> <h4 class="general-title">{{ $value->nama_lsp }}</h4></a>
                                    <div class="post-meta">
                                        <p>
                                         Oleh: <a href="#"> Admin</a><span class="sep">/</span>
                                         Publikasi pada: <span class="publish-on">{{ \Carbon\Carbon::parse($value->created_at)->format('d M Y') }}</span> 

                                        <span class="sep">/</span> Kategori: <a href="{{ action('frontend\FLspC@lspBy',[$value->id_kategori] )}}">{{ $value->id_kategori }}</a> 

                                        </p>
                                    </div>
                                </header>
                                
                                <div class="post-desc">
                                    {!! str_limit($value->description_lsp,100) !!}
                                    <a class="readmore" href="{{ action('frontend\FLspC@lspDetail',[$value->id_lsp,$value->slug])}}" title="">Selengkapnya...</a>
                                </div>
                             </div>
                         </div>
                    </div>
                </article>

                 @if($key !== 2 )
                    <hr>
                @else

                <br>

                @endif


                @endforeach
                    
               
                       
               <div class=" text-right">
                        <a href="{{ url('informasi') }}" class="btn btn-primary">Selengkapnya ...</a>
                </div> 

                    
            </div><!-- end content -->
            
            <div id="sidebar" class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="padding: 10px 10px 0">
                            
                
                <div class="widget">
                    <h4 class="title">
                    	<span>Informasi</span>
                    </h4>
                   <div class="tabbable">
                      <ul class="nav nav-tabs">
                        <li class="active"><a href="#recent" data-toggle="tab">Informasi Terbaru</a></li>
                        <li><a href="#lowongan" data-toggle="tab">Lowongan</a></li>
                      </ul>
                      <div class="tab-content">
                        <div class="tab-pane active" id="recent">
                            <ul class="recent_posts">
                                @foreach($informasi as $key => $value )
                                <li>
                                    <a href="{{ action('frontend\FInfoC@infoDetail',[$value->id_informasi,$value->slug])}}">
                                    @if($value->image !== NULL)
                                        <img src="{{ url('img') }}/{{ $value->image }}" alt="" />
                                    @else
                                        <img src="{{ url('img') }}/no-image.png" alt="" />

                                    @endif

                                    {{ $value->title }}
                                    </a>
                                    <a class="readmore" href="{{ action('frontend\FInfoC@infoDetail',[$value->id_informasi,$value->slug])}}">Selengkapnya...</a>
                                </li>

                                @endforeach 

                               
                            </ul><!-- recent posts -->
                        </div>

                        <div class="tab-pane" id="lowongan">
                            <ul class="recent_posts">
                                @foreach($lowongan as $key => $value )
                                <li>
                                    <a href="{{ action('frontend\FBursaC@detail',[$value->id_lowongan,$value->slug])}}">
                                    

                                   <b> {{ $value->nama_lowongan }}</b>
                                    </a><br>
                                    <a class="readmore" href="{{ action('frontend\FBursaC@detail',[$value->id_lowongan,$value->slug])}}">Selengkapnya...</a>
                                </li>

                                @endforeach 

                               
                            </ul><!-- recent posts -->
                        </div>
                        
                      </div>
                    </div><!-- tabbable -->
                </div><!-- end widget --> 

                <div class="widget">
                    <h4 class="title">
                        <span>Kategori </span>
                    </h4>
                    <ul class="pages">
                        @foreach($kategori as $key =>$value)
                             <li><a href="{{ action('frontend\FInfoC@infoBy',[$value->nama_kategori] )}}">{{ $value->nama_kategori }}</a></li>
                        @endforeach
                    </ul>
                </div>

            
            	
                
			</div><!-- end sidebar -->
		</div><!-- end container -->
  <hr>
        <div class="col-md-12" style="margin-bottom: 200px">
            
                
                    <div class="stat f-container">
                        
                        <div class="f-element col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <i class="fa fa-laptop fa-4x"></i>
                            <div class="milestone-counter">
                                <span class="stat-count highlight">{{ $CountPerusahaan }}</span>
                                <div class="milestone-details">Perusahaan Mitra</div>
                            </div>
                        </div>
                        <div class="f-element col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <i class="fa fa-comments-o fa-4x"></i>
                            <div class="milestone-counter">
                                <span class="stat-count highlight">{{ $CountLowongan }}</span>
                                <div class="milestone-details">Lowongan Terbuka</div>
                            </div>
                        </div>
                        <div class="f-element col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <i class="fa fa-users fa-4x"></i>
                            <div class="milestone-counter">
                                <span class="stat-count highlight">{{ $CountAlumni }}</span>
                                <div class="milestone-details">Alumni Terdaftar</div>
                            </div>
                        </div>
                        <div class="f-element col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <i class="fa fa-users fa-4x"></i>
                            <div class="milestone-counter">
                                <span class="stat-count highlight">{{ $CountLsp }}</span>
                                <div class="milestone-details">Sertifikasi Profesi</div>
                            </div>
                        </div>
                    </div><!-- end stat -->
        </div>
	</section><!-- end section -->

	@endsection