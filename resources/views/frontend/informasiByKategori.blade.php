@extends('partials.frontend.front')

@section('title')
    Informasi
@endsection

@section('content')

    <section class="post-wrapper-top" >
        <div class="container">
            <div class="post-wrapper-top-shadow">
                <span class="s1"></span>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <ul class="breadcrumb">
                    <li><a href="index.html">Beranda</a></li>
                    <li>Informasi</li>
                </ul>
                <h2>Informasi Seputar Kerja</h2>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <!-- search -->
                    <div class="search-bar">
                        <form action="" method="get">
                            <fieldset>
                                <input type="image" src="images/pixel.gif" class="searchsubmit" alt="" />
                                <input type="text" class="search_text showtextback" name="s" id="s" value="Search on this site..." />                           
                            </fieldset>
                        </form>
                    </div>
                    <!-- / end div .search-bar -->
            </div>
        </div>
    </section><!-- end post-wrapper-top -->

    <section class="section1" style="background-color: #fff;">
    	<div class="container clearfix">
        	<div class="content col-lg-8 col-md-8 col-sm-8 col-xs-12 clearfix" >
                
                    
            @foreach($informasi as $key => $value)
				<article>                        
					
                    
					<div class="post-desc">
                        <div class="row">
                            <div class="col-md-3">
                                <img src="{{ url('front/demos/01_recent_post.png') }}" alt="" />
                            </div>
                             <div class="col-md-9">
                                <header class="page-header blog-title">
                                    <h4 class="general-title">{{ $value->title }}</h4>
                                    <div class="post-meta">
                                        <p>
                                         Oleh: <a href="#"> Admin</a><span class="sep">/</span>
                                         Publikasi pada: <span class="publish-on">{{ \Carbon\Carbon::parse($value->created_at)->format('d M Y') }}</span> 

                                        <span class="sep">/</span> Kategori: <a href="#">{{ $value->id_kategori }}</a> 

                                        </p>
                                    </div>
                                </header>
                                
                                <div class="post-desc">
                                    {!! str_limit($value->description,100) !!}
                                    <a class="readmore" href="{{ action('frontend\FInfoC@infoDetail',[$value->id_informasi,$value->slug])}}" title="">Selengkapnya...</a>
                                </div>
                             </div>
                         </div>
					</div>
				</article>
					
				<hr>

                @endforeach

               
                        
                <div class=" text-center">
                     {{ $informasi->links() }}
                   
                </div>  
                
                


               
            </div><!-- end content -->
            
            <div id="sidebar" class="col-lg-4 col-md-4 col-sm-4 col-xs-12" >
                            
                
                <div class="widget">
                    <h4 class="title">
                    	<span>Informasi</span>
                    </h4>
                   <div class="tabbable">
                      <ul class="nav nav-tabs">
                        <li class="active"><a href="#recent" data-toggle="tab">Recent</a></li>
                        <li><a href="#popular" data-toggle="tab">Agenda</a></li>
                      </ul>
                      <div class="tab-content">
                        <div class="tab-pane active" id="recent">
                            <ul class="recent_posts">
                                <li>
                                    <a href="#">
                                    <img src="{{ url('front/demos/01_recent_post.png') }}" alt="" />Mockup Design PSD Template
                                    </a>
                                    <a class="readmore" href="#">read more</a>
                                </li>
                                <li>
                                    <a href="#">
                                    <img src="{{ url('front/demos/01_recent_post.png') }}" alt="" />App Screen Mockup Template
                                    </a>
                                    <a class="readmore" href="#">read more</a>
                                </li>
                                <li>
                                    <a href="#">
                                    <img src="{{ url('front/demos/01_recent_post.png') }}" alt="" />App Screen Mockup Template
                                    </a>
                                    <a class="readmore" href="#">read more</a>
                                </li>
                                <li>
                                    <a href="#">
                                    <img src="{{ url('front/demos/01_recent_post.png') }}" alt="" />App Screen Mockup Template
                                    </a>
                                    <a class="readmore" href="#">read more</a>
                                </li>
                            </ul><!-- recent posts -->
                        </div>
                        <div class="tab-pane" id="popular">
                            <ul class="recent_posts">
                                <li>
                                    <a href="#">
                                    <img src="demos/01_flickr.jpg" alt="" />Most Popular Templates for iPhone5
                                    </a>
                                    <a class="readmore" href="#">read more</a>
                                </li>
                                <li>
                                    <a href="#">
                                    <img src="demos/02_flickr.jpg" alt="" />Another Great Recent Post with Image
                                    </a>
                                    <a class="readmore" href="#">read more</a>
                                </li>
                            </ul><!-- recent posts -->
                        </div>
                      </div>
                    </div><!-- tabbable -->
                </div><!-- end widget --> 

                <div class="widget">
                    <h4 class="title">
                        <span>Kategori</span>
                    </h4>
                    <ul class="pages">
                        <li><a href="#">Job Seeker</a></li>
                        <li><a href="#">Tips Karir</a></li>
                        <li><a href="#">Panggilan Kerja</a></li>
                       
                    </ul>
                </div>

              
            
            	
                
			</div><!-- end sidebar -->
		</div><!-- end container -->
  
	</section><!-- end section -->

	@endsection