@extends('partials.frontend.front')

@section('title')
   Tracer Study
@endsection

@section('content')

    <section class="post-wrapper-top" >
        <div class="container">
            <div class="post-wrapper-top-shadow">
                <span class="s1"></span>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <ul class="breadcrumb">
                    <li><a href="index.html">Beranda</a></li>
                    <li>Tracer Study </li>
                </ul>
                <h2>Tracer Study : Pekerjaan Alumni</h2>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <!-- search -->
                    <div class="search-bar">
                        <form action="" method="get">
                            <fieldset>
                                <input type="image" src="images/pixel.gif" class="searchsubmit" alt="" />
                                <input type="text" class="search_text showtextback" name="s" id="s" value="Search on this site..." />                           
                            </fieldset>
                        </form>
                    </div>
                    <!-- / end div .search-bar -->
            </div>
        </div>
    </section><!-- end post-wrapper-top -->

    <section class="section1" style="background-color: #fff;">
    	<div class="container clearfix">
        	<div class="content col-lg-8 col-md-8 col-sm-8 col-xs-12 clearfix" >
                <div id="map" style="width: 100%; height: 400px"></div>               
            </div><!-- end content -->

            <div id="sidebar" class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="alert alert-info">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>Penting!</strong> <br>
                  Jika Kamu Alumni Undiksha, dimohonkan untuk mendaftar sebagai anggota di CDC Undiksha untuk melengkapi data tracer study. Terimaksih<br>

                  
                </div>
                
            </div>
            
            
		</div><!-- end container -->
  
	</section><!-- end section -->

	@endsection

    @section('script')

        
    
    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyD4wDLsrjPR-PhCj1g2rNZn1GJAMsHsAGI&language=id"></script>
    
    <script>


        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 7,
            center: new google.maps.LatLng(-8.376904309253167, 115.22516952812498),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var infowindow = new google.maps.InfoWindow();

        var marker, i;


        var locations = {!! $data !!}; 





        function loadMarker(){
            for (i = 0; i < locations.length; i++) {  

                console.log(locations[i]);


                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i].latitude, locations[i].longitude),
                    map: map
                });

                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                        var contentItem = '<h4 class="m-t-0 header-title">'+ locations[i].nama_lengkap +' <small>' + locations[i].nama_instansi + '</small></h4>';
                        contentItem += '<p>'+ locations[i].alamat_instansi +'</p>';

                        infowindow.setContent(contentItem);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
        }

       

    
             loadMarker();
                   
    </script>
    @endsection