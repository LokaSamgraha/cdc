@extends('partials.frontend.front')

@section('title')
    Lembaga Sertifikasi Profesi
@endsection

@section('content')

    <section class="post-wrapper-top" >
        <div class="container">
            <div class="post-wrapper-top-shadow">
                <span class="s1"></span>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <ul class="breadcrumb">
                    <li><a href="index.html">Beranda</a></li>
                    <li>LSP</li>
                </ul>
                <h2>Lembaga Sertifikasi Profesi : {{ $id_kategori }}</h2>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <!-- search -->
                    <div class="search-bar">
                        <form action="" method="get">
                            <fieldset>
                                <input type="image" src="images/pixel.gif" class="searchsubmit" alt="" />
                                <input type="text" class="search_text showtextback" name="s" id="s" value="Search on this site..." />                           
                            </fieldset>
                        </form>
                    </div>
                    <!-- / end div .search-bar -->
            </div>
        </div>
    </section><!-- end post-wrapper-top -->

    <section class="section1" style="background-color: #fff;">
    	<div class="container clearfix">
        	<div class="content col-lg-8 col-md-8 col-sm-8 col-xs-12 clearfix" >
                
                    
            @foreach($lsp as $key => $value)
				<article>                        
					
                    
					<div class="post-desc">
                        <div class="row">
                            <div class="col-md-3">

                                @if($value->image !== NULL)
                                <img src="{{ url('img') }}/{{ $value->image }}" alt="" />
                                @else
                                <img src="{{ url('img') }}/no-image.png" alt="" />

                                @endif
                            </div>
                             <div class="col-md-9">
                                <header class="page-header blog-title judul-custom">
                                    <a href="{{ action('frontend\FLspC@lspDetail',[$value->id_lsp,$value->slug])}}"> <h4 class="general-title">{{ $value->nama_lsp }}</h4></a>
                                    <div class="post-meta">
                                        <p>
                                         Oleh: <a href="#"> Admin</a><span class="sep">/</span>
                                         Publikasi pada: <span class="publish-on">{{ \Carbon\Carbon::parse($value->created_at)->format('d M Y') }}</span> 

                                        <span class="sep">/</span> Kategori: <a href="{{ action('frontend\FLspC@lspBy',[$value->id_kategori] )}}">{{ $value->id_kategori }}</a> 

                                        </p>
                                    </div>
                                </header>
                                
                                <div class="post-desc">
                                    {!! str_limit($value->description_lsp,100) !!}
                                    <a class="readmore" href="{{ action('frontend\FLspC@lspDetail',[$value->id_lsp,$value->slug])}}" title="">Selengkapnya...</a>
                                </div>
                             </div>
                         </div>
					</div>
				</article>
					
				<hr>

                @endforeach

               
                        
                <div class=" text-center">
                     {{ $lsp->links() }}
                   
                </div>  
                
                


               
            </div><!-- end content -->
            
            <div id="sidebar" class="col-lg-4 col-md-4 col-sm-4 col-xs-12" >
                            
                
                 
                <div class="widget">
                    <h4 class="title">
                        <span>Informasi</span>
                    </h4>
                   <div class="tabbable">
                      <ul class="nav nav-tabs">
                        <li class="active"><a href="#recent" data-toggle="tab">Informasi Terbaru</a></li>
                        <li><a href="#lowongan" data-toggle="tab">Lowongan</a></li>
                      </ul>
                      <div class="tab-content">
                        <div class="tab-pane active" id="recent">
                            <ul class="recent_posts">
                                @foreach($informasi as $key => $value )
                                <li>
                                    <a href="{{ action('frontend\FInfoC@infoDetail',[$value->id_informasi,$value->slug])}}">
                                    @if($value->image !== NULL)
                                        <img src="{{ url('img') }}/{{ $value->image }}" alt="" />
                                    @else
                                        <img src="{{ url('img') }}/no-image.png" alt="" />

                                    @endif

                                    {{ $value->title }}
                                    </a>
                                    <a class="readmore" href="{{ action('frontend\FInfoC@infoDetail',[$value->id_informasi,$value->slug])}}">Selengkapnya...</a>
                                </li>

                                @endforeach 

                               
                            </ul><!-- recent posts -->
                        </div>

                        <div class="tab-pane" id="lowongan">
                            <ul class="recent_posts">
                                @foreach($lowongan as $key => $value )
                                <li>
                                    <a href="{{ action('frontend\FBursaC@detail',[$value->id_lowongan,$value->slug])}}">
                                    

                                   <b> {{ $value->nama_lowongan }}</b>
                                    </a><br>
                                    <a class="readmore" href="{{ action('frontend\FBursaC@detail',[$value->id_lowongan,$value->slug])}}">Selengkapnya...</a>
                                </li>

                                @endforeach 

                               
                            </ul><!-- recent posts -->
                        </div>
                        
                      </div>
                    </div><!-- tabbable -->
                </div><!-- end widget --> 

                <div class="widget">
                    <h4 class="title">
                        <span>Kategori </span>
                    </h4>
                    <ul class="pages">
                        @foreach($kategori as $key =>$value)
                             <li><a href="{{ action('frontend\FLspC@lspBy',[$value->nama_kategori] )}}">{{ $value->nama_kategori }}</a></li>
                        @endforeach
                    </ul>
                </div>
              

              
            
            	
                
			</div><!-- end sidebar -->
		</div><!-- end container -->
  
	</section><!-- end section -->

	@endsection