<div class="form-group">
	<label class="col-sm-2 control-label">Nama</label>
	<div class="col-sm-10">
		{!! Form::hidden('id_perusahaan', null) !!}
		{!! Form::text('name', null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'Nama perusahaan' ]) !!}
	</div>
</div>

@if(isset($target))
<div class="form-group">
	<label class="col-sm-2 control-label"></label>

	<div class="col-sm-10">
		<img class="img img-thumbnail" style="height:120px;"  src="{{ url('img') }}/{{ $target->image }}"> 
	</div>

</div>
@endif
<div class="form-group">
	<label class="col-sm-2 control-label">Logo</label>

	<div class="col-sm-10">
		{!! Form::file('image',['id' => 'bootstrap-input']) !!}
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label">Deskripsi</label>
	<div class="col-sm-10">
		{!! Form::textarea('description', null, ['class' => 'form-control ', 'id'=>'summernote', 'required' => true, 'placeholder' => 'Deskripsi perusahaan']) !!}
	</div>
</div>