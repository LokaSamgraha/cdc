@extends('partials.backend.master')

@section('page_title')
	Riwayat Pekerjaan
@stop


@section('custom_styles')
	<link href="{{ url('vendors/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css"/>
	<link href="{{ url('vendors/datatables/buttons.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
	<link href="{{ url('vendors/datatables/extensions/Responsive/css/dataTables.responsive.css') }}" rel="stylesheet" type="text/css"/>
  	<link href="{{ url('vendors/jquery-confirm/jquery-confirm.min.css') }}" rel="stylesheet" type="text/css" />

	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
@stop

@section('title_breadcrumb')
	<div class="row page-heading">
		<h1 class="page-title">Pekerjaan Alumni</h1>

		<ol class="breadcrumb">
			<li><a href="{{ action('backend\DashboardC@tampil') }}">Dashboard</a></li>
			<li class="active">Alumni</li>
		</ol>
	</div>
@stop

@section('content')
	<div class="row">
		<div class="col-sm-12">
			<div class="ibox">
				<div class="ibox-head">
					<div class="ibox-title">Tambah Data Pekerjaan</div>
					<div class="ibox-tools">
						<a class="ibox-collapse">
							<i class="fa fa-angle-down"></i></a><a class="fullscreen-link"><i class="fa fa-arrows-alt"></i>
						</a>
					</div>
				</div>
				<div class="ibox-body">
					<ul class="nav nav-tabs">
	                    <li class="active"><a href="#tab-1-1" data-toggle="tab"><i class="fa fa-line-chart"></i> Data</a></li>
	                    <li><a href="#tab-1-2" data-toggle="tab"><i class="fa fa-plus-circle"></i> Add New Data</a></li>
	                  </ul>
	                  <div class="tab-content">
	                    <div class="tab-pane fade in active" id="tab-1-1">
	                    	<table class="table table-hover nowrap" id="dataTable" style="width: 100%;">
	                    		<thead>
	                    			<tr>
	                    				<th>#</th>
	                    				<th>Nama Perusahaan</th>
	                    				<th>Jabatan</th>
	                    				<th>Alamat</th>
	                    				<th>Tahun </th>
	                    				<th>Aksi </th>
	                    			</tr>
	                    		</thead>
	                    		<tbody>
	                    		</tbody>
	                    	</table>
	                    </div>
	                    <div class="tab-pane" id="tab-1-2">
	                    	{!! Form::open(['action' => 'backend\AlumniC@storePekerjaan', 'class' => 'form-horizontal form-validate']) !!}
	                    		<div class="row">
	                    		<div class="col-md-offset-2 col-md-8 col-md-offset-2">
	                    		@include('backend.alumni._form_kerja')
	                    		<div class="form-group">
	                    			<input type="hidden" name="nim" value="{{ $user['nim'] }}">
	                    		</div>
	                    		
	                    		<div class="form-group">
	                    			<div class="col-sm-offset-2 col-sm-10">
	                    				<button class="btn btn-info" type="submit">Simpan</button>
	                    			</div>
	                    		</div>
	                    	</div></div>
	                    	{!! Form::close() !!}
	                   	</div>
	                  </div><br>
				</div>
			</div>
		</div>
	</div>
@stop

@section('custom_scripts')
	<script src="{{ url('vendors/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/dataTables.buttons.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/buttons.flash.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/jszip.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/pdfmake.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/vfs_fonts.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/buttons.html5.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/buttons.print.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/colVis.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/jquery-confirm/jquery-confirm.min.js') }}" type="text/javascript"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/fileinput.min.js"></script>


	<script>


		$(document).ready(function(){
			$("#bootstrap-input").fileinput({
			    'showUpload': false
			});


			var dataTable = $('#dataTable').DataTable({
		        dom: 'Bfrtip',
		        lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
		        buttons: [
		        	'pageLength',
	                'colvis',
	                {
	                    extend: 'pdf',
	                    exportOptions: {
	                        columns: ':visible'
	                    }
	                },
	                {
	                    extend: 'excel',
	                    exportOptions: {
	                        columns: ':visible'
	                    }
	                }
	            ],
	            columnDefs: [ {
	                targets: -1,
	                visible: false
	            } ],
				responsive: true,
				processing: true,
				serverSide: true,
				ajax: '{{ action('backend\AlumniC@getPekerjaan') }}',
				columns: [
					{ data: 'DT_Row_Index', orderable: false, searchable: false},
					{ data: 'nama_instansi', name: 'nama_instansi' },
					{ data: 'nama_pekerjaan', name: 'nama_pekerjaan' },
					{ data: 'alamat_instansi', name: 'alamat_instansi' },
					{ data: 'tahun', name: 'tahun' },
					{ data: 'action', name: 'action' },
			
				]
			});

		$('#dataTable').on('click', '.btn-danger[data-remote]',function(){
            var idAndName = $(this).data('remote');

            var split = idAndName.split(',');

            var id = split[0];
            var name = split[1];

              $.confirm({
                  icon: 'fa fa-trash',
                  title: 'Hapus Data',
                  content: 'Hapus data Alumni dengan nama : '+ name +'?',
                  columnClass: 'medium',
                  theme: 'light',
                  autoClose: 'batal|20000',
                  buttons: {
                      deletePengguna: {
                          text: 'Hapus',
                          btnClass: 'btn-red',
                          action: function () {
                            $.ajax({
                              type: "POST",
                              url: "{{ action('backend\AlumniC@hapusPekerjaan') }}",
                              data: {
                                id_pekerjaan_alumni : id,
                                _token : "{{ csrf_token() }}"
                              },
                              success: function(data){
                               dataTable.ajax.reload();

                               	 Command: toastr["success"]("Berhasil Dihapus", "Pemberitahuan!!!")

									toastr.options = {
									  "closeButton": true,
									  "debug": false,
									  "newestOnTop": false,
									  "progressBar": false,
									  "positionClass": "toast-top-right",
									  "preventDuplicates": false,
									  "onclick": null,
									  "showDuration": "300",
									  "hideDuration": "1000",
									  "timeOut": "5000",
									  "extendedTimeOut": "1000",
									  "showEasing": "swing",
									  "hideEasing": "linear",
									  "showMethod": "fadeIn",
									  "hideMethod": "fadeOut"
									}
                               
                              }
                            });
                          }
                      },
                      batal: function () {

                      }
                  }
              });
            });


		});
	</script>

	<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyA49B12MUJIU6LL3_V3RDS81lMtEvmvRJk&language=id"></script>

    <script>

    	$('a[href="#tab-1-2"]').on('shown.bs.tab', function(e)
    {
        google.maps.event.trigger(map, 'resize');
    });
    

        var myCenter = new google.maps.LatLng($('#input_lat').val(), $('#input_lng').val());
        var marker;

        function initialize()
        {
            var mapProp = {
                center:myCenter,
                zoom:8,
                mapTypeId:google.maps.MapTypeId.ROADMAP
            };

            var map=new google.maps.Map(document.getElementById("map"),mapProp);

            var marker=new google.maps.Marker({
                position:myCenter,
                draggable:true,
            });
            var geocoder = new google.maps.Geocoder;


            marker.setMap(map);

            google.maps.event.addListener(marker, 'dragend', function() 
            {
                $('#input_lat').val(this.getPosition().lat());
                $('#input_lng').val(this.getPosition().lng());

                getcodeLatLng(geocoder, map, this.getPosition().lat(), this.getPosition().lng());

            });
        }

        function getcodeLatLng(geocoder, map, lat, lng)
        {   
            var latlng = {lat : parseFloat(lat), lng : parseFloat(lng)};
            geocoder.geocode({'location': latlng}, function(results, status) {
                    if (status === 'OK') {
                        if (results[1]) {

                            // infowindow.setContent(results[1].formatted_address);
                            $('#alamat_instansi').val(results[1].formatted_address);


                        } else {
                            window.alert('No results found');
                        }
                      } else {
                            window.alert('Geocoder failed due to: ' + status);
                      }
            });

        }

        google.maps.event.addDomListener(window, 'load', initialize);


    </script>  
@stop