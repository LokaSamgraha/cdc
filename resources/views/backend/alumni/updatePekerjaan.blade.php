@extends('partials.backend.master')

@section('page_title')
	Riwayat Pekerjaan
@stop


@section('custom_styles')



@stop

@section('title_breadcrumb')
	<div class="row page-heading">
		<h1 class="page-title">Data Riwayat Pekerjaan Alumni</h1>

		<ol class="breadcrumb">
			<li><a href="{{ action('backend\DashboardC@tampil') }}">Dashboard</a></li>
			<li class="active">Pekerjaan Alumni</li>
		</ol>
	</div>
@stop

@section('content')
	<div class="row">
		<div class="col-sm-12">
			<div class="ibox">
				<div class="ibox-head">
					<div class="ibox-title">Ubah Riwayat Pekerjaan Alumni</div>
					<div class="ibox-tools">
						<a class="ibox-collapse">
							<i class="fa fa-angle-down"></i></a><a class="fullscreen-link"><i class="fa fa-arrows-alt"></i>
						</a>
					</div>
				</div>
				<div class="ibox-body">
					<ul class="nav nav-tabs">
	                    <li class="active"><a href="#tab-1-1" data-toggle="tab"><i class="fa fa-pencil"></i> Ubah Data</a></li>
	                   
	                  </ul>
	                  <div class="tab-content">
	                    <div class="tab-pane fade in active" id="tab-1-1">
	                    	{!! Form::model($target,['action' => 'backend\AlumniC@updatePekerjaan', 'class' => 'form-horizontal form-validate']) !!}
	                    	<div class="row">
		                    	<div class="col-md-offset-2 col-md-8 col-md-offset-2">
		                    		@include('backend.alumni._form_kerja')

		                    		<div class="form-group">
		                    			<div class="col-sm-offset-2 col-sm-10">
		                    				<button class="btn btn-info" type="submit">Update</button>
		                    			</div>
		                    		</div>
		                    	</div>
		                    </div>
	                    	{!! Form::close() !!}
	                    </div>
	                   
	                  </div>
	                  <br>
	                  <a href="javascript:history.back()" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp;Kembali</a>
				</div>
			</div>
		</div>
	</div>
@stop

@section('custom_scripts')
	
	<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyA49B12MUJIU6LL3_V3RDS81lMtEvmvRJk&language=id"></script>

    <script>

    	$('a[href="#tab-1-2"]').on('shown.bs.tab', function(e)
    {
        google.maps.event.trigger(map, 'resize');
    });
    

        var myCenter = new google.maps.LatLng($('#input_lat').val(), $('#input_lng').val());
        var marker;

        function initialize()
        {
            var mapProp = {
                center:myCenter,
                zoom:8,
                mapTypeId:google.maps.MapTypeId.ROADMAP
            };

            var map=new google.maps.Map(document.getElementById("map"),mapProp);

            var marker=new google.maps.Marker({
                position:myCenter,
                draggable:true,
            });
            var geocoder = new google.maps.Geocoder;


            marker.setMap(map);

            google.maps.event.addListener(marker, 'dragend', function() 
            {
                $('#input_lat').val(this.getPosition().lat());
                $('#input_lng').val(this.getPosition().lng());

                getcodeLatLng(geocoder, map, this.getPosition().lat(), this.getPosition().lng());

            });
        }

        function getcodeLatLng(geocoder, map, lat, lng)
        {   
            var latlng = {lat : parseFloat(lat), lng : parseFloat(lng)};
            geocoder.geocode({'location': latlng}, function(results, status) {
                    if (status === 'OK') {
                        if (results[1]) {

                            // infowindow.setContent(results[1].formatted_address);
                            $('#alamat_instansi').val(results[1].formatted_address);


                        } else {
                            window.alert('No results found');
                        }
                      } else {
                            window.alert('Geocoder failed due to: ' + status);
                      }
            });

        }

        google.maps.event.addDomListener(window, 'load', initialize);


    </script>  

@stop