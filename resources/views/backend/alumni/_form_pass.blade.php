<div class="form-group">
	<label class="col-sm-2 control-label">Password Baru</label>
	<div class="col-sm-10">
		{!! Form::password('password', ['id'=>'password','class' => 'form-control ', 'required' => true, 'placeholder' => 'Password Baru' ]) !!}
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Ulangi Password Baru</label>
	<div class="col-sm-10">
		{!! Form::password('passwordc', ['class' => 'form-control ', 'required' => true, 'placeholder' => 'Ulangi Password Baru' ]) !!}
	</div>
</div>
