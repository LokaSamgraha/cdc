@extends('partials.backend.master')

@section('page_title')
	Alumni
@stop


@section('custom_styles')

	
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
	 <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
  	<link href="{{ url('vendors/jquery-confirm/jquery-confirm.min.css') }}" rel="stylesheet" type="text/css" />
  	<link href="{{ url('vendors/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />


@stop

@section('title_breadcrumb')
	<div class="row page-heading">
		<h1 class="page-title">Data Alumni</h1>

		<ol class="breadcrumb">
			<li><a href="{{ action('backend\DashboardC@tampil') }}">Dashboard</a></li>
			<li class="active">Alumni</li>
		</ol>
	</div>
@stop

@section('content')
	<div class="row">
		<div class="col-sm-12">
			<div class="ibox">
				<div class="ibox-head">
					<div class="ibox-title">Ubah Data Alumni</div>
					<div class="ibox-tools">
						<a class="ibox-collapse">
							<i class="fa fa-angle-down"></i></a><a class="fullscreen-link"><i class="fa fa-arrows-alt"></i>
						</a>
					</div>
				</div>
				<div class="ibox-body">
					<ul class="nav nav-tabs">
	                    <li class="active"><a href="#tab-1-1" data-toggle="tab"><i class="fa fa-pencil"></i> Ubah Data</a></li>
	                    <li><a href="#tab-1-2" data-toggle="tab"><i class="fa fa-briefcase"></i> Pekerjaan</a></li>
	                   
	                  </ul>
	                  <div class="tab-content">
	                    <div class="tab-pane fade in active" id="tab-1-1">
	                    	{!! Form::model($target,['action' => 'backend\AlumniC@update', 'class' => 'form-horizontal form-validate','files' => true]) !!}

	                    		@include('backend.alumni._form')


	                    		<div class="form-group">
	                    			<div class="col-sm-offset-2 col-sm-10">
	                    				<button class="btn btn-info" type="submit">Update</button>
	                    			</div>
	                    		</div>
	                    	{!! Form::close() !!}
	                    </div>

	                    <div class="tab-pane fade in" id="tab-1-2">
	                    	<table class="table" id="dataTable">
	                    		<thead>
	                    			<tr>
	                    				<th>#</th>
	                    				<th>Nama Pekerjaan</th>
	                    				<th>Nama Perusahaan</th>
	                    				<th>Tahun Bekerja</th>
	                    				<th>opsi</th>
	                    			</tr>
	                    		</thead>
	                    		<?php 
	                    			$no = 1;
	                    		?>
	                    		<tbody>
	                    		@forelse ($pekerjaan as $key => $value)

	                    			<?php

		                    			if($value->selesai_bekerja == NULL) {
											$selesai = 'sekarang';

		                    			}else{
											$selesai = $value->selesai_bekerja;

		                    			}


	                    			?>


								    <tr>
	                    				<td>{{ $no }}</td>
	                    				<td>{{ $value->nama_pekerjaan }}</td>
	                    				<td>{{ $value->nama_instansi }} </td>
	                    				<td>{{ $value->mulai_bekerja }} sampai {{ $selesai}} </td>
	                    				<td>
	                    					<a href="{{ action('backend\AlumniC@editPekerjaan',$value->id_pekerjaan_alumni ) }}" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i>&nbsp;Ubah </a>&nbsp;<button class="btn btn-danger btn-xs" id="deleteUser" data-remote="{{ $value->id_pekerjaan_alumni }},{{ $value->nama_pekerjaan}}"><i class="fa fa-close"></i>&nbsp;Hapus</button>
	                    				</td>
	                    			</tr>
	                    			<?php $no++; ?>
								@empty
								 <tr>
								 	<td colspan="5">
								 		 Tidak ada data pekerjaan
								 	</td>
								   
								</tr>
								@endforelse
	                    			
	                    		</tbody>
	                    		
	                    	</table>
	                    </div>


	                   
	                  </div>
	                  <br>
	                  <a href="{{ action('backend\AlumniC@tampil') }}" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp;Kembali</a>
				</div>
			</div>
		</div>
	</div>
@stop

@section('custom_scripts')
	

	<script src="{{ url('vendors/jquery-confirm/jquery-confirm.min.js') }}" type="text/javascript"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/fileinput.min.js"></script>
	 <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>
	<script src="{{ url('vendors/select2/js/select2.min.js') }}" type="text/javascript"></script>



	<script>
		$(document).ready(function(){
			$('#summernote').summernote({
				height: 500 
			});
			
			$("#bootstrap-input").fileinput({
			    'showUpload': false
			});

			$('.select2').select2();



			$('#dataTable').on('click', '.btn-danger[data-remote]',function(){
            var idAndName = $(this).data('remote');

            var split = idAndName.split(',');

            var id = split[0];
            var name = split[1];

              $.confirm({
                  icon: 'fa fa-trash',
                  title: 'Hapus Data',
                  content: 'Hapus data Pekerjaan Alumni dengan nama : '+ name +'?',
                  columnClass: 'medium',
                  theme: 'light',
                  autoClose: 'batal|20000',
                  buttons: {
                      deletePengguna: {
                          text: 'Hapus',
                          btnClass: 'btn-red',
                          action: function () {
                            $.ajax({
                              type: "POST",
                              url: "{{ action('backend\AlumniC@hapusPekerjaan') }}",
                              data: {
                                id_pekerjaan_alumni : id,
                                _token : "{{ csrf_token() }}"
                              },
                              success: function(data){
                               

                               	 Command: toastr["success"]("Berhasil Dihapus", "Pemberitahuan!!!")

									toastr.options = {
									  "closeButton": true,
									  "debug": false,
									  "newestOnTop": false,
									  "progressBar": false,
									  "positionClass": "toast-top-right",
									  "preventDuplicates": false,
									  "onclick": null,
									  "showDuration": "300",
									  "hideDuration": "1000",
									  "timeOut": "5000",
									  "extendedTimeOut": "1000",
									  "showEasing": "swing",
									  "hideEasing": "linear",
									  "showMethod": "fadeIn",
									  "hideMethod": "fadeOut"
									}

									location.reload();
                               
                              }
                            });
                          }
                      },
                      batal: function () {

                      }
                  }
              });
            });


		});



	</script>

@stop