<div class="form-group">
	<label class="col-sm-2 control-label">NIM</label>
	<div class="col-sm-10">
		{!! Form::hidden('id_alumni', null) !!}
		{!! Form::text('nim', null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'nim alumni' ]) !!}
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Nama Lengkap</label>
	<div class="col-sm-10">
		{!! Form::text('nama_lengkap', null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'nama lengkap' ]) !!}
	</div>
</div>

@if($user['level'] == 'admin' )
<div class="form-group">
	<label class="col-sm-2 control-label"><i><strong>Validasi Data</strong></i></label>
	<div class="col-sm-10">
		{!! Form::select('is_active',['need confirm' => 'Need Confirm', 'confirm' => 'Confirm'], null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'validasi data alumni' ]) !!}
	</div>
</div>
@endif

<div class="form-group">
	<label class="col-sm-2 control-label">Tempat & Tanggal Lahir</label>
	<div class="col-sm-5">
		{!! Form::text('tempat_lahir', null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'tempat lahir' ]) !!}
	</div>
	<div class="col-sm-5">
		{!! Form::text('tgl_lahir', null, ['class' => 'form-control datepicker', 'required' => true, 'placeholder' => 'tanggal lahir' ]) !!}
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Jenis Kelamin</label>
	<div class="col-sm-10">
		{!! Form::select('jk',$list_kelamin, null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'jenis kelamin alumni' ]) !!}
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Agama</label>
	<div class="col-sm-10">
		{!! Form::select('agama',$list_agama, null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'agama alumni' ]) !!}
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Kontak Alumni</label>
	<div class="col-sm-5">
		{!! Form::text('no_telp', null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'nomer telepon alumni' ]) !!}
	</div>
	<div class="col-sm-5">
		{!! Form::text('no_selular', null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'nomer hp alumni' ]) !!}
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label">Alamat</label>
	<div class="col-sm-10">
		{!! Form::text('alamat1', null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'alamat asal alumni' ]) !!}
	</div>

	
</div>

<div class="form-group">
	<label class="col-sm-2 control-label"></label>
	<div class="col-sm-10">
		{!! Form::text('alamat2', null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'alamat tinggal alumni' ]) !!}
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Email</label>
	<div class="col-sm-10">
		{!! Form::text('email', null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'email alumni' ]) !!}
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Jurusan</label>
	<div class="col-sm-10">
		{!! Form::select('id_jurusan',$list_jurusan, null, ['style' => 'width:500px;','class' => 'form-control select2 ', 'required' => true, 'placeholder' => 'jurusan alumni' ]) !!}
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Tingkat</label>
	<div class="col-sm-10">
		{!! Form::select('tingkat',$list_jenjang, null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'tingkat alumni' ]) !!}
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Tahun Masuk & Tanggal Lulus</label>
	<div class="col-sm-5">
		{!! Form::text('tahun_masuk_kuliah', null, ['class' => 'form-control yearpicker', 'required' => true, 'placeholder' => 'tahun masuk kuliah' ]) !!}
	</div>
	<div class="col-sm-5">
		{!! Form::text('tgl_lulus', null, ['class' => 'form-control datepicker', 'required' => true, 'placeholder' => 'tangal lulus' ]) !!}
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">IPK</label>
	<div class="col-sm-10">
		{!! Form::text('ipk', null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'ipk alumni' ]) !!}
	</div>
</div>


<div class="form-group">
	<label class="col-sm-2 control-label">Username</label>
	<div class="col-sm-10">
		{!! Form::text('username', null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'username alumni' ]) !!}
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Masa Tunggu Kerja <br> <small>dalam bulan</small></label>
	<div class="col-sm-10">
		{!! Form::number('masa_tunggu_kerja', null, ['class' => 'form-control ', 'required' => true,  ]) !!}
	</div>
</div>



@if($user['level'] == 'admin' )

<div class="form-group">
	<label class="col-sm-2 control-label">Password</label>
	<div class="col-sm-10">
		{!! Form::password('password', ['class' => 'form-control ', 'placeholder' => 'password alumni' ]) !!}
	</div>
</div>

@endif

