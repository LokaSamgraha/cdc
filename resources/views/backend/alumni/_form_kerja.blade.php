
{!! Form::hidden('id_pekerjaan_alumni',null) !!}
<div class="form-group">
	<label class="col-sm-2 control-label">Nama Perusahaan</label>
	<div class="col-sm-10">
		{!! Form::text('nama_instansi',null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'Nama Perusahaan/Instansi' ]) !!}
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Nama Pekerjaan</label>
	<div class="col-sm-10">
		{!! Form::text('nama_pekerjaan',null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'Nama Pekerjaan']) !!}
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label">Tahun Bekerja</label>
	<div class="col-sm-5">
		{!! Form::text('mulai_bekerja',null, ['class' => 'form-control yearpicker', 'required' => true, 'placeholder' => 'Tahun Mulai ']) !!}
	</div>
	<div class="col-sm-5">
		{!! Form::text('selesai_bekerja',null, ['class' => 'form-control yearpicker', 'placeholder' => 'Tahun Selesai']) !!}
		<label style="color: grey;">Kosongkan tahun selesai jika masih bekerja</label>
	</div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Alamat Instansi</label>
    <div class="col-sm-10">
        {!! Form::text('alamat_instansi', null, ['id'=>'alamat_instansi', 'class' => 'form-control','required' => true, 'placeholder' => 'Alamat Instansi terisi Otomatis' ]) !!}
    </div>
   
</div>

<div class="alert alert-success" style="background-color: white; color:green;">
    Silahkan posisikan marker pada map sesuai alamat tempat bekerja 
</div>
<div class="form-group">

	<div id="map" style="width: 100%; height: 250px"></div>
</div>

    <input id="input_lat" name="latitude" type="hidden" value="-8.3769043092532">
    <input id="input_lng" name="longitude" type="hidden" value="115.22516952812">