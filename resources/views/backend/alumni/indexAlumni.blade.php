@extends('partials.backend.master')

@section('page_title')
	Alumni
@stop


@section('custom_styles')
	
@stop

@section('title_breadcrumb')
	<div class="row page-heading">
		<h1 class="page-title">Data Profil Alumni</h1>

		<ol class="breadcrumb">
			<li><a href="{{ action('backend\DashboardC@tampil') }}">Dashboard</a></li>
			<li class="active">Profil Alumni</li>
		</ol>
	</div>
@stop

@section('content')
	<div class="row">
		<div class="col-sm-12">
			<div class="ibox">
				<div class="ibox-head">
					<div class="ibox-title"> Data Profil</div>
					<div class="ibox-tools">
						<a class="ibox-collapse">
							<i class="fa fa-angle-down"></i></a><a class="fullscreen-link"><i class="fa fa-arrows-alt"></i>
						</a>
					</div>
				</div>
				<div class="ibox-body">
					<ul class="nav nav-tabs">
	                    <li class="active"><a href="#tab-1-1" data-toggle="tab">Data Alumni</a></li>
	                    <li><a href="#tab-1-2" data-toggle="tab">Ganti Password</a></li>
	                  </ul>
	                  <div class="tab-content">
	                    <div class="tab-pane fade in active" id="tab-1-1">
	                    	{!! Form::model($target,['action' => 'backend\AlumniC@update', 'class' => 'form-horizontal form-validate']) !!}
	                    	<div class="row">
	                    		<div class="col-md-offset-2 col-md-8 col-md-offset-2">
	                    		@include('backend.alumni._form')
	                    		<div class="form-group">
	                    			<div class="col-sm-offset-2 col-sm-10">
	                    				<button class="btn btn-info" type="submit">Simpan</button>
	                    			</div>
	                    		</div>
	                    	</div>

	                    	</div>
	                    	
	                    	{!! Form::close() !!}
	                    </div>
	                    <div class="tab-pane" id="tab-1-2">
	                    	{!! Form::open(['action' => 'backend\UserC@updatePassword', 'class' => 'form-horizontal form-pass']) !!}
	                    		<div class="row">
	                    		<div class="col-md-offset-2 col-md-8 col-md-offset-2">
	                    		@include('backend.alumni._form_pass')
	                    		<div class="form-group">
	                    			<div class="col-sm-offset-2 col-sm-10">
	                    				<button class="btn btn-info" type="submit">Simpan</button>
	                    			</div>
	                    		</div>
	                    	</div></div>
	                    	{!! Form::close() !!}
	                   	</div>
	                  </div><br>
				</div>
			</div>
		</div>
	</div>
@stop

@section('custom_scripts')

@stop