<div class="form-group">
	<label class="col-sm-2 control-label">Nama Kategori</label>
	<div class="col-sm-4">
		{!! Form::hidden('id_kategori', null) !!}
		{!! Form::text('nama_kategori', null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'Nama Kategori' ]) !!}
	</div>
</div>

{!! Form::hidden('jenis', $jenis) !!}


<div class="form-group">
	<label class="col-sm-2 control-label">Keterangan</label>
	<div class="col-sm-10">
		{!! Form::textarea('keterangan', null, ['class' => 'form-control']) !!}
	</div>
</div>



