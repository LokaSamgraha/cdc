

<div class="form-group">
	<label class="col-sm-2 control-label">Nama Lengkap</label>
	<div class="col-sm-10">
		{!! Form::text('nama_lengkap', null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'nama lengkap' ]) !!}
	</div>
</div>

@if($user['level'] == 'admin')

<div class="form-group">
	<label class="col-sm-2 control-label"><i><strong>Validasi Data</strong></i></label>
	<div class="col-sm-10">
		{!! Form::select('is_active', ['need confirm' => 'Need Confirm', 'confirm'=> 'Confirm'], null,['class' => 'form-control ', 'required' => true, 'placeholder' => 'Pilih Status' ]) !!}
	</div>
</div>

@endif

<div class="form-group">
	<label class="col-sm-2 control-label">Tempat & Tanggal Lahir</label>
	<div class="col-sm-5">
		{!! Form::text('tempat_lahir', null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'tempat lahir' ]) !!}
	</div>
	<div class="col-sm-5">
		{!! Form::text('tgl_lahir', null, ['class' => 'form-control datepicker', 'required' => true, 'placeholder' => 'tanggal lahir' ]) !!}
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Jenis Kelamin</label>
	<div class="col-sm-10">
		{!! Form::select('jk',\Config::get('dummy.jenis_kelamin'), null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'jenis kelamin alumni' ]) !!}
	</div>
</div>


<div class="form-group">
	<label class="col-sm-2 control-label">Kontak Alumni</label>
	<div class="col-sm-5">
		{!! Form::text('no_telp', null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'nomer telepon alumni' ]) !!}
	</div>
	<div class="col-sm-5">
		{!! Form::text('no_selular', null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'nomer hp alumni' ]) !!}
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Username</label>
	<div class="col-sm-10">
		{!! Form::hidden('id', null) !!}
		{!! Form::text('username', null, ['class' => 'form-control', 'required' => true, 'placeholder' => 'name']) !!}
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Email</label>
	<div class="col-sm-10">
		{!! Form::email('email', null, ['class' => 'form-control', 'required' => true, 'placeholder' => 'email']) !!}
	</div>
</div>




@if($user['level'] == 'admin' )
<div class="form-group">
	<label class="col-sm-2 control-label">Password</label>
	<div class="col-sm-10">
		{!! Form::password('password', ['class' => 'form-control']) !!}
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Level</label>
	<div class="col-sm-10">
		{!! Form::select('level', ['admin' => 'Admin', 'anggota' => 'Anggota'], null, ['class' => 'form-control', 'required' => true]) !!}
	</div>
</div>

@endif