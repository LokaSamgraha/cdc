<div class="form-group">
	<label class="col-sm-2 control-label">Nama</label>
	<div class="col-sm-10">
		{!! Form::hidden('id_fakultas', null) !!}
		{!! Form::text('nama', null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'Nama Fakultas' ]) !!}
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Kode Fakultas</label>
	<div class="col-sm-10">
		{!! Form::text('kode_fakultas', null, ['class' => 'form-control', 'required' => true, 'placeholder' => 'Kode Fakultas']) !!}
	</div>
</div>

