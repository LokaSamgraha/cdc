<div class="form-group">
	<label class="col-sm-2 control-label">Email</label>
	<div class="col-sm-10">
		{!! Form::hidden('id_kontak', null) !!}
		{!! Form::text('email', null, ['class' => 'form-control', 'required' => true, 'placeholder' => 'email']) !!}
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label">Nomer Telepon</label>
	<div class="col-sm-10">
		{!! Form::text('no_telp', null, ['class' => 'form-control', 'required' => true, 'placeholder' => 'nomer telepon']) !!}
		
	</div>
</div>




<div class="form-group">
	<label class="col-sm-2 control-label">Tentang Kami</label>
	<div class="col-sm-10">
		{!! Form::textarea('tentang_kami', null, ['class' => 'form-control', 'required' => true, 'cols' => 300 ]) !!}
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Nama Bank & No Rekening</label>
	<div class="col-sm-5">
		{!! Form::text('nama_bank', null, ['class' => 'form-control', 'required' => true, 'placeholder' => 'Nama Bank']) !!}
		
	</div>

	<div class="col-sm-5">
		{!! Form::text('no_rek', null, ['class' => 'form-control', 'required' => true, 'placeholder' => 'Nomer Rekening']) !!}
		
	</div>
</div>


<div class="form-group">
	<label class="col-sm-2 control-label">Sosial Media * (opsional)</label>
	<div class="col-sm-3">
		{!! Form::text('fb', null, ['class' => 'form-control', 'placeholder' => 'Masukkan Link Facebook']) !!}
	</div>
	<div class="col-sm-3">
		{!! Form::text('twitter', null, ['class' => 'form-control',  'placeholder' => 'Masukkan Link Twitter']) !!}
	</div>

	<div class="col-sm-3">
		{!! Form::text('gplus', null, ['class' => 'form-control',  'placeholder' => 'Masukkan Link google plus']) !!}
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label">Alamat</label>
	<div class="col-sm-10">
		{!! Form::textarea('alamat', null, ['class' => 'form-control', 'required' => true, 'cols' => 300,'id' =>'alamat_instansi' ]) !!}
	</div>
</div>
<div class="col-sm-offset-2 col-sm-10">
	<div class="alert alert-success" style="background-color: white; color:green;">
    Silahkan posisikan marker pada map sesuai alamat kantor CDC Undiksha
</div>

</div>

<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
	<div id="map" style="width: 100%; height: 300px"></div>
	</div>
</div>

<div class="form-group">
	
	<label class="col-sm-2 control-label">Latitude & Longitude</label>
	<div class="col-sm-3">
		{!! Form::text('latitude', '-8.116387', ['class' => 'form-control' , 'id' => 'input_lat', 'required']) !!}
	</div>
	<div class="col-sm-3">
		{!! Form::text('longitude', '115.087887', ['class' => 'form-control', 'id' => 'input_lng','required' ]) !!}
	</div>

</div>

    




