@extends('partials.backend.master')

@section('page_title')
	Setting Umum
@stop


@section('custom_styles')
	<link href="{{ url('vendors/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css"/>
	<link href="{{ url('vendors/datatables/buttons.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
	<link href="{{ url('vendors/datatables/extensions/Responsive/css/dataTables.responsive.css') }}" rel="stylesheet" type="text/css"/>

	 <link href="{{ url('vendors/bootstrap-select/dist/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css"/>

@stop

@section('title_breadcrumb')
	<div class="row page-heading">
		<h1 class="page-title">Data Umum</h1>

		<ol class="breadcrumb">
			<li><a href="{{ action('backend\DashboardC@tampil') }}">Dashboard</a></li>
			<li class="active">Setting</li>
		</ol>
	</div>
@stop

@section('content')
	<div class="row">
		<div class="col-sm-12">
			<div class="ibox">
				<div class="ibox-head">
					<div class="ibox-title"> Data Umum</div>
					<div class="ibox-tools">
						<a class="ibox-collapse">
							<i class="fa fa-angle-down"></i></a><a class="fullscreen-link"><i class="fa fa-arrows-alt"></i>
						</a>
					</div>
				</div>
				<div class="ibox-body">
					<ul class="nav nav-tabs">
	                    
	                    <li class="active"><a href="#tab-1-2" data-toggle="tab"><i class="fa fa-plus-circle"></i> Data Umum</a></li>
	                  </ul>
	                  <div class="tab-content">
	                   
	                    <div class="tab-pane fade in active" id="tab-1-2">
	                    	{!! Form::model($kontak,['action' => 'backend\KontakC@update', 'class' => 'form-horizontal']) !!}
	                    		@include('backend.kontak._form')
	                    		<div class="form-group">
	                    			<div class="col-sm-offset-2 col-sm-10">
	                    				<button class="btn btn-info" type="submit">Simpan</button>
	                    			</div>
	                    		</div>
	                    	{!! Form::close() !!}
	                   	</div>
	                  </div><br>
				</div>
			</div>
		</div>
	</div>
@stop

@section('custom_scripts')
	<script src="{{ url('vendors/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/dataTables.buttons.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/buttons.flash.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/jszip.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/pdfmake.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/vfs_fonts.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/buttons.html5.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/buttons.print.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/colVis.min.js') }}" type="text/javascript"></script>

	 <script src="{{ url('vendors/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>


	<script>
		$(document).ready(function(){
			var dataTable = $('#dataTable').DataTable({
		        dom: 'Bfrtip',
		        lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
		        buttons: [
		        	'pageLength',
	                'colvis',
	                {
	                    extend: 'pdf',
	                    exportOptions: {
	                        columns: ':visible'
	                    }
	                },
	                {
	                    extend: 'excel',
	                    exportOptions: {
	                        columns: ':visible'
	                    }
	                }
	            ],
	            columnDefs: [ {
	                targets: -1,
	                visible: false
	            } ],
				responsive: true,
				processing: true,
				serverSide: true,
				ajax: '{{ action('backend\InformasiC@getData') }}',
				columns: [
					{ data: 'id_informasi', name: 'id_informasi' },
					{ data: 'link_title', name: 'link_title' },
					{ data: 'create_at', name: 'create_at' },
					{ data: 'action', name: 'action' },
				]
			});
		});
	</script>


	<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyA49B12MUJIU6LL3_V3RDS81lMtEvmvRJk&language=id"></script>

    <script>

    	$('a[href="#tab-1-2"]').on('shown.bs.tab', function(e)
    {
        google.maps.event.trigger(map, 'resize');
    });
    

        var myCenter = new google.maps.LatLng($('#input_lat').val(), $('#input_lng').val());
        var marker;

        function initialize()
        {
            var mapProp = {
                center:myCenter,
                zoom:12,
                mapTypeId:google.maps.MapTypeId.ROADMAP
            };

            var map=new google.maps.Map(document.getElementById("map"),mapProp);

            var marker=new google.maps.Marker({
                position:myCenter,
                draggable:true,
            });
            var geocoder = new google.maps.Geocoder;


            marker.setMap(map);

            google.maps.event.addListener(marker, 'dragend', function() 
            {
                $('#input_lat').val(this.getPosition().lat());
                $('#input_lng').val(this.getPosition().lng());

                getcodeLatLng(geocoder, map, this.getPosition().lat(), this.getPosition().lng());

            });
        }

        function getcodeLatLng(geocoder, map, lat, lng)
        {   
            var latlng = {lat : parseFloat(lat), lng : parseFloat(lng)};
            geocoder.geocode({'location': latlng}, function(results, status) {
                    if (status === 'OK') {
                        if (results[1]) {

                            // infowindow.setContent(results[1].formatted_address);
                            $('#alamat_instansi').val(results[1].formatted_address);


                        } else {
                            window.alert('No results found');
                        }
                      } else {
                            window.alert('Geocoder failed due to: ' + status);
                      }
            });

        }

        google.maps.event.addDomListener(window, 'load', initialize);


    </script>  
@stop