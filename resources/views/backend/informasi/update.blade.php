@extends('partials.backend.master')

@section('page_title')
	Informasi
@stop


@section('custom_styles')

	
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
	 <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">


@stop

@section('title_breadcrumb')
	<div class="row page-heading">
		<h1 class="page-title">Data Informasi</h1>

		<ol class="breadcrumb">
			<li><a href="{{ action('backend\DashboardC@tampil') }}">Dashboard</a></li>
			<li class="active">Informasi</li>
		</ol>
	</div>
@stop

@section('content')
	<div class="row">
		<div class="col-sm-12">
			<div class="ibox">
				<div class="ibox-head">
					<div class="ibox-title">Ubah Data Informasi</div>
					<div class="ibox-tools">
						<a class="ibox-collapse">
							<i class="fa fa-angle-down"></i></a><a class="fullscreen-link"><i class="fa fa-arrows-alt"></i>
						</a>
					</div>
				</div>
				<div class="ibox-body">
					<ul class="nav nav-tabs">
	                    <li class="active"><a href="#tab-1-1" data-toggle="tab"><i class="fa fa-pencil"></i> Ubah Data</a></li>
	                   
	                  </ul>
	                  <div class="tab-content">
	                    <div class="tab-pane fade in active" id="tab-1-1">
	                    	{!! Form::model($target,['action' => 'backend\InformasiC@update', 'class' => 'form-horizontal form-validate','files' => true]) !!}

	                    		@include('backend.informasi._form')


	                    		<div class="form-group">
	                    			<div class="col-sm-offset-2 col-sm-10">
	                    				<button class="btn btn-info" type="submit">Update</button>
	                    			</div>
	                    		</div>
	                    	{!! Form::close() !!}
	                    </div>
	                   
	                  </div>
	                  <br>
	                  <a href="{{ action('backend\InformasiC@tampil') }}" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp;Kembali</a>
				</div>
			</div>
		</div>
	</div>
@stop

@section('custom_scripts')
	

	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/fileinput.min.js"></script>
	 <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>



	<script>
		$(document).ready(function(){
			$('#summernote').summernote({
				height: 500 
			});
			
			$("#bootstrap-input").fileinput({
			    'showUpload': false
			});


		});
	</script>

@stop