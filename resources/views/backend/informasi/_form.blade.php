<div class="form-group">
	<label class="col-sm-2 control-label">Judul Informasi</label>
	<div class="col-sm-10">
		{!! Form::hidden('id_informasi', null) !!}
		{!! Form::text('title', null, ['class' => 'form-control', 'required' => true, 'placeholder' => 'Judul Informasi']) !!}
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Kategori Informasi</label>
	<div class="col-sm-10">
		{!! Form::select('id_kategori', \App\Kategori::where('jenis','informasi')->pluck('nama_kategori','nama_kategori') , null, ['class' => 'form-control', 'required' => true, 'placeholder' => 'Pilih Kategori']) !!}
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Isi Informasi</label>
	<div class="col-sm-10">
		{!! Form::textarea('description', null, ['class' => 'form-control', 'required' => true, 'placeholder' => 'Deskripsi Informasi','id' => 'summernote']) !!}
	</div>
</div>

@if(isset($target))
	@if($target->image !== NULL)
		<div class="form-group">
			<label class="col-sm-2 control-label"></label>

			<div class="col-sm-10">
				<img class="img img-thumbnail" style="height:120px;"  src="{{ url('img') }}/{{ $target->image }}"> 
			</div>

		</div>
	@endif
@endif
<div class="form-group">
	<label class="col-sm-2 control-label">Thumbnail</label>
	<div class="col-sm-10">
		{!! Form::file('image',['id' => 'bootstrap-input']) !!}
	</div>
</div>