@extends('partials.backend.master')

@section('page_title')
	Lowongan
@stop


@section('custom_styles')
	<link href="{{ url('vendors/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css"/>
	<link href="{{ url('vendors/datatables/buttons.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
	<link href="{{ url('vendors/datatables/extensions/Responsive/css/dataTables.responsive.css') }}" rel="stylesheet" type="text/css"/>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />

	 <link href="{{ url('vendors/bootstrap-select/dist/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css"/>

	 <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">

@stop

@section('title_breadcrumb')
	<div class="row page-heading">
		<h1 class="page-title">Data Lowongan</h1>

		<ol class="breadcrumb">
			<li><a href="{{ action('backend\DashboardC@tampil') }}">Dashboard</a></li>
			<li class="active">Lowongan</li>
		</ol>
	</div>
@stop

@section('content')
	<div class="row">
		<div class="col-sm-12">
			<div class="ibox">
				<div class="ibox-head">
					<div class="ibox-title">Ubah Data Lowongan</div>
					<div class="ibox-tools">
						<a class="ibox-collapse">
							<i class="fa fa-angle-down"></i></a><a class="fullscreen-link"><i class="fa fa-arrows-alt"></i>
						</a>
					</div>
				</div>
				<div class="ibox-body">
					<ul class="nav nav-tabs">
	                    <li class="active"><a href="#tab-1-1" data-toggle="tab"><i class="fa fa-pencil"></i> Ubah Data</a></li>
	                   
	                  </ul>
	                  <div class="tab-content">
	                    <div class="tab-pane fade in active" id="tab-1-1">
	                    	{!! Form::model($target,['action' => 'backend\LowonganC@update', 'class' => 'form-horizontal form-validate']) !!}
	                    		@include('backend.lowongan._form')
	                    		<div class="form-group">
	                    			<div class="col-sm-offset-2 col-sm-10">
	                    				<button class="btn btn-info" type="submit">Update</button>
	                    			</div>
	                    		</div>
	                    	{!! Form::close() !!}
	                    </div>
	                   
	                  </div>
	                  <br>
	                  <a href="{{ action('backend\LowonganC@tampil') }}" class="btn btn-default"><i class="fa fa-arrow-left"></i> &nbsp;Kembali</a>
				</div>
			</div>
		</div>
	</div>
@stop

@section('custom_scripts')
	<script src="{{ url('vendors/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/dataTables.buttons.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/buttons.flash.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/jszip.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/pdfmake.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/vfs_fonts.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/buttons.html5.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/buttons.print.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/colVis.min.js') }}" type="text/javascript"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/fileinput.min.js"></script>

	 <script src="{{ url('vendors/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>

	 <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>


	<script>
		$(document).ready(function(){

			$('#summernote').summernote({
				height:500
			});

			$('.selectpicker').selectpicker({
                  
                });

			$("#bootstrap-input").fileinput({
			    'showUpload': false
			});


			var dataTable = $('#dataTable').DataTable({
		        dom: 'Bfrtip',
		        lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
		        buttons: [
		        	'pageLength',
	                'colvis',
	                {
	                    extend: 'pdf',
	                    exportOptions: {
	                        columns: ':visible'
	                    }
	                },
	                {
	                    extend: 'excel',
	                    exportOptions: {
	                        columns: ':visible'
	                    }
	                }
	            ],
	            columnDefs: [ {
	                targets: -1,
	                visible: false
	            } ],
				responsive: true,
				processing: true,
				serverSide: true,
				ajax: '{{ action('backend\PerusahaanC@getData') }}',
				columns: [
					{ data: 'id_perusahaan', name: 'id_perusahaan' },
					{ data: 'logo', name: 'logo' },
					{ data: 'name', name: 'name' },
					{ data: 'description', name: 'description' },
					{ data: 'action', name: 'action' },
				]
			});
		});
	</script>
@stop