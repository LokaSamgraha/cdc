<div class="form-group">
	<label class="col-sm-2 control-label">Nama Lowongan Pekerjaan</label>
	<div class="col-sm-10">
		{!! Form::hidden('id_lowongan', null) !!}
		{!! Form::text('nama_lowongan', null, ['class' => 'form-control', 'required' => true, 'placeholder' => 'Lowongan Pekerjaan']) !!}
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label">Perusahaan Mitra</label>
	<div class="col-sm-10">
		{!! Form::select('id_perusahaan',$list_perusahaan, null, ['class' => 'form-control', 'required' => true, 'placeholder' => 'Pilih Perusahaan Mitra']) !!}
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label">Jurusan</label>
	<div class="col-sm-10">
		{!! Form::select('nama_jurusan[]', $list_jurusan, isset($jurusan_ar) ? $jurusan_ar : null, ['class' => 'selectpicker form-control', 'required' => true, 'multiple' ]) !!}
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label">Jenjang </label>
	<div class="col-sm-10">
		{!! Form::select('jenjang',$list_jenjang ,null, ['class' => 'form-control', 'required' => true, 'placeholder' => 'Pilih Jenjang ']) !!}
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label">Jenis Pekerjaan</label>
	<div class="col-sm-10">
		{!! Form::select('jenis_pekerjaan', $list_jenis_pekerjaan,null, ['class' => 'form-control', 'required' => true, 'placeholder' => 'Pilih Jenis Pekerjaan']) !!}
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label">Penempatan</label>
	<div class="col-sm-10">
		{!! Form::text('penempatan', null, ['class' => 'form-control', 'required' => true, 'placeholder' => 'Penempatan Pekerjaan']) !!}
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Posisi Tersedia</label>
	<div class="col-sm-10">
		{!! Form::number('posisi_tersedia', null, ['class' => 'form-control', 'required' => true, 'placeholder' => 'Posisi Tersedia']) !!}
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Kualifikasi</label>
	<div class="col-sm-10">
		{!! Form::textarea('kualifikasi', null, ['class' => 'form-control', 'required' => true,'id'=>'summernote', 'cols' => 300 ]) !!}
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label">Tanggal Buka & tutup</label>
	<div class="col-sm-5">
		{!! Form::text('tgl_buka', null, ['class' => 'form-control datepicker', 'required' => true, 'placeholder' => 'Tanggal Buka']) !!}
	</div>
	<div class="col-sm-5">
		{!! Form::text('tgl_tutup', null, ['class' => 'form-control datepicker', 'required' => true, 'placeholder' => 'Tanggal Tutup']) !!}
	</div>
</div>



