<div class="form-group">
	<label class="col-sm-2 control-label">Nama</label>
	<div class="col-sm-10">
		{!! Form::hidden('id_lsp', null) !!}
		{!! Form::text('nama_lsp', null, ['class' => 'form-control', 'required' => true, 'placeholder' => 'nama lsp']) !!}
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Kategori LSP</label>
	<div class="col-sm-10">
		{!! Form::select('id_kategori', \App\Kategori::where('jenis','lsp')->pluck('nama_kategori','nama_kategori') , null, ['class' => 'form-control', 'required' => true,  'placeholder' => 'Pilih Kategori']) !!}
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Deskripsi</label>
	<div class="col-sm-10">
		{!! Form::textarea('description_lsp', null, ['id' => 'description_lsp', 'class' => 'form-control', 'required' => true]) !!}
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Harga Pendaftaran</label>
	<div class="col-sm-5">
		{!! Form::number('harga_pendaftaran', null, ['class' => 'form-control', 'required' => true, 'placeholder' => 'Harga Pendaftaran']) !!}
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Harga Training</label>
	<div class="col-sm-5">
		{!! Form::number('harga', null, ['class' => 'form-control', 'required' => true, 'placeholder' => 'Harga Training']) !!}
	</div>
</div>



@if(isset($target))
	@if($target->image !== NULL)
		<div class="form-group">
			<label class="col-sm-2 control-label"></label>
			<div class="col-sm-10">
				<img class="img img-thumbnail" style="height:120px;"  src="{{ url('img') }}/{{ $target->image }}"> 
			</div>
		</div>
	@endif
@endif

<div class="form-group">
	<label class="col-sm-2 control-label">Image</label>
	<div class="col-sm-6">
		{!! Form::file('upload_image', ['id' => 'upload_image']) !!}
	</div>
</div>