@extends('partials.backend.master')

@section('page_title')
	Detail Pendaftaran LSP
@stop


@section('custom_styles')

@stop

@section('title_breadcrumb')
	<div class="row page-heading">
		<h1 class="page-title">Data LSP</h1>

		<ol class="breadcrumb">
			<li><a href="{{ action('backend\DashboardC@tampil') }}">Dashboard</a></li>
			<li class="active"> LSP</li>
		</ol>
	</div>
@stop

@section('content')
	<div class="row">
		<div class="col-sm-12">
			<div class="ibox">
				<div class="ibox-head">
					<div class="ibox-title">Detail Pendaftaran LSP</div>
					<div class="ibox-tools">
						<a class="ibox-collapse">
							<i class="fa fa-angle-down"></i></a><a class="fullscreen-link"><i class="fa fa-arrows-alt"></i>
						</a>
					</div>
				</div>
				<div class="ibox-body">
					<ul class="nav nav-tabs">
	                    <li class="active"><a href="#tab-1-1" data-toggle="tab"><i class="fa fa-line-chart"></i> Data </a></li>
	                  </ul>
	                  <div class="tab-content">
	                  	
	                    <div class="tab-pane fade in active" id="tab-1-1">

	                    	<div class="row">
	                    		<div class="col-md-offset-2 col-md-10">
	                    	<table class="table table-hover nowrap"  style="width: 100%;">
	                    	<div class="alert alert-info">
				                  	<p><strong> Pemberitahuuan !!!</strong></p>
				                  	<p>Status Pembayaran terdiri dari : </p> 
				                  	<ul>
				                  		<li><label class="label label-warning"> Belum Bayar </label>  : Pendaftar lsp belum mengupload bukti tranfer ke sistem </li>
				                  		<li><label class="label label-info">Menunggu Validasi </label> : Pendaftar lsp sudah mengupload bukti transfer tapi belum di validasi oleh Admin</li>
				                  		<li><label class="label label-success">Sudah Bayar </label>: Bukti transfer sudah divalidasi oleh admin dan sudah sah mendaftar di LSP</li>

				                  	</ul>
				                  	<p>Proses validasi data pembayaran paling lambat dilakukan dalam 2 x 24 jam. Hubungi administrator jika dalam kurun waktu tersebut belum di validasi.</p>

				                  	</div>

	                    			<tr>
	                    				<td>Nama LSP</td>
	                    				<td>{{ $lsp->nama_lsp}}</td>
	                    			</tr>
	                    			<tr>
		                				<td>Kategori LSP</td>
		                				<td>{{ $lsp->id_kategori }}</td>
		                			</tr>
		                			
		                			<tr>
		                				<td>Harga Pendaftaran</td>
		                				<td>Rp. {{ number_format($lsp->harga_pendaftaran,2) }}</td>
		                				
		                			</tr>

		                			<tr>
		                				<td>Harga Training</td>
		                				<td>Rp. {{ number_format($lsp->harga,2) }}</td>
		                				
		                			</tr>

		                			<tr>
		                				<td><i><strong>Total Harga</strong></i></td>
		                				<td><i><strong>Rp. {{ number_format($lsp->harga + $lsp->harga_pendaftaran ,2) }}</strong></i></td>
		                				
		                			</tr>

	                    			<tr>
	                    				<td>Tanggal Pendaftaran</td>
	                    				<td>{{ \Carbon\Carbon::parse($lsp->created_at)->format('d M Y') }}</td>
	                    			</tr>
	                    			<tr>
	                    				<td>Status Pembayaran </td>
		                				<td>
		                    				@if($lsp->status_pembayaran == 'Belum Bayar')
		                    				<label class="label label-warning">{{ $lsp->status_pembayaran }}  </label>
		                    				@elseif($lsp->status_pembayaran == 'Menunggu Validasi')
		                    				<label class="label label-info">{{ $lsp->status_pembayaran }}  </label>
		                    				@else
		                    				<label class="label label-success">{{ $lsp->status_pembayaran }}  </label>
		                    				@endif
		                				</td>
		                			</tr>


		                			<tr>
		                				<td>Bukti Transfer </td>
		                				@if($lsp->nama_file != NULL)

		                				<td> <a href="{{ url('files') }}/{{ $lsp->nama_file }}"><i class="fa fa-file"></i>&nbsp;&nbsp;Lihat Bukti transfer</a></td>

		                				@else

		                				<td>Belum ada File yang diunggah</td>

		                				@endif

		                			</tr>
	                    			
	                    		
	                    			
	                    			
	                    			
	                    		
	                    	</table>
	                    	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"> <i class="fa fa-arrow-circle-o-up"></i> Upload Bukti Transfer</button>



	                    	</div>
	                    		

	                    	</div>
	                    	<a href="{{ action('backend\LspC@tampilLSPdiIkuti') }}" class="btn btn-default"><i class="fa fa-arrow-left"></i> Kembali</a>
	                    </div>
	                 
	                  </div><br>
					

						<!-- Modal -->
						<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						  <div class="modal-dialog" role="document">
						    <div class="modal-content">
						      <div class="modal-header">
						        <h5 class="modal-title" id="exampleModalLabel">Upload File</h5>
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						          <span aria-hidden="true">&times;</span>
						        </button>
						      </div>
						      <div class="modal-body">
						      	{!! Form::open(['action' => 'backend\LspC@uploadBukti', 'class' => 'form-horizontal form-validate','files' => true]) !!}

						      	<input type="hidden" name="id_pendaftar_lsp" value="{{ $lsp->id_pendaftar_lsp }}">
						      	<div class="form-group">
									<label class="col-sm-3 control-label">Berkas <br><small> (* jpg, jpeg, <br> max 2 mb)</small></label>
									<div class="col-sm-9">
										{!! Form::file('nama_file', ['id' => 'nama_file']) !!}
									</div>
								</div>
						      	
						        
						      </div>
						      <div class="modal-footer">
						        <a  class="btn btn-default" data-dismiss="modal">Close</a>
						        <button type="submit" class="btn btn-primary">Upload</button>
						       {!! Form::close() !!}
						      </div>
						    </div>
						  </div>
						</div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('custom_scripts')
	
@stop