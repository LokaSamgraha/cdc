@extends('partials.backend.master')

@section('page_title')
	LSP
@stop


@section('custom_styles')
	<link href="{{ url('vendors/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css"/>
	<link href="{{ url('vendors/datatables/buttons.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
	<link href="{{ url('vendors/datatables/extensions/Responsive/css/dataTables.responsive.css') }}" rel="stylesheet" type="text/css"/>
	<link href="{{ url('vendors/bootstrap-select/dist/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css"/>
  	<link href="{{ url('vendors/jquery-confirm/jquery-confirm.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ url('vendors/bootstrap-fileinput/css/fileinput.css') }}" rel="stylesheet" type="text/css"/>
	<link href="{{ url('vendors/summernote/summernote.css') }}" rel="stylesheet" type="text/css"/>
@stop

@section('title_breadcrumb')
	<div class="row page-heading">
		<h1 class="page-title">Data LSP</h1>

		<ol class="breadcrumb">
			<li><a href="{{ action('backend\DashboardC@tampil') }}">Dashboard</a></li>
			<li class="active">LSP</li>
		</ol>
	</div>
@stop

@section('content')
	<div class="row">
		<div class="col-sm-12">
			<div class="ibox">
				<div class="ibox-head">
					<div class="ibox-title">Tambah Data LSP</div>
					<div class="ibox-tools">
						<a class="ibox-collapse">
							<i class="fa fa-angle-down"></i></a><a class="fullscreen-link"><i class="fa fa-arrows-alt"></i>
						</a>
					</div>
				</div>
				<div class="ibox-body">
					<ul class="nav nav-tabs">
	                    <li class="active"><a href="#tab-1-1" data-toggle="tab"><i class="fa fa-line-chart"></i> Data</a></li>
	                    <li><a href="#tab-1-2" data-toggle="tab"><i class="fa fa-plus-circle"></i> Add New Data</a></li>
	                  </ul>
	                  <div class="tab-content">
	                    <div class="tab-pane fade in active" id="tab-1-1">
	                    	<table class="table table-hover nowrap" id="dataTable" style="width: 100%;">
	                    		<thead>
	                    			<tr>
	                    				<th>#</th>
	                    				<th>Thumbnail</th>
	                    				<th>Name</th>
	                    				<th>Deskripsi</th>
	                    				<th>Last Update</th>
	                    				<th width="15%;">Opsi</th>
	                    			</tr>
	                    		</thead>
	                    		<tbody>
	                    		</tbody>
	                    	</table>
	                    </div>
	                    <div class="tab-pane" id="tab-1-2">
	                    	{!! Form::open(['action' => 'backend\LspC@simpan', 'class' => 'form-horizontal form-validate','files' => true]) !!}
	                    		@include('backend.lsp._form')
	                    		<div class="form-group">
	                    			<div class="col-sm-offset-2 col-sm-10">
	                    				<button class="btn btn-info" type="submit">Simpan</button>
	                    			</div>
	                    		</div>
	                    	{!! Form::close() !!}
	                   	</div>
	                  </div><br>
				</div>
			</div>
		</div>
	</div>
@stop

@section('custom_scripts')
	<script src="{{ url('vendors/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/dataTables.buttons.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/buttons.flash.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/jszip.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/pdfmake.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/vfs_fonts.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/buttons.html5.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/buttons.print.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/datatables/colVis.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/jquery-confirm/jquery-confirm.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/bootstrap-fileinput/js/fileinput.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('vendors/summernote/summernote.min.js') }}" type="text/javascript"></script>

	<script>
		$(document).ready(function(){
			$('#description_lsp').summernote({
				height: 300 
			});

			$("#upload_image").fileinput({
			    'showUpload': false
			});

			var dataTable = $('#dataTable').DataTable({
		        dom: 'Bfrtip',
		        lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
		        buttons: [
		        	'pageLength',
	                'colvis',
	                {
	                    extend: 'pdf',
	                    exportOptions: {
	                        columns: ':visible'
	                    }
	                },
	                {
	                    extend: 'excel',
	                    exportOptions: {
	                        columns: ':visible'
	                    }
	                }
	            ],
	            columnDefs: [ {
	                targets: -1,
	                visible: false
	            } ],
				responsive: true,
				processing: true,
				serverSide: true,
				ajax: '{{ action('backend\LspC@getData') }}',
				columns: [
					{ data: 'DT_Row_Index', orderable: false, searchable: false },
					{ 
						data: 'image',
					 },
					{ data: 'nama_lsp' },
					{ data: 'desc_word' },
					{ data: 'updated_at' },
					{ data: 'action', name: 'action' },
				]
			});

		$('#dataTable').on('click', '.btn-danger[data-remote]',function(){
            var idAndName = $(this).data('remote');

            var split = idAndName.split(':;');

            var id = split[0];
            var name = split[1];

              $.confirm({
                  icon: 'fa fa-trash',
                  title: 'Hapus Data',
                  content: 'Hapus Data LSP : '+ name +'?',
                  columnClass: 'medium',
                  theme: 'light',
                  autoClose: 'batal|20000',
                  buttons: {
                      deletePengguna: {
                          text: 'Hapus',
                          btnClass: 'btn-red',
                          action: function () {
                            $.ajax({
                              type: "POST",
                              url: "{{ action('backend\LspC@hapus') }}",
                              data: {
                                trigger : id,
                                _token : "{{ csrf_token() }}"
                              },
                              success: function(data){
                               dataTable.ajax.reload();

                               	 Command: toastr["success"]("Berhasil Dihapus", "Pemberitahuan")

									toastr.options = {
									  "closeButton": true,
									  "debug": false,
									  "newestOnTop": false,
									  "progressBar": false,
									  "positionClass": "toast-top-right",
									  "preventDuplicates": false,
									  "onclick": null,
									  "showDuration": "300",
									  "hideDuration": "1000",
									  "timeOut": "5000",
									  "extendedTimeOut": "1000",
									  "showEasing": "swing",
									  "hideEasing": "linear",
									  "showMethod": "fadeIn",
									  "hideMethod": "fadeOut"
									}
                               
                              }
                            });
                          }
                      },
                      batal: function () {

                      }
                  }
              });
            });
		});
	</script>
@stop