@extends('partials.backend.master')

@section('page_title')
	Detail Pendaftaran LSP
@stop


@section('custom_styles')

@stop

@section('title_breadcrumb')
	<div class="row page-heading">
		<h1 class="page-title">Data LSP</h1>

		<ol class="breadcrumb">
			<li><a href="{{ action('backend\DashboardC@tampil') }}">Dashboard</a></li>
			<li class="active"> LSP</li>
		</ol>
	</div>
@stop

@section('content')
	<div class="row">
		<div class="col-sm-12">
			<div class="ibox">
				<div class="ibox-head">
					<div class="ibox-title">Detail Pendaftaran LSP</div>
					<div class="ibox-tools">
						<a class="ibox-collapse">
							<i class="fa fa-angle-down"></i></a><a class="fullscreen-link"><i class="fa fa-arrows-alt"></i>
						</a>
					</div>
				</div>
				<div class="ibox-body">
					<ul class="nav nav-tabs">
	                    <li class="active"><a href="#tab-1-1" data-toggle="tab"><i class="fa fa-line-chart"></i> Data </a></li>
	                  </ul>
	                  <div class="tab-content">
	                  	
	                    <div class="tab-pane fade in active" id="tab-1-1">

	                    	<div class="row">
	                    		<div class="col-md-offset-2 col-md-10">
	                    	<table class="table table-hover nowrap"  style="width: 100%;">
	                    		
	                    			<tr>
	                    				<td>Nama Lengkap </td>
	                    				<td>{{ $lsp->nama_lengkap }}</td>
	                    			</tr>

	                    			<tr>
	                    				<td>Email </td>
	                    				<td>{{ $lsp->email }}</td>
	                    			</tr>

	                    			<tr>
	                    				<td>No telepon </td>
	                    				<td>{{ $lsp->no_telp }}</td>
	                    			</tr>

	                    			<tr>
	                    				<td>No Selular </td>
	                    				<td>{{ $lsp->no_selular }}</td>
	                    			</tr>

	                    			<tr>
	                    				<td>Nama LSP</td>
	                    				<td>{{ $lsp->nama_lsp }}</td>
	                    			</tr>

	                    			<tr>
		                				<td>Kategori LSP</td>
		                				<td>{{ $lsp->id_kategori }}</td>
		                			</tr>
		                			<tr>
		                				<td>Harga Pendaftaran</td>
		                				<td>Rp. {{ number_format($lsp->harga_pendaftaran,2) }}</td>
		                				
		                			</tr>

		                			<tr>
		                				<td>Harga Training</td>
		                				<td>Rp. {{ number_format($lsp->harga,2) }}</td>
		                				
		                			</tr>

		                			<tr>
		                				<td><i><strong>Total Harga</strong></i></td>
		                				<td><i><strong>Rp. {{ number_format($lsp->harga + $lsp->harga_pendaftaran ,2) }}</strong></i></td>
		                				
		                			</tr>


	                    			<tr>
	                    				<td>Tanggal Pendaftaran</td>
	                    				<td>{{ \Carbon\Carbon::parse($lsp->created_at)->format('d M Y') }}</td>
	                    			</tr>
	                    			<tr>
	                    				<td>Status Pembayaran </td>
		                				<td>
		                    				@if($lsp->status_pembayaran == 'Belum Bayar')
		                    				<label class="label label-warning">{{ $lsp->status_pembayaran }}  </label>
		                    				@elseif($lsp->status_pembayaran == 'Menunggu Validasi')
		                    				<label class="label label-info">{{ $lsp->status_pembayaran }}  </label>
		                    				@else
		                    				<label class="label label-success">{{ $lsp->status_pembayaran }}  </label>
		                    				@endif
		                				</td>
		                			</tr>


		                			<tr>
		                				<td>Bukti Transfer </td>
		                				@if($lsp->nama_file != NULL)

		                				<td> <a href="{{ url('files') }}/{{ $lsp->nama_file }}"><i class="fa fa-file"></i>&nbsp;&nbsp;Lihat Bukti transfer</a></td>

		                				@else

		                				<td>Belum ada File yang diunggah</td>

		                				@endif

		                			</tr>
	                    			
	                    		
	                    	</table>

	                    	<div class="alert alert-info">

	                    	<p>
	                    		<strong> Pemberitahuan !!! </strong><br>
	                    		Validasi data bukti transfer dengan klik <i>lihat bukti transfer</i> , kemudian ubah status dibawah ini menjadi <i> Sudah Bayar </i>. <small>(* Jika Status masih Belum Bayar berarti bukti transfer belum diupload oleh pendaftar )</small>
	                    	</p>
	                    		
	                    	</div>

	                    {!! Form::open(['action' => 'backend\LspC@updateStatusBerkas', 'class' => 'form-horizontal form-validate']) !!}
	                    	<div class="form-group">
								<label class="col-sm-2 control-label">Status Pembayaran</label>
								<div class="col-sm-5">
									{!! Form::hidden('id_pendaftar_lsp',$lsp->id_pendaftar_lsp )!!}
									{!! Form::select('status_pembayaran', ['Belum Bayar'=>'Belum Bayar','Menunggu Validasi'=>'Menunggu Validasi','Sudah Bayar' => 'Sudah Bayar'], $lsp->status_pembayaran, ['class' => 'form-control', 'required' => true,  'placeholder' => 'Pilih Status Pembayaran']) !!}
									
								</div>
							</div>
	                    	<button type="submit" class="btn btn-primary"> <i class="fa fa-arrow-circle-o-up"></i> Simpan</button>

	                    {!! Form::close() !!}



	                    	</div>
	                    		

	                    	</div>
	                    	<a href="javascript:history.back()" class="btn btn-default"><i class="fa fa-arrow-left"></i> Kembali</a>
	                    </div>
	                 
	                  </div>
					

						
				</div>
			</div>
		</div>
	</div>
@stop

@section('custom_scripts')
	
@stop