@extends('partials.backend.master')

@section('page_title')
	Dashboard
@stop

@section('custom_styles')
 	<link href="{{ url('vendors/line-awesome/css/line-awesome.min.css') }}" rel="stylesheet"/>
    <link href="{{ url('vendors/themify-icons/css/themify-icons.css') }}" rel="stylesheet"/>
@stop

@section('title_breadcrumb')
  <div class="row page-heading">
    <h1 class="page-title">Dashboard</h1>

    <ol class="breadcrumb">
      <li><a href="#">Dashboard</a></li>
      <li class="active">admin</li>
    </ol>
  </div>
@stop

@section('content')
	<div class="row">


			<div class="col-md-6">
              <div class="ibox">
                <div class="ibox-body bg-green">
                  <div class="d-flex justify-content-between m-b-20">
                    <div class="text-white">
                      <h3 class="m-0">Statistik Lowongan</h3>
                      <div>Statistik Lowongan kerja Setiap Tahun</div>
                    </div>
                   
                  </div>
                  <div>
                    <canvas id="line_chart" style="height:260px;"></canvas>
                  </div>
                </div>
                
              </div>
          </div>

          <div class="col-md-3 col-sm-6">
              <div class="ibox">
                <div class="ibox-body">
                  <h2 class="m-b-5">{{ $Cperusahaan }}</h2>
                  <div class="text-muted">Perusahaan Mitra</div><i class="ti-home widget-stat-icon text-success"></i>
                </div>
              </div>
            </div>
            <div class="col-md-3 col-sm-6">
              <div class="ibox">
                <div class="ibox-body">
                  <h2 class="m-b-5">{{ $Cbursa }}</h2>
                  <div class="text-muted">Bursa Kerja</div><i class="ti-wallet widget-stat-icon text-success"></i>
                </div>
              </div>
            </div>
            <div class="col-md-3 col-sm-6">
              <div class="ibox">
                <div class="ibox-body">
                  <h2 class="m-b-5">{{ $Canggota }}</h2>
                  <div class="text-muted">Anggota CDC</div><i class="ti-id-badge widget-stat-icon text-success"></i>
                </div>
              </div>
            </div>

            <div class="col-md-3 col-sm-6">
              <div class="ibox">
                <div class="ibox-body">
                  <h2 class="m-b-5">{{ $Calumni }}</h2>
                  <div class="text-muted">Alumni</div><i class="ti-user widget-stat-icon text-success"></i>
                </div>
              </div>
            </div>

          



		


	</div>
@stop

@section('custom_scripts')

	<script src="{{ url('vendors/chart.js/dist/Chart.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('vendors/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('vendors/jquery-sparkline/dist/jquery.sparkline.min.js" type="text/javascript') }}"></script>
    {{-- <script src="{{ url('js/scripts/dashboard_1_demo.js') }}" type="text/javascript"></script> --}}

    <script type="text/javascript">
      
      $(function(){
             var lineData = {
        labels: {{ $year_js }},
        datasets: [
            {
                label: "Data 2",
                borderColor: 'rgba(213,217,219, 1)',
                pointBorderColor: "#fff",
                data: {{ $lowongan_js }},
                borderWidth: 4,
                pointBorderWidth: 2,
                fill: false,
                //lineTension: .1
            }
        ]
    };
    var lineOptions = {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
            display: false
        },
        scales: {
              xAxes: [{
                  gridLines: {
                      display: false,
                      color: 'rgba(255,255,255,.3)',
                  },
                  ticks: {
                      fontColor: '#eee'
                  }
              }],
              yAxes: [{
                  gridLines: {
                      color: 'rgba(255,255,255,.3)'
                  },
                  ticks: {
                      fontColor: '#eee'
                  }
              }]
        },
    };
    var ctx = document.getElementById("line_chart").getContext("2d");
    new Chart(ctx, {type: 'line', data: lineData, options: lineOptions}); 
      });
    </script>
@stop