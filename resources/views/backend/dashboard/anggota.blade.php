@extends('partials.backend.master')

@section('page_title')
	Dashboard
@stop

@section('custom_styles')
 	<link href="{{ url('vendors/line-awesome/css/line-awesome.min.css') }}" rel="stylesheet"/>
    <link href="{{ url('vendors/themify-icons/css/themify-icons.css') }}" rel="stylesheet"/>
@stop

@section('title_breadcrumb')
  <div class="row page-heading">
    <h1 class="page-title">Dashboard</h1>

    <ol class="breadcrumb">
      <li><a href="#">Dashboard</a></li>
      <li class="active">anggota</li>
    </ol>
  </div>
@stop

@section('content')
	<div class="row">
    <div class="col-md-12">
            <div class="social-widget social-widget-facebook m-r-20 m-b-20">
              <div class="social-widget-header"><i class="fa fa-user"></i></div>
              <a href="{{ action('backend\UserC@tampilProfilAnggota') }}"><div class="social-widget-count">Profil</div></a>
            </div>
            <div class="social-widget social-widget-twitter m-r-20 m-b-20">
              <div class="social-widget-header"><i class="fa fa-arrow-circle-up"></i></div>
              <a href="{{ action('backend\LspC@tampilLSPdiIkuti') }}"><div class="social-widget-count">LSP</div></a>
            </div>
           
          </div>


	</div>
@stop

@section('custom_scripts')

	<script src="{{ url('vendors/chart.js/dist/Chart.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('vendors/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('vendors/jquery-sparkline/dist/jquery.sparkline.min.js" type="text/javascript') }}"></script>
    <script src="{{ url('js/scripts/dashboard_1_demo.js') }}" type="text/javascript"></script>
@stop