@extends('partials.backend.master')

@section('page_title')
	Dashboard
@stop

@section('custom_styles')
 	<link href="{{ url('vendors/line-awesome/css/line-awesome.min.css') }}" rel="stylesheet"/>
    <link href="{{ url('vendors/themify-icons/css/themify-icons.css') }}" rel="stylesheet"/>
@stop

@section('title_breadcrumb')
  <div class="row page-heading">
    <h1 class="page-title">Dashboard</h1>

    <ol class="breadcrumb">
      <li><a href="#">Dashboard</a></li>
      <li class="active">alumni</li>
    </ol>
  </div>
@stop

@section('content')
	<div class="row">
  <div class="col-md-3">

             <div class="ibox">
                <div class="ibox-body text-center">
                  <div class="m-t-20"><img class="img-circle" src="{{url('img/2.png')}}"/>
                  </div>
                  <h4 class="font-strong m-b-10 m-t-10">{{ $alumni->nama_lengkap }}</h4>
                  <div class="m-b-20 text-muted">{{ $alumni->nama }}</div>
                  <div class="profile-social m-b-20"><a href="javascript:;"><i class="fa fa-twitter"></i></a><a href="javascript:;"><i class="fa fa-facebook"></i></a><a href="javascript:;"><i class="fa fa-pinterest"></i></a><a href="javascript:;"><i class="fa fa-dribbble"></i></a></div>
                  <div>
                    <a href="{{ action('backend\AlumniC@riwayatPekerjaan') }}" class="btn btn-info btn-rounded m-b-5"><i class="fa fa-plus"></i> Pekerjaan</a>

                    <a href="{{ action('backend\UserC@tampilProfilAnggota') }}" class="btn btn-default btn-rounded m-b-5"><i class="fa fa-user"></i>  Profil</a>
                  </div>
                </div>
              </div>

  </div>



		


	</div>
@stop

@section('custom_scripts')

	<script src="{{ url('vendors/chart.js/dist/Chart.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('vendors/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('vendors/jquery-sparkline/dist/jquery.sparkline.min.js" type="text/javascript') }}"></script>
    <script src="{{ url('js/scripts/dashboard_1_demo.js') }}" type="text/javascript"></script>
@stop