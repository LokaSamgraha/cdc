<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
            <meta content="IE=edge" http-equiv="X-UA-Compatible">
                <meta content="width=device-width initial-scale=1.0" name="viewport">
                    <title>
                        Login &mdash; {{ $siteInfo['name'] }}
                    </title>
                    @include('partials.backend._styles')
                    <link href="{{ asset('css/pages/auth-light.css') }}" rel="stylesheet"/>
                </meta>
            </meta>
        </meta>
    </head>
    <body>
        <div class="content">
            {{-- <div class="brand">
                <a class="link" href="index.html">
                    {{ $siteInfo['name'] }}
                </a>
            </div> --}}
            {!! Form::open(['action' => 'backend\HomeC@loginAction', 'id' => 'login-form', 'style' => 'margin-top: 15%;']) !!}
                <div class="text-center m-t-10">
                    <span class="login-img">
                        <i class="fa fa-user">
                        </i>
                    </span>
                </div>
                <h3 class="login-title">
                    Log in
                </h3>

                @if(Session::has('flash_message'))
                	<?php
                		$sessionObj = Session::get('flash_message');
                	?>
	                <div class="alert alert-{{ $sessionObj['tipe'] }} alert-bordered">{{ $sessionObj['pesan'] }}</div>
                @endif

                <div class="form-group">
                	{!! Form::email('trigger1', null, ['class' => 'form-control', 'placeholder' => 'Email', 'required' => true]) !!}
                </div>
                <div class="form-group">
                	{!! Form::password('trigger2', ['class' => 'form-control', 'placeholder' => 'Password', 'required' => true]) !!}
                </div>

                <div class="form-group d-flex justify-content-between">
                    <label class="ui-checkbox ui-checkbox-info">
                        <input type="checkbox">
                            <span class="input-span">
                            </span>
                            Remember me
                        </input>
                    </label>
                    <a href="forgot_password.html">
                        Forgot password?
                    </a>
                </div>
                <div class="form-group">
                    <button class="btn btn-info btn-block" type="submit">Login</button>
                </div>
            {!! Form::close() !!}
            <div class="login-footer">
                Not a member?
                <a class="text-info" href="register.html">Create accaunt</a>
            </div>
        </div>
        <!-- CORE PLUGINS -->
        <script src="{{ asset('vendors/jquery/dist/jquery.min.js') }}" type="text/javascript">
        </script>
        <!-- PAGE LEVEL PLUGINS -->
        <script src="{{ asset('vendors/jquery-validation/dist/jquery.validate.min.js') }}" type="text/javascript">
        </script>
        <!-- PAGE LEVEL SCRIPTS-->
        <script type="text/javascript">
            $(function(){
		        $('#login-form').validate({
		            errorClass:"help-block",
		            rules: {
		                email: {required:true,email:true},
		                password: {required:true}
		            },
		            highlight:function(e){$(e).closest(".form-group").addClass("has-error")},
		            unhighlight:function(e){$(e).closest(".form-group").removeClass("has-error")},
		        });
		      });
        </script>
    </body>
</html>