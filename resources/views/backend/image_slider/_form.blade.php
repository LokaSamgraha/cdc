<div class="form-group">
	<label class="col-sm-2 control-label">Title</label>
	<div class="col-sm-10">
		{!! Form::hidden('id_image_slider', null) !!}
		{!! Form::text('title', null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'Title image' ]) !!}
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Sub title</label>
	<div class="col-sm-10">
		{!! Form::text('subtitle', null, ['class' => 'form-control ', 'required' => true, 'placeholder' => 'Sub title image' ]) !!}
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Theme Color</label>
	<div class="col-sm-3">
		{!! Form::text('theme_color', null, ['class' => 'form-control colorpicker', 'required' => true ]) !!}
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Publish</label>
	<div class="col-sm-2">
		{!! Form::select('publish',['Yes' => 'Yes', 'No' => 'No' ], null, ['class' => 'form-control', 'required' => true ]) !!}
	</div>
</div>

@if(isset($target))
<div class="form-group">
	<label class="col-sm-2 control-label"></label>

	<div class="col-sm-10">
		<img class="img img-thumbnail" style="height:120px;"  src="{{ url('img') }}/{{ $target->image }}"> 
	</div>

</div>
@endif
<div class="form-group">
	<label class="col-sm-2 control-label">Image Slider</label>

	<div class="col-sm-10">
		{!! Form::file('image',['id' => 'bootstrap-input']) !!}
	</div>
</div>
