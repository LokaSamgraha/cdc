<header class="header clf">
    <div class="page-brand"><a class="link" href="index.html">
      <span class="brand">{{ $siteInfo['name'] }}</span>
      <span class="brand-mini" style="font-size: 12px;">{{ $siteInfo['shortname'] }}</span></a>
    </div>

    <!-- START TOP-LEFT TOOLBAR-->
    <ul class="nav pull-left navbar-toolbar">
      <li><a class="sidebar-toggler js-sidebar-toggler"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></a></li>
      <li><a class="search-toggler js-search-toggler"><i class="fa fa-search"></i><span>Search here...</span></a></li>
    </ul>
    <!-- END TOP-LEFT TOOLBAR-->

    <!-- START TOP-RIGHT TOOLBAR-->
    <ul class="nav pull-right navbar-toolbar">

      <li class="dropdown dropdown-user">
        <a class="dropdown-toggle link" data-toggle="dropdown"><span>{{ $loginInfo['name'] }}</span>
          <img src="{{ asset('img/logo.png') }}">
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
        
          <li><a href="{{ action('backend\HomeC@logout') }}" id="btn-logout"><i class="fa fa-power-off"></i>Logout</a></li>
        </ul>
      </li>

    </ul>
  </header>