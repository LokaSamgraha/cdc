<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
            <meta content="IE=edge" http-equiv="X-UA-Compatible">
                <meta content="width=device-width initial-scale=1.0" name="viewport">
                    <title>
                        @yield('page_title')
                        &mdash; 
                        {{ $siteInfo['name'] }}
                    </title>
                    @include('partials.backend._styles')
                    @yield('custom_styles')
                </meta>
            </meta>
        </meta>
    </head>
    <body class="fixed-navbar">
        <div class="page-wrapper">
            @include('partials.backend._header')
      
            @include('partials.backend._sidebar')
            <div class="wrapper content-wrapper">
                @yield('title_breadcrumb')

                <div class="page-content fade-in-up">
                    @yield('content')
                </div>
                @include('partials.backend._footer')
            </div>
        </div>
        <!-- START SEARCH PANEL-->
        <form class="search-top-bar">
            <input class="search-input" placeholder="Search..." type="text">
                <button class="reset input-search-icon">
                    <i class="fa fa-search"></i>
                </button>
                <button class="reset input-search-close" type="button">
                    <i class="fa fa-close"></i>
                </button>
            </input>
        </form>
        <!-- END SEARCH PANEL-->

        <!-- BEGIN PAGA BACKDROPS-->
        <div class="sidenav-backdrop backdrop">
        </div>
        <div class="preloader-backdrop">
            <div class="page-preloader">
                Loading
            </div>
        </div>
        <!-- END PAGA BACKDROPS-->

        {!! Form::open(['action' => 'backend\HomeC@logout', 'id' => 'form-logout']) !!}
        {!! Form::close() !!}

        @include('partials.backend._scripts')
        <script>
            $(document).ready(function(){
                $('.datepicker').datepicker({
                    format: 'yyyy-mm-dd',
                    autoclose: true
                  
                });

                $('.yearpicker').datepicker({
                    format: " yyyy",
                    viewMode: "years", 
                    minViewMode: "years",
                    clearBtn: true,
                    autoclose: true
                  
                });
                
                $('#btn-logout').on('click', function(){
                    $('#form-logout').submit();
                });


                $.validator.addMethod('filesize', function (value, element, param) {
                    return this.optional(element) || (element.files[0].size <= param)
                }, 'File size must be less than {0}');
                
                //validate form validate class
                $(".form-validate").validate({
                      errorElement: 'span', //default input error message container
                      errorClass: 'error', // default input error message class
                      focusInvalid: false, // do not focus the last invalid input
                      ignore: "",
                      rules: {
                          firstname: "required",
                          level: "required",
                          aktif: "required",
                          username:"required",
                          email: {
                              required: true,
                              email: true
                          },
                          password: {
                              
                              minlength: 6
                          },
                          passwordc: {
                              required: true,
                              minlength: 6,
                              equalTo: "#password"
                          },
                          no_telp: {
                              required: true,
                              digits: true
                          },

                          posisi_tersedia: {
                            required:true,
                            digits:true
                          },

                          nama_file: {
                              required: true,
                              extension: "jpg,jpeg",
                              filesize: 20048000,
                          }


                      },
                      
                      highlight: function (element) { // hightlight error inputs
                          $(element)
                          .closest('.form-group').addClass('has-error'); // set error class to the control group
                      },
                      unhighlight: function (element) { // revert the change done by hightlight
                          $(element)
                          .closest('.form-group').removeClass('has-error'); // set error class to the control group
                      }
                    });

                //validate form validate class
                $(".form-pass").validate({
                      errorElement: 'span', //default input error message container
                      errorClass: 'error', // default input error message class
                      focusInvalid: false, // do not focus the last invalid input
                      ignore: "",
                      rules: {
                          
                          password: {
                              required: true,
                              minlength: 6
                          },
                          passwordc: {
                              required: true,
                              minlength: 6,
                              equalTo: "#password"
                          }
                        
                      },
                      
                      highlight: function (element) { // hightlight error inputs
                          $(element)
                          .closest('.form-group').addClass('has-error'); // set error class to the control group
                      },
                      unhighlight: function (element) { // revert the change done by hightlight
                          $(element)
                          .closest('.form-group').removeClass('has-error'); // set error class to the control group
                      }
                    });

                
            });
        </script>

        <script type="text/javascript">
             @if(count(Request::segments()) > 0)
                    @foreach(Request::segments() as $url)
                     $(function(){
                        $('.{{ $url }}').addClass('active');
                        $('.{{ $url }}-ul').addClass('collapse in');
                    });
                    @endforeach 
                @endif
        </script>

        <script type="text/javascript">

        @if(Session::has('pesan'))
            
            $(function(){

                 Command: toastr["{!! Session('tipe') !!}"]("{!! Session('pesan') !!}", "Pemberitahuan")

                    toastr.options = {
                      "closeButton": true,
                      "debug": false,
                      "newestOnTop": false,
                      "progressBar": false,
                      "positionClass": "toast-top-right",
                      "preventDuplicates": false,
                      "onclick": null,
                      "showDuration": "300",
                      "hideDuration": "1000",
                      "timeOut": "5000",
                      "extendedTimeOut": "1000",
                      "showEasing": "swing",
                      "hideEasing": "linear",
                      "showMethod": "fadeIn",
                      "hideMethod": "fadeOut"
                    }

            });

        @endif
        </script>
        
        @yield('custom_scripts')
    </body>
</html>