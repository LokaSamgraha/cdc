<nav class="page-sidebar" id="sidebar">
    <div id="sidebar-collapse">

        @if($loginInfo['level'] == 'admin' )
        <ul class="nav side-menu metismenu">
            <li class="heading">
                MAIN CONTROLLER
            </li>
            <li class="dashboard">
                <a href="{{ action('backend\DashboardC@tampil') }}">
                    <i class="sidebar-item-icon fa fa-dashboard">
                    </i>
                    <span class="nav-label">
                        Dashboard
                    </span>
                </a>
            </li>
            <li class="heading">
                FEATURES
            </li>
            <li class="kelola-user">
                <a href="javascript:;">
                    <i class="sidebar-item-icon fa fa-users"></i>
                    <span class="nav-label">
                       Kelola Pengguna
                    </span>
                    <i class="fa fa-angle-left arrow"></i>
                </a>
                <ul class="nav nav-2-level kelola-user-ul">
                    <li>
                        <a class="anggota" href="{{ action('backend\UserC@tampil') }}">
                            Anggota 
                        </a>
                    </li>

                    <li>
                        <a class="alumni" href="{{ action('backend\AlumniC@tampil') }}">
                            Alumni 
                        </a>
                    </li>

                    <li>
                        <a class="admin" href="{{ action('backend\UserC@tampilAdmin') }}">
                            Admin 
                        </a>
                    </li>
                </ul>
            </li>
            <li class="bursa-kerja">
                <a href="javascript::void();">
                    <i class="sidebar-item-icon fa fa-briefcase">
                    </i>
                    <span class="nav-label">
                        Bursa Kerja
                    </span>
                    <i class="fa fa-angle-left arrow">
                    </i>
                </a>
                <ul class="nav nav-2-level bursa-kerja-ul">
                    <li>
                        <a class="perusahaan" href="{{ action('backend\PerusahaanC@tampil') }}">
                            Perusahaan
                        </a>
                    </li>
                    <li>
                        <a class="lowongan" href="{{ action('backend\LowonganC@tampil') }}">
                            Lowongan
                        </a>
                    </li>
                </ul>
            </li>
            <li class="kelola-informasi">
                <a href="javascript:;">
                    <i class="sidebar-item-icon fa fa-info-circle"></i>
                    <span class="nav-label">
                       Kelola Informasi
                    </span>
                    <i class="fa fa-angle-left arrow"></i>
                </a>
                <ul class="nav nav-2-level kelola-informasi-ul">
                    
                    <li>
                        <a href="{{ action('backend\KategoriC@tampil',['informasi']) }}" class="kategori" >
                            <span class="nav-label">Kategori Informasi</span>
                            
                        </a>
                    </li>
                    <li>
                        <a href="{{ action('backend\InformasiC@tampil') }}" class="informasi" >
                            <span class="nav-label">Informasi</span>
                        </a>
                        
                    </li>


                        
                    
                </ul>
            </li>
            <li class="image">
                <a href="{{ action('backend\LspC@tampilPendaftar') }}">
                    <i class="sidebar-item-icon fa fa-user">
                    </i>
                    <span class="nav-label">
                        Pendaftar LSP
                    </span>
                </a>
            </li>
            <li class="kelola-lsp">
                <a href="javascript:;">
                    <i class="sidebar-item-icon fa fa-bullseye"></i>
                    <span class="nav-label">
                       Kelola LSP
                    </span>
                    <i class="fa fa-angle-left arrow"></i>
                </a>
                <ul class="nav nav-2-level kelola-lsp-ul">
                    <li>
                        <a class="kelola-kategori" href="{{ action('backend\KategoriC@tampil',['lsp']) }}">
                            Kategori LSP
                        </a>
                    </li>
                    <li>
                        <a class="kelola-lsp" href="{{ action('backend\LspC@tampil') }}">
                            LSP
                        </a>
                    </li>
                </ul>
            </li>

             <li class="kelola-mahasiswa">
                <a href="javascript:;">
                    <i class="sidebar-item-icon fa fa-university"></i>
                    <span class="nav-label">
                       Fakultas & Jurusan
                    </span>
                    <i class="fa fa-angle-left arrow"></i>
                </a>
                <ul class="nav nav-2-level kelola-mahasiswa-ul">
                    <li>
                        <a class="fakultas" href="{{ action('backend\FakultasC@tampil') }}">
                            Fakultas
                        </a>
                    </li>
                    <li>
                        <a class="jurusan" href="{{ action('backend\JurusanC@tampil') }}">
                            Jurusan
                        </a>
                    </li>

                    

                    

                </ul>
            </li>

            <li class="heading">
                SETTING
            </li>

            <li class="image">
                <a href="{{ action('backend\ImageSliderC@tampil') }}">
                    <i class="sidebar-item-icon fa fa-image">
                    </i>
                    <span class="nav-label">
                        Image Slider
                    </span>
                </a>
            </li>

            <li class="kontak">
                <a class="kontak-kami" href="{{ action('backend\KontakC@tampil') }}">
                    <i class="sidebar-item-icon fa fa-gear">
                    </i>
                    <span class="nav-label">
                        Umum
                    </span>
                </a>
            </li>

           
        </ul>

        @elseif($loginInfo['level'] == 'alumni')
        <ul class="nav side-menu metismenu">
            <li class="heading">
                MAIN CONTROLLER
            </li>
            <li class="dashboard">
                <a href="{{ action('backend\DashboardC@tampilAlumni') }}">
                    <i class="sidebar-item-icon fa fa-dashboard">
                    </i>
                    <span class="nav-label">
                        Dashboard
                    </span>
                </a>
            </li>

            <li class="profil">
                <a href="{{ action('backend\AlumniC@tampilProfilAlumni') }}">
                    <i class="sidebar-item-icon fa fa-user">
                    </i>
                    <span class="nav-label">
                        Profil
                    </span>
                </a>
            </li>

            <li class="pekerjaan">
                <a href="{{ action('backend\AlumniC@riwayatPekerjaan') }}">
                    <i class="sidebar-item-icon fa fa-star">
                    </i>
                    <span class="nav-label">
                        Riwayat Pekerjaan
                    </span>
                </a>
            </li>
            <li class="pembayaran">
                <a href="{{ action('backend\LspC@tampilLSPdiIkuti') }}">
                    <i class="sidebar-item-icon fa fa-arrow-up">
                    </i>
                    <span class="nav-label">
                        Pendaftaran LSP
                    </span>
                </a>
            </li>

            <li class="heading">
                SETTING
            </li>

            <li class="PASSWORD">
                <a href="{{ action('backend\AlumniC@tampilProfilAlumni') }}">
                    <i class="sidebar-item-icon fa fa-gear">
                    </i>
                    <span class="nav-label">
                        Password
                    </span>
                </a>
            </li>

        </ul>

        @else

        <ul class="nav side-menu metismenu">
            <li class="heading">
                MAIN CONTROLLER
            </li>

            <li class="dashboard">
                <a href="{{ action('backend\DashboardC@tampilAnggota') }}">
                    <i class="sidebar-item-icon fa fa-dashboard">
                    </i>
                    <span class="nav-label">
                        Dashboard
                    </span>
                </a>
            </li>

            <li class="profil">
                <a href="{{ action('backend\UserC@tampilProfilAnggota') }}">
                    <i class="sidebar-item-icon fa fa-user">
                    </i>
                    <span class="nav-label">
                        Profil
                    </span>
                </a>
            </li>

            <li class="pembayaran">
                <a href="{{ action('backend\LspC@tampilLSPdiIkuti') }}">
                    <i class="sidebar-item-icon fa fa-arrow-up">
                    </i>
                    <span class="nav-label">
                        Pendaftaran LSP
                    </span>
                </a>
            </li>

        </ul>





        @endif


        <div class="sidebar-footer">
            <a href="javascript:;">
                <i class="fa fa-bullhorn">
                </i>
            </a>
            <a href="javascript:;">
                <i class="fa fa-calendar">
                </i>
            </a>
            <a href="javascript:;">
                <i class="fa fa-commenting">
                </i>
            </a>
            <a href="{{ action('backend\HomeC@logout') }}">
                <i class="fa fa-power-off">
                </i>
            </a>
        </div>
    </div>
</nav>