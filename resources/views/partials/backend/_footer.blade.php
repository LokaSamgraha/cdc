<footer class="page-footer">
  <div class="to-top"><i class="fa fa-angle-double-up"></i></div>
  <div class="pull-left">{!! $siteInfo['footer'] !!}</div>
</footer>