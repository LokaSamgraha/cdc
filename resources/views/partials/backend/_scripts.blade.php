
<script src="{{ asset('vendors/jquery/dist/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/bootstrap-3/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/metisMenu/dist/metisMenu.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/jquery-idletimer/dist/idle-timer.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/toastr/toastr.min.js') }}" type="text/javascript"></script>
<!-- PAGE LEVEL PLUGINS-->
<script src="{{ asset('vendors/chart.js/dist/Chart.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/jquery-sparkline/dist/jquery.sparkline.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/datepicker/bootstrap-datepicker.js') }}" type="text/javascript"></script>


<script src="{{ url('vendors/validationEngine/jquery.validationEngine.js') }}"></script>
<script src="{{ url('vendors/validationEngine/languages/jquery.validationEngine-en.js') }}"></script>
<script src="{{ url('vendors/validation/jquery.validate.min.js') }}"></script>
<script src="{{ url('vendors/validation/localization/messages_id.min.js') }}"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/additional-methods.js"></script>


<!-- CORE SCRIPTS-->
<script src="{{ asset('js/app.js') }}"" type="text/javascript"></script>