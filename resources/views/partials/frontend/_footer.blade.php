<footer class="footer">
        <div class="container">
            <div class="widget col-lg-4 col-md-4 col-sm-12">
            <h4 class="title">Tentang Kami</h4>
                <p>{{ $kontak_kami->tentang_kami }}</p>
                
            </div><!-- end widget -->
            <div class="widget col-lg-4 col-md-4 col-sm-12">
            <h4 class="title">Tautan</h4>
				<ul class="contact_details">
                    <li><a href="http://undiksha.ac.id/"><i class="fa fa-chevron-right"></i> Web Undiksha</a></li>
                    <li><a href="http://si.undiksha.ac.id/"><i class="fa fa-chevron-right"></i> SIAK Undiksha</a></li>
                    <li><a href="http://elearning.undiksha.ac.id/"><i class="fa fa-chevron-right"></i> Elearning Undiksha</a></li>
                </ul>
            </div><!-- end widget -->  
            <div class="widget col-lg-4 col-md-4 col-sm-12">
            <h4 class="title">Kontak Kami</h4>
                <ul class="contact_details">
                	<li><i class="fa fa-envelope-o"></i> {{ $kontak_kami->email }}</li>
                	<li><i class="fa fa-phone-square"></i> {{ $kontak_kami->no_telp }}</li>
                	<li><i class="fa fa-home"></i> {{ $kontak_kami->alamat }}</li>
                </ul><!-- contact_details -->
            </div><!-- end widget -->  
          
        </div><!-- end container -->
        
        <div class="copyrights">
            <div class="container">
                <div class="col-lg-6 col-md-6 col-sm-12 columns">
                    <p>Copyright © 2017 - All right reserved.</p>
                </div><!-- end widget -->
                <div class="col-lg-6 col-md-6 col-sm-12 text-right">
                <div class="social_buttons">
                    <a href="{{ $kontak_kami->fb }}" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a>
                    <a href="{{ $kontak_kami->twitter }}" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a>
                    <a href="{{ $kontak_kami->gplus }}" data-toggle="tooltip" data-placement="top" title="Google+"><i class="fa fa-google-plus"></i></a>
                   
                </div>
            </div>
                 
            </div><!-- end container -->
        </div><!-- end copyrights -->
    </footer><!-- end footer -->