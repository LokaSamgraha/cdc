<!doctype html>
<html class="no-js" lang="en">
	<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title> @yield('title') | Career Development Center - UNDIKSHA</title>
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<link rel="shortcut icon" href="favicon.ico">
    
	<!-- Google Fonts -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
	<!-- Awesome Fonts -->
	<link rel="stylesheet" href="{{ URL::to('front/css/font-awesome.css') }}">
	<!-- Bootstrap -->
	<link href="{{ URL::to('front/assets/css/bootstrap.css') }}" rel="stylesheet">
	<!-- Template Styles -->
	<link rel="stylesheet" href="{{ URL::to('front/style.css') }}">
	<link rel="stylesheet" href="{{ URL::to('front/css/dark.css') }}">    
	<!-- Layer Slider -->
	<link rel="stylesheet" href="{{ URL::to('front/layerslider/css/layerslider.css') }}" type="text/css">

    <link href="{{ asset('vendors/datepicker/datepicker3.css') }}" rel="stylesheet" type="text/css" />
  <script src="{{ URL::to('front/assets/js/jquery.js') }}"></script>
    

    
	<!-- http://www.456bereastreet.com/archive/201209/tell_css_that_javascript_is_available_asap/ -->
	<script>
		document.documentElement.className = document.documentElement.className.replace(/(\s|^)no-js(\s|$)/, '$1js$2');
	</script>
    
<style type="text/css">
    .judul-custom a:hover h4{  
        color: #0099FF;
        text-decoration: underline; 
       

    }

    .widget a:hover{
        color: #0099FF;
        text-decoration: underline; 
    }
</style>
    @yield('style')



  
	<!-- Support for HTML5 -->
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Enable media queries on older browsers -->
	<!--[if lt IE 9]>
		<script src="assets/js/respond.min.js"></script>
	<![endif]-->
  
	<script src="{{ URL::to('front/js/modernizr.js') }}"></script>
  </head>
<body>
    <div class="topbar clearfix">
    	<div class="container">
			<div class="col-lg-6 col-md-6 col-sm-12 text-left">
          @if($loginInfo != NULL)
          <div class="callus">
            
               <a href="{{ url('dashboard/'.$loginInfo['level']) }}"> <p> <span> Anda Login {{ $loginInfo['name'] }}</span></p></a>
              </div>

          @endif
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 text-right">
            	<div class="callus">
                    <p><span><i class="fa fa-envelope-o"></i> {{ $kontak_kami->email }}</span> <span><i class="fa fa-phone"></i> {{ $kontak_kami->no_telp }}</span></p>
                </div>
			</div>
    	</div><!-- end container -->
    </div><!-- end topbar -->

    @include('partials.frontend._header')
   
    

<section class="post-wrapper-top">
    
    

        @yield('content')

            

</section>

   
    @include('partials.frontend._footer')
    <div class="dmtop">Scroll to Top</div>

      <!-- Main Scripts-->
	<script src="{{ URL::to('front/assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::to('front/assets/js/jquery.unveilEffects.js') }}"></script>	
	<script src="{{ URL::to('front/js/retina-1.1.0.js') }}"></script>
	<script src="{{ URL::to('front/js/jquery.hoverex.min.js') }}"></script>
	<script src="{{ URL::to('front/js/jquery.hoverdir.js') }}"></script>
    <script src="{{ URL::to('front/js/owl.carousel.js') }}"></script>	
    <script src="{{ URL::to('front/js/jetmenu.js') }}"></script>	
	<script src="{{ URL::to('front/js/jquery.hoverex.min.js') }}"></script>
	<script src="{{ URL::to('front/js/jquery.prettyPhoto.js') }}"></script>
	<script src="{{ URL::to('front/js/custom.js') }}"></script>
     <script src="{{ URL::to('front/layerslider/js/greensock.js') }}" type="text/javascript"></script>
    <script src="{{ URL::to('front/layerslider/js/layerslider.transitions.js') }}" type="text/javascript"></script>
    <script src="{{ URL::to('front/layerslider/js/layerslider.kreaturamedia.jquery.js') }}" type="text/javascript"></script>

    <script src="{{ asset('vendors/datepicker/bootstrap-datepicker.js') }}" type="text/javascript"></script>

    <script src="{{ url('vendors/validationEngine/jquery.validationEngine.js') }}"></script>
    <script src="{{ url('vendors/validationEngine/languages/jquery.validationEngine-en.js') }}"></script>
    <script src="{{ url('vendors/validation/jquery.validate.min.js') }}"></script>
    <script src="{{ url('vendors/validation/localization/messages_id.min.js') }}"></script>
    @yield('script')
    <script>
            jQuery("#layerslider").layerSlider({
                pauseOnHover: false,
                autoPlayVideos: false,
                responsive: true,
                responsiveUnder: 1120,
                layersContainer: 1120,
                skin: 'v5',
                skinsPath: "layerslider/skins/"
            });

             $(document).ready(function(){
                $('.datepicker').datepicker({
                    format: 'yyyy-mm-dd',
                  
                });

                $('.yearpicker').datepicker({
                    format: " yyyy",
                    viewMode: "years", 
                    minViewMode: "years",
                    clearBtn: true,
                    autoclose: true
                  
                });

                //validate form validate class
                $(".form-validate").validate({
                      errorElement: 'span', //default input error message container
                      errorClass: 'error', // default input error message class
                      focusInvalid: false, // do not focus the last invalid input
                      ignore: "",
                      rules: {
                          firstname: "required",
                          level: "required",
                          aktif: "required",
                          username:"required",
                          email: {
                              required: true,
                              email: true
                          },
                          password: {
                              required: true,
                              minlength: 6
                          },
                          passwordc: {
                              required: true,
                              minlength: 6,
                              equalTo: "#password"
                          },
                          no_telp: {
                              required: true,
                              digits: true
                          },

                          posisi_tersedia: {
                            required:true,
                            digits:true
                          }
                      },
                      
                      highlight: function (element) { // hightlight error inputs
                          $(element)
                          .closest('.form-group').addClass('has-error'); // set error class to the control group
                      },
                      unhighlight: function (element) { // revert the change done by hightlight
                          $(element)
                          .closest('.form-group').removeClass('has-error'); // set error class to the control group
                      }
                    });
            
            });
        </script>
	</body>
</html>