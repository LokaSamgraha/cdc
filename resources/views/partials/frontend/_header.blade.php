 <header class="header">
    	<div class="container">
        	<div class="site-header clearfix" style="padding-top: 20px;">
                <div class="col-lg-3 col-md-3 col-sm-12 title-area pull-left">
                    <div class="site-title" id="title" style="padding-bottom: 20px;">
                        <a href="index.html" title="">
                            <img src="{{ URL::to('front/images/front-cdc2.png') }}" alt="">	
                        </a>
                    </div>
                </div><!-- title area -->
                <div class="col-lg-9 col-md-12 col-sm-12">
                   <div id="nav">
                        <div class="container clearfix">
                        <ul id="jetmenu" class="jetmenu blue">
                            <li><a href="{{ url('/') }}">Beranda</a>
                                
                            </li>
                            <li><a href="{{ url('bursa-kerja') }}">Bursa Kerja</a>
                               
                            </li>
                            <li><a href="{{ url('informasi') }}">Informasi</a>
                               
                            </li>
                           
                            <li><a href="{{ url('alumni') }}">Alumni</a>
                                
                            </li>
                          
                            <li><a href="{{ url('tracer-study') }}">Tracer Study</a>
                                
                            </li>
                            <li><a href="{{ url('kontak-kami') }}">Kontak Kami</a>
                                
                            </li>
                            <li><a  title ="Lembaga Sertifikasi Profesi" href="{{ url('lsp') }}">LSP</a>
                                
                            </li>

                           <!--  <li class="right"><a href="#">LSP</a>
                                <div class="megamenu full-width">
                                    <div class="row">
                                        <div class="col2">
                                        <h5 class="title">Profil LSP</h5>
                                      <ul class="contact_details">
                                                <li><a href="http://undiksha.ac.id/"><i class="fa fa-chevron-right"></i> Visi Misi</a></li>
                                                <li><a href="http://si.undiksha.ac.id/"><i class="fa fa-chevron-right"></i> Tujuan</a></li>
                                                <li><a href="http://elearning.undiksha.ac.id/"><i class="fa fa-chevron-right"></i> Struktur Organisasi</a></li>
                                                <li><a href="http://elearning.undiksha.ac.id/"><i class="fa fa-chevron-right"></i> Sasaran Mutu</a></li>
                                            </ul>
                                        </div>
                                        <div class="col2">
                                        <h5 class="title">Sertifikasi LSP</h5>
                                           <ul class="contact_details">
                                                <li><a href="http://undiksha.ac.id/"><i class="fa fa-chevron-right"></i> Pemegang Sertifikasi</a></li>
                                                <li><a href="http://si.undiksha.ac.id/"><i class="fa fa-chevron-right"></i> Skema Sertifikasi</a></li>
                                                <li><a href="http://elearning.undiksha.ac.id/"><i class="fa fa-chevron-right"></i> Tempat Uji Kompetensi</a></li>
                                            </ul>
                                        </div>
                                        <div class="col2">
                                        <h5 class="title">Informasi LSP</h5>
                                            <ul class="contact_details">
                                                <li><a href="http://undiksha.ac.id/"><i class="fa fa-chevron-right"></i> Pengumaman LSP</a></li>
                                                <li><a href="http://si.undiksha.ac.id/"><i class="fa fa-chevron-right"></i> Formulir LSP</a></li>
                                                <li><a href="http://elearning.undiksha.ac.id/"><i class="fa fa-chevron-right"></i> Training Package</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    
                                </div>
                            </li> -->
                            
                            <li><a href="{{ url('login') }}">LOGIN</a>
                                
                            </li>


                            
                        </ul>
                        </div>
                    </div><!-- nav -->   
                </div><!-- title area -->
            </div><!-- site header -->
    	</div><!-- end container -->
    </header><!-- end header -->