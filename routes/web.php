<?php


Route::get('/', 'frontend\FIndexC@index');
Route::get('/bursa-kerja', 'frontend\FBursaC@index');
Route::get('/bursa-kerja/company/{id_perusahaan}', 'frontend\FIndexC@lowonganBy');

Route::get('/informasi', 'frontend\FInfoC@index');
Route::get('/informasi/view/{id_informasi}/{slug}', 'frontend\FInfoC@infoDetail');
Route::get('/informasi/kategori/{id_kategori}', 'frontend\FInfoC@infoBy');

Route::get('/tracer-study', 'frontend\FTracerStudyC@index');
Route::get('/alumni', 'frontend\FAlumniC@index');
Route::get('/alumni/front/ajax', 'frontend\FAlumniC@getData');

Route::get('/kontak-kami', 'frontend\FKontakC@index');

Route::get('/lsp', 'frontend\FLspC@index');
Route::get('/lsp/kategori/{id_kategori}', 'frontend\FLspC@lspBy');

Route::get('/pendaftaran/anggota', 'backend\HomeC@registerAnggota');
Route::post('/store/anggota', 'backend\HomeC@storeAnggota');
    


Route::get('/pendaftaran/alumni', 'backend\HomeC@registerAlumni');
Route::post('/store/alumni', 'backend\HomeC@storeAlumni');

   

Route::get('login', ['uses' => 'backend\HomeC@showLogin', 'as' => 'login']);
Route::post('login', 'backend\HomeC@loginAction');
 Route::get('logout', 'backend\HomeC@logout');

Route::group(['middleware' => 'auth'], function () {
   
Route::get('/bursa-kerja/view/{id_lowongan}/{slug}', 'frontend\FBursaC@detail');
Route::get('/lsp/view/{id_lsp}/{slug}', 'frontend\FLspC@lspDetail');
Route::get('/lsp/daftar/{id_lsp}', 'frontend\FLspC@showDaftarDetail');
Route::post('/lsp/daftar', 'frontend\FLspC@storeDaftar');
   
Route::get('dashboard/admin', 'backend\DashboardC@tampil')->middleware('admin');
 
   

    Route::group(['prefix' => 'bursa-kerja','middleware' =>'admin'], function () {
        Route::group(['prefix' => 'perusahaan'], function () {
            Route::post('simpan', 'backend\PerusahaanC@simpan');
            Route::post('hapus', 'backend\PerusahaanC@hapus');
            Route::get('{id}/edit', 'backend\PerusahaanC@edit');
            Route::post('update', 'backend\PerusahaanC@update');
            Route::get('get-data', 'backend\PerusahaanC@getData');
            Route::get('/', 'backend\PerusahaanC@tampil');
        });

        Route::group(['prefix' => 'lowongan'], function () {
            Route::post('simpan', 'backend\LowonganC@simpan');
            Route::post('hapus', 'backend\LowonganC@hapus');
            Route::get('{id}/edit', 'backend\LowonganC@edit');
            Route::post('update', 'backend\LowonganC@update');
            Route::get('get-data', 'backend\LowonganC@getData');
            Route::get('/', 'backend\LowonganC@tampil');
        });
    });

   
    
    Route::group(['prefix' => 'kelola-lsp','middleware' => 'admin'], function () {
        Route::post('simpan', 'backend\LspC@simpan');
        Route::post('hapus', 'backend\LspC@hapus');
        Route::get('{id}/edit', 'backend\LspC@edit');
        Route::post('update', 'backend\LspC@update');
        Route::get('get-data', 'backend\LspC@getData');
        Route::get('/', 'backend\LspC@tampil');
    });


    Route::group(['prefix' => 'pendaftar-lsp'], function () {

        Route::post('simpan', 'backend\LspC@simpan');
        Route::post('hapus', 'backend\LspC@hapus');
        Route::get('{id}/edit', 'backend\LspC@edit');
        Route::post('update', 'backend\LspC@updateStatusBerkas');
        Route::get('get-data', 'backend\LspC@getPendaftar');
        Route::get('/', 'backend\LspC@tampilPendaftar');
        Route::get('detailBerkasTransfer/{id}','backend\LspC@detailBerkasTransfer');

    });

    Route::group(['prefix' => 'kelola-informasi', 'middleware' =>'admin'], function () {
        Route::group(['prefix' => 'informasi'], function () {
            Route::post('simpan', 'backend\InformasiC@simpan');
            Route::post('hapus', 'backend\InformasiC@hapus');
            Route::get('{id}/edit', 'backend\InformasiC@edit');
            Route::post('update', 'backend\InformasiC@update');
            Route::get('get-data', 'backend\InformasiC@getData');
            Route::get('/', 'backend\InformasiC@tampil');


        });
        
    });

    Route::group(['prefix' => 'kelola-mahasiswa', 'middleware' =>'admin'], function () {
        Route::group(['prefix' => 'fakultas'], function () {
            Route::post('simpan', 'backend\FakultasC@simpan');
            Route::post('hapus', 'backend\FakultasC@hapus');
            Route::get('{id}/edit', 'backend\FakultasC@edit');
            Route::post('update', 'backend\FakultasC@update');
            Route::get('get-data', 'backend\FakultasC@getData');
            Route::get('/', 'backend\FakultasC@tampil');
        });

        Route::group(['prefix' => 'jurusan'], function () {
            Route::post('simpan', 'backend\JurusanC@simpan');
            Route::post('hapus', 'backend\JurusanC@hapus');
            Route::get('{id}/edit', 'backend\JurusanC@edit');
            Route::post('update', 'backend\JurusanC@update');
            Route::get('get-data', 'backend\JurusanC@getData');
            Route::get('/', 'backend\JurusanC@tampil');
        });

      


    });

Route::group(['prefix' => 'kelola-user'], function () {

      Route::group(['prefix' => 'alumni'], function () {
            Route::post('simpan', 'backend\AlumniC@simpan');
            Route::post('hapus', 'backend\AlumniC@hapus');
            Route::get('{id}/edit', 'backend\AlumniC@edit');
            Route::post('update', 'backend\AlumniC@update');
            Route::get('get-data', 'backend\AlumniC@getData');
            Route::get('/', 'backend\AlumniC@tampil');


        });

    Route::group(['prefix' => 'anggota'], function () {
        Route::post('simpan', 'backend\UserC@simpan');
        Route::post('hapus', 'backend\UserC@hapus');
        Route::get('{id}/edit', 'backend\UserC@edit');
        Route::post('update', 'backend\UserC@update');
        Route::get('get-data', 'backend\UserC@getData');
        Route::get('/', 'backend\UserC@tampil');
    });

    Route::group(['prefix' => 'admin', 'middleware' =>'admin'], function () {

        Route::get('/', 'backend\UserC@tampilAdmin');
        Route::get('get-data', 'backend\UserC@getAdmin');

    });



   });

    Route::group(['prefix' => 'kelola-image', 'middleware' =>'admin'], function () {
            Route::post('simpan', 'backend\ImageSliderC@simpan');
            Route::post('hapus', 'backend\ImageSliderC@hapus');
            Route::get('{id}/edit', 'backend\ImageSliderC@edit');
            Route::post('update', 'backend\ImageSliderC@update');
            Route::get('get-data', 'backend\ImageSliderC@getData');
            Route::get('/', 'backend\ImageSliderC@tampil');
        });

    Route::group(['prefix' => 'kelola-kontak-kami', 'middleware' =>'admin'], function () {
            Route::post('simpan', 'backend\KontakC@simpan');
            Route::post('hapus', 'backend\KontakC@hapus');
            Route::get('{id}/edit', 'backend\KontakC@edit');
            Route::post('update', 'backend\KontakC@update');
            Route::get('get-data', 'backend\KontakC@getData');
            Route::get('/', 'backend\KontakC@tampil');
        });

    

    Route::group(['prefix' => 'kelola-kategori', 'middleware' =>'admin'], function () {
        Route::post('simpan', 'backend\KategoriC@simpan');
        Route::post('hapus', 'backend\KategoriC@hapus');
        Route::get('jenis/{jenis}/{id}/edit', 'backend\KategoriC@edit');
        Route::post('update', 'backend\KategoriC@update');
        Route::get('get-data/{jenis}', 'backend\KategoriC@getData');
        Route::get('jenis/{jenis}', 'backend\KategoriC@tampil');
    });

    //alumni router
Route::group(['middleware' =>'alumni'], function () {
    Route::get('dashboard/alumni', 'backend\DashboardC@tampilAlumni');
    Route::get('alumni/profil','backend\AlumniC@tampilProfilAlumni');
    Route::get('alumni/riwayat-pekerjaan','backend\AlumniC@riwayatPekerjaan');
    Route::get('get-data/pekerjaan','backend\AlumniC@getPekerjaan');

});

    Route::post('update/password','backend\UserC@updatePassword'); 
    Route::get('daftar-lsp','backend\LspC@tampilLSPdiIkuti');
    Route::get('daftar-lsp/{id}/edit','backend\LspC@detailLSPdiIkuti');
    Route::get('get-data/daftar-lsp','backend\LspC@getLspdiIkuti');
    Route::post('batal/daftar-lsp','backend\LspC@batalDaftar');


Route::group(['middleware' => 'adminAlumni'] , function () {


    Route::get('alumni/pekerjaan/{id}/edit','backend\AlumniC@editPekerjaan');
    Route::post('alumni/alumni/pekerjaan','backend\AlumniC@updatePekerjaan');


    Route::post('store/pekerjaan','backend\AlumniC@storePekerjaan');
    Route::post('hapus/alumni/pekerjaan','backend\AlumniC@hapusPekerjaan');


});

Route::post('alumni/store/bukti/transfer','backend\LspC@uploadBukti');

Route::get('anggota/profil','backend\UserC@tampilProfilAnggota');
    //Anggota router
     Route::group(['middleware' =>'anggota'], function () {
        Route::get('dashboard/anggota', 'backend\DashboardC@tampilAnggota');
        
    });






});

