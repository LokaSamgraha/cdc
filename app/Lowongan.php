<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lowongan extends Model
{
    protected $table = 'lowongan';
    protected $primarykey = 'id_lowongan';
    protected $guarded = [];
    protected $dates = [];
}
