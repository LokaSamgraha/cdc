<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class StartApp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start app required data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $pesan    = 'Perintah sudah dijalankan sebelumnya';
        $cekAdmin = \App\User::get()->toArray();
        if (count($cekAdmin) == 0) {
            $dataAdmin = [
                'name'     => 'admin',
                'email'    => 'admin@email.com',
                'password' => bcrypt('admin'),
            ];

            $saveAdmin = \App\User::create($dataAdmin);
            $pesan     = 'Perintah berhasil dijalankan';
        }

        $this->info($pesan);
    }
}
