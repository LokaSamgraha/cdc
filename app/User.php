<?php

namespace App;

use Auth;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //     'username', 'email', 'password','nama_lengkap','tempat_lahir','tgl_lahir','jk','no_telp','no_selular','is_active','level','id_equal'
    // ];
    protected $guarded = [];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function getLoginInfo()
    {
        $loginUser = array();

        $user = Auth::user();
        if ($user) {
            $loginUser['user_id']     = $user['id'];
            $loginUser['name']        = $user['username'];
            $loginUser['nama_lengkap']= $user['nama_lengkap'];
            $loginUser['email']       = $user['email'];
            $loginUser['level']       = $user['level'];
            $loginUser['nim']       = $user['id_equal'];
            $loginUser['last_access'] = $user['updated_at']->diffForHumans();
        }

        return $loginUser;
    }
}
