<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = 'kategori';
    protected $primarykey = 'id_kategori';
    protected $guarded = [];
    protected $dates = [];
}
