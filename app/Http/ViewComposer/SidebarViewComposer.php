<?php
namespace App\Http\ViewComposer;

use Illuminate\Contracts\View\View;

class SidebarViewComposer
{
    public function compose(View $view)
    {
        $loginInfo = \App\User::getLoginInfo();
        $view->with('loginInfo', $loginInfo);

        $view->with('siteInfo', \Config::get('site_info'));
    }
}
