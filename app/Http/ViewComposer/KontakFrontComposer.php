<?php
namespace App\Http\ViewComposer;

use Illuminate\Contracts\View\View;

class KontakFrontComposer
{
    public function compose(View $view)
    {
        
        $kontak = \App\Kontak::where('id_kontak',1)->firstOrFail();
        $view->with('kontak_kami', $kontak);

    }
}
