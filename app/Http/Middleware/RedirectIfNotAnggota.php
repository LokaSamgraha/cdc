<?php

namespace App\Http\Middleware;

use Closure;

class RedirectIfNotAnggota
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userLogin = \App\User::getLoginInfo();
        if ($userLogin['level'] == 'anggota') {
            return $next($request);
        } else {
            return back();
        }
    }
}
