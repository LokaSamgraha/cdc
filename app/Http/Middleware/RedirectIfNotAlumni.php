<?php

namespace App\Http\Middleware;

use Closure;

class RedirectIfNotAlumni
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      $userLogin = \App\User::getLoginInfo();
        if ($userLogin['level'] == 'alumni') {
            return $next($request);
        } else {
            return back();
        }
    }
}
