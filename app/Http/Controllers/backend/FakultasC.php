<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\GlobalHelper as Ghelp;
use App\Helpers\MyImage;
use App\Fakultas;
use DataTables;

class FakultasC extends Controller
{
    private $exceptFields = array();
    private $fakultasObj;

    public function __construct()
    {
        $this->fakultasObj = new Fakultas;

        $this->exceptFields = [
            '_token',
            'id_fakultas',
        ];
    }

    public function getData()
    {
        $no= 1 ;
        $table = $this->fakultasObj->get();
        return DataTables::of($table)
            ->addIndexColumn()
            
            ->addColumn('action', function ($table) {

                  return '<a href="fakultas/'.$table->id_fakultas.'/edit" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i>&nbsp;Ubah </a>&nbsp;<button class="btn btn-danger btn-xs" id="deleteUser" data-remote="'.$table->id_fakultas.','.$table->nama.'"><i class="fa fa-close"></i>&nbsp;Hapus</button>';
                })
            ->rawColumns(['logo','action'])
            ->make(true);
    }

    public function tampil()
    {
        return view('backend.fakultas.index');
    }

    public function simpan(Request $request)
    {


        $simpan = $this->fakultasObj->create($request->all());
        Ghelp::pesan($simpan, '==', true, 'Data fakultas berhasil disimpan', 'Gagal menyimpan data fakultas');
        return back();
    }

    public function edit($id_fakultas){
        $target = $this->fakultasObj->where('id_fakultas',$id_fakultas)->firstOrFail();

        return view('backend.fakultas.update',compact('target'));

    }

    public function update(Request $request){
        
        $perusahanFilter = $this->fakultasObj->where('id_fakultas', $request->id_fakultas)->first();


        $update = $this->fakultasObj->where('id_fakultas',$request->id_fakultas)->update($request->except($this->exceptFields));
        Ghelp::pesan($update, '==', true, 'Data fakultas berhasil diupdate', 'Gagal mengupdate data fakultas');

        return redirect()->action('backend\FakultasC@tampil');
    }

    public function hapus(Request $request)
    {

       $hapus = $this->fakultasObj->where('id_fakultas',$request->id_fakultas)->delete();

    }
}
