<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Kategori;
use App\Helpers\GlobalHelper as Ghelp;
use DataTables;


class KategoriC extends Controller
{
    private $exceptFields = array();
    private $kategoriObj;

    public function __construct()
    {
        $this->kategoriObj = new Kategori;

        $this->exceptFields = [
            '_token',
            'id_kategori',
        ];
    }

    public function getData($jenis)
    {

    	
    	$table = $this->kategoriObj->where('jenis',$jenis)->get();
    	
        return DataTables::of($table)
            ->addIndexColumn()
            
            ->addColumn('action', function ($table) {
                  return '<a href="'.$table->jenis.'/'.$table->id_kategori.'/edit" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i>&nbsp;Ubah </a>&nbsp;<button class="btn btn-danger btn-xs" id="deleteUser" data-remote="'.$table->id_kategori.','.$table->nama_kategori.'"><i class="fa fa-close"></i>&nbsp;Hapus</button>';
                })
            ->addColumn('last_update',function($table){
          			return \Carbon\Carbon::parse($table->update_at)->toDayDateTimeString();
            })
            ->rawColumns(['action'])
            ->make(true);
    }



    public function tampil($jenis)
    {

    	return view('backend.kategori.index',compact('jenis'));
    
        
    }

    public function simpan(Request $request)
    {
    	$request->request->add(['slug'=>str_slug($request->nama_kategori)]);

        $simpan = $this->kategoriObj->create($request->all());
        Ghelp::pesan($simpan, '==', true, 'Data kategori berhasil disimpan', 'Gagal menyimpan data kategori');
        return back();
    }

    public function edit($jenis,$id_kategori){

        $target = $this->kategoriObj->where('id_kategori',$id_kategori)->firstOrFail();

        return view('backend.kategori.update',compact('target','jenis'));

    }

    public function update(Request $request){
        
        $update = $this->kategoriObj->where('id_kategori',$request->id_kategori)->update($request->except($this->exceptFields));
        Ghelp::pesan($update, '==', true, 'Data kategori berhasil diupdate', 'Gagal mengupdate data kategori');

        return redirect()->action('backend\KategoriC@tampil',[$request->jenis]);
    }

    public function hapus(Request $request)
    {
       $hapus = $this->kategoriObj->where('id_kategori',$request->id_kategori)->delete();

    }
}
