<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\GlobalHelper as Ghelp;
use DataTables;
use App\Jurusan;
use App\fakultas;

class JurusanC extends Controller
{
    private $exceptFields = array();
    private $jurusanObj;

    public function __construct()
    {
        $this->jurusanObj = new Jurusan;

        $this->exceptFields = [
            '_token',
            'id_jurusan',
        ];
    }

    public function getData()
    {
        $table = $this->jurusanObj
        ->join('fakultas','fakultas.id_fakultas','jurusan.id_fakultas')
		->select(\DB::raw('jurusan.*,fakultas.nama as "nama_fakultas"'))
        ->get();
        return DataTables::of($table)
            ->addIndexColumn()
            
            ->addColumn('action', function ($table) {

                  return '<a href="jurusan/'.$table->id_jurusan.'/edit" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i>&nbsp;Ubah </a>&nbsp;<button class="btn btn-danger btn-xs" id="deleteUser" data-remote="'.$table->id_jurusan.','.$table->nama.'"><i class="fa fa-close"></i>&nbsp;Hapus</button>';
                })
            ->rawColumns(['logo','action'])
            ->make(true);
    }

    public function tampil()
    {
    	$list_fakultas = \App\Fakultas::pluck('nama','id_fakultas');
        return view('backend.jurusan.index',compact('list_fakultas'));
    }

    public function simpan(Request $request)
    {


        $simpan = $this->jurusanObj->create($request->all());
        Ghelp::pesan($simpan, '==', true, 'Data jurusan berhasil disimpan', 'Gagal menyimpan data jurusan');
        return back();
    }

    public function edit($id_jurusan){
    	$list_fakultas = Fakultas::pluck('nama','id_fakultas');

        $target = $this->jurusanObj->where('id_jurusan',$id_jurusan)->firstOrFail();

        return view('backend.jurusan.update',compact('target','list_fakultas'));

    }

    public function update(Request $request){
        
        $update = $this->jurusanObj->where('id_jurusan',$request->id_jurusan)->update($request->except($this->exceptFields));
        Ghelp::pesan($update, '==', true, 'Data jurusan berhasil diupdate', 'Gagal mengupdate data jurusan');

        return redirect()->action('backend\JurusanC@tampil');
    }

    public function hapus(Request $request)
    {

       $hapus = $this->jurusanObj->where('id_jurusan',$request->id_jurusan)->delete();

    }
}
