<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\GlobalHelper as Ghelp;
use App\Lowongan;
use App\Perusahaan;
use App\Jurusan;
use DataTables;
use Config;

class LowonganC extends Controller
{
    private $exceptFields = array();
    private $perusahaanObj;

    public function __construct()
    {
        $this->lowonganObj = new Lowongan;

        $this->exceptFields = [
            '_token',
            'id_lowongan',
        ];
    }

    public function getData()
    {
        
        $table = Lowongan::join('perusahaan','perusahaan.id_perusahaan','lowongan.id_perusahaan')
                ->select(\DB::raw('lowongan.*,perusahaan.name'));
          return Datatables::of($table)->addColumn('action', function ($table) {

          return '<a href="lowongan/'.$table->id_lowongan.'/edit" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i>&nbsp;Ubah </a>&nbsp;<button class="btn btn-danger btn-xs" id="deleteUser" data-remote="'.$table->id_lowongan.','.$table->nama_lowongan.'"><i class="fa fa-close"></i>&nbsp;Hapus</button>';
        })
        ->addIndexColumn()
        ->addColumn('last_update', function($table){
          return \Carbon\Carbon::parse($table->updated_at)->toDayDateTimeString();
        })
        ->make(true);
    }

    public function tampil()
    {
        $list_jenjang = Config::get('dummy.jenjang');
        $list_jenis_pekerjaan = Config::get('dummy.jenis_pekerjaan');
        $list_perusahaan = Perusahaan::pluck('name','id_perusahaan');
        $list_jurusan = Jurusan::pluck('nama','nama');
        $list_jurusan->prepend('Semua Jurusan', 'Semua Jurusan');
        return view('backend.lowongan.index',compact('list_perusahaan','list_jurusan','list_jenjang','list_jenis_pekerjaan'));
    }

    public function simpan(Request $request)
    {
        $jurusan_ar = implode(', ', $request->nama_jurusan);
        $request->request->add(['nama_jurusan' => $jurusan_ar, 'slug' => str_slug($request->nama_lowongan) ]);
        $simpan = $this->lowonganObj->create($request->except('files'));
        Ghelp::pesan($simpan, '==', true, 'Data lowongan berhasil disimpan', 'Gagal menyimpan data lowongan');
        return back();
    }

    public function edit($id_lowongan){


        $list_jenjang = Config::get('dummy.jenjang');
        $list_jenis_pekerjaan = Config::get('dummy.jenis_pekerjaan');
        $list_perusahaan = Perusahaan::pluck('name','id_perusahaan');
        $list_jurusan = Jurusan::pluck('nama','nama');
        $list_jurusan->prepend('Semua Jurusan', 'Semua Jurusan');


        $target = $this->lowonganObj->where('id_lowongan',$id_lowongan)->firstOrFail();

        $jurusan_ar = explode(', ', $target->nama_jurusan);
        return view('backend.lowongan.update',compact('target','list_perusahaan','list_jurusan','list_jenjang','list_jenis_pekerjaan', 'jurusan_ar'));

    }

    public function update(Request $request){
        
        $jurusan_ar = implode(', ', $request->nama_jurusan);
        $request->request->add(['nama_jurusan' => $jurusan_ar]);
        $update = $this->lowonganObj->where('id_lowongan',$request->id_lowongan)->update($request->except('files','_token'));
        Ghelp::pesan($update, '==', true, 'Data lowongan berhasil diupdate', 'Gagal lowongan data perusahaan');

        return redirect()->action('backend\LowonganC@tampil');
    }


    public function hapus(Request $request)
    {
       $hapus = $this->lowonganObj->where('id_lowongan',$request->id_lowongan)->delete();
    }
}
