<?php

namespace App\Http\Controllers\backend;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Helpers\GlobalHelper as Ghelp;


class HomeC extends Controller
{
	public function __construct()
	{
	   
	}

	public function showLogin()
	{
	    return view('frontend.login');
	}

	public function loginAction(Request $request)
	{
	    if (Auth::attempt(['username' => $request->trigger1, 'password' => $request->trigger2])) {

	    	$user = Auth::user();
	    	if($user['is_active'] == 'confirm'){
	    		if($user['level'] == 'admin'){
	    			return redirect()->action('backend\DashboardC@tampil');
	    		}
	    		else if($user['level'] == 'alumni'){

	    			return redirect()->action('backend\DashboardC@tampilAlumni');
	    		}else{
	    			return redirect()->action('backend\DashboardC@tampilAnggota');

	    		}
	    		
	    	}else{
	    		\Session::flash('flash_message', array('pesan' => 'Akun anda masih dalam proses validasi oleh administrator', 'tipe' => 'danger'));
	        return back()->withInput();
	    	}

	       
	    } else {
	        \Session::flash('flash_message', array('pesan' => 'Kombinasi username dan password salah', 'tipe' => 'danger'));
	        return back()->withInput();
	    }
	}

	public function logout(Request $request)
	{
	    Auth::guard()->logout();
	    $request->session()->flush();
	    $request->session()->regenerate();
	    \Session::flash('flash_message', array('pesan' => 'Anda telah keluar dari sistem', 'tipe' => 'info'));
	    return redirect()->action('backend\HomeC@showLogin');
	}

	public function registerAnggota(){


		return view('frontend.registerAnggota');
	}

	public function storeAnggota(Request $request){

		$request->request->add([
							'password' => bcrypt($request->password),
							'level' => 'anggota',
								] );

		$validator = Validator::make($request->all(), [
  			  'mathcaptcha' => 'required|mathcaptcha',
            
        ]);

        if ($validator->fails()) {
        	app('mathcaptcha')->reset();
        	return back()
          			 ->withErrors($validator)
                     ->withInput();
        }else{

        	

		
        $simpan = \App\User::create($request->except(['passwordc','_token','mathcaptcha']));
        Ghelp::pesan($simpan, '==', true, '<strong>Pendaftaran berhasil </strong>, silahkan tunggu validasi data oleh administrator! Validasi data dilakukan paling lambat <i> 2 x 24 jam </i>. Silahkan hubungi administrator jika data belum divalidasi dalam kurun waktu tersebut.', 'Gagal melakukan pendaftaran');
        	
        	return back();
        }


		   

	}

	public function registerAlumni(){
		return view('frontend.registerAlumni');
	}


	public function storeAlumni(Request $request){
		
		$request->request->add(['password' => bcrypt($request->password)] );


		$validator = Validator::make($request->all(), [
  			  'mathcaptcha' => 'required|mathcaptcha',
            
        ]);

         if ($validator->fails()) {
        	app('mathcaptcha')->reset();
        	return back()
          			 ->withErrors($validator)
                     ->withInput();
        }else{

			$simpanAlumni = \App\Alumni::create($request->except(['mulai_bekerja','selesai_bekerja','mathcaptcha','nama_pekerjaan','nama_instansi','alamat_instansi','latitude','longitude','passwordc']));

			if($simpanAlumni){
				$simpanPekerjaan = \App\PekerjaanALumni::create([
					'nim' => $request->nim,
					'nama_pekerjaan' => $request->nama_pekerjaan,
					'nama_instansi' => $request->nama_instansi,
					'alamat_instansi' =>$request->alamat_instansi,
					'latitude' => $request->latitude,
					'longitude' => $request->longitude,
					'mulai_bekerja' => $request->mulai_bekerja,
					'selesai_bekerja' => $request->selesai_bekerja,

				]);
			if($simpanPekerjaan){
				$simpanAnggota = \App\User::create([
					'username' => $request->username,
					'email' => $request->email,
					'nama_lengkap' => $request->nama_lengkap,
					'tgl_lahir' => $request->tgl_lahir,
					'jk' => $request->jk,
					'no_telp' => $request->no_telp,
					'no_selular' => $request->no_selular,
					'tempat_lahir' => $request->tempat_lahir,
					'password' => $request->password,
					'id_equal' => $request->nim,
					'level' => 'alumni',

				]);
			}
				
			}

			 Ghelp::pesan($simpanAnggota, '==', true, '<strong>Pendaftaran berhasil </strong>, silahkan tunggu validasi data oleh administrator! Validasi data dilakukan paling lambat <i> 2 x 24 jam </i>. Silahkan hubungi administrator jika data belum divalidasi dalam kurun waktu tersebut.', 'Gagal melakukan pendaftaran');
	        return back();

	    }
	}
}
