<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Helpers\GlobalHelper as Ghelp;
use DataTables;
use App\Helpers\MyFile;


class LspC extends Controller
{
	private $exludeFields = array();
    private $lspObj;
    private $imageObj;

    public function __construct()
    {
        $this->lspObj = new \App\Lsp;
        $this->imageObj = new \App\Helpers\MyImage;

        $this->exludeFields = [
            '_token',
            'id_lsp',
            'upload_image',
        ];
    }

    public function getData()
    {
        $table = $this->lspObj::select('lsp.*')
                       ->orderBy('lsp.created_at','DSC');

        return Datatables::of($table)->addColumn('action', function ($table) {
            $editLink = action('backend\LspC@edit', [$table->id_lsp]);
            return '<a href="'.$editLink.'" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i>&nbsp;Ubah </a>&nbsp;<button class="btn btn-danger btn-xs" id="deleteUser" data-remote="'.$table->id_lsp.':;'.$table->nama_lsp.'"><i class="fa fa-close"></i>&nbsp;Hapus</button>';
        })
        ->addIndexColumn()
        ->addColumn('image', function ($table) {
            if($table->image == null){
                return '<label class="label label-warning">Tidak ada gambar</label>';
            }else{
                return '<img height="50px" src="'.url('img').'/'.$table->image.'">';
            }
        })
        ->addColumn('desc_word',function($table){
            return   str_limit(strip_tags($table->description_lsp,150));
        })
        ->addColumn('updated_at', function($table){
            return \Carbon\Carbon::parse($table->updated_at)->diffForHumans();
        })
        ->rawColumns(['description_lsp', 'image', 'updated_at','action'])
        ->make(true);
    }

    public function tampil()
    {
        return view('backend.lsp.index');
    }

    public function simpan(Request $request)
    {
        if ($request->hasFile('upload_image')) {
            $saveImage = $this->imageObj::saveImage($request->file('upload_image'), $request->nama_lsp);
            $request['image'] = $saveImage;
        }
        $request['slug'] = str_slug($request->nama_lsp);

        $userLogin = User::getLoginInfo();
        $request['user_id'] = $userLogin['user_id'];

        $simpan = $this->lspObj->create($request->except($this->exludeFields));
        Ghelp::pesan($simpan, '==', true, 'Data lsp berhasil disimpan', 'Gagal menyimpan data lsp');
        return back();
    }

    public function edit($id){
        $target = $this->lspObj->findOrFail($id);
        return view('backend.lsp.update',compact('target'));
    }

    public function update(Request $request){
        $target = $this->lspObj::findOrFail($request->id_lsp);

        $userLogin = User::getLoginInfo();
        $request['user_id'] = $userLogin['user_id'];
        $request['slug'] = str_slug($request->nama_lsp);

        if ($request->hasFile('upload_image')) {
            if ($target->image != NULL) {
                $this->imageObj::deleteImage($target->image);
            }
            $saveImage = $this->imageObj::saveImage($request->file('upload_image'), $request->nama_lsp);
            $request['image'] = $saveImage;
        }

        $update = $target->update($request->except($this->exludeFields));
        Ghelp::pesan($update, '==', true, 'Data lsp berhasil diupdate', 'Gagal mengupdate data lsp');
        return redirect()->action('backend\LspC@tampil');
    }

    public function hapus(Request $request)
    {
       $target = $this->lspObj->findOrFail($request->trigger);
       if($target->image != null || $target != ''){
           $this->imageObj::deleteImage($target->image);
       }
       $hapus = $target->delete();
    }

    //pendaftran LSP backend

    public function getLspdiIkuti(){

        $user = \App\User::getLoginInfo();
        $table = $this->lspObj::join('pendaftar_lsp','pendaftar_lsp.id_lsp','=','lsp.id_lsp')
                       ->join('users','users.id','=','pendaftar_lsp.id_anggota')
                       ->select(\DB::raw('pendaftar_lsp.*,lsp.nama_lsp,lsp.id_kategori'))
                       ->orderBy('pendaftar_lsp.created_at','DSC')
                       ->where('pendaftar_lsp.id_anggota',$user['user_id'])->get();
        return Datatables::of($table)->addColumn('action', function ($table) {
            $editLink = action('backend\LspC@detailLSPdiIkuti', [$table->id_pendaftar_lsp]);
            return '<a href="'.$editLink.'" class="btn btn-info btn-xs"><i class="fa fa-file"></i>&nbsp;Detail </a>&nbsp;
                <button class="btn btn-danger btn-xs" id="deleteUser" data-remote="'.$table->id_pendaftar_lsp.':;'.$table->nama_lsp.'"><i class="fa fa-close"></i>&nbsp;Batal Daftar</button>';
        })
        ->addIndexColumn()
      
        ->addColumn('tanggal_pendaftaran', function($table){
             return \Carbon\Carbon::parse($table->created_at)->format('d M Y H:i:s');
        })

        ->addColumn('status',function($table){
            if($table->status_pembayaran == 'Belum Bayar'){
                return '<label class="label label-warning">'.$table->status_pembayaran.'</label>';
            }else if($table->status_pembayaran == 'Menunggu Validasi'){
                return '<label class="label label-info">'.$table->status_pembayaran.'</label>';
            }else{
                return '<label class="label label-success">'.$table->status_pembayaran.'</label>';

            }

        })
        ->rawColumns(['action','tanggal_pendaftaran','status'])
        ->make(true);
    }

    public function tampilLSPdiIkuti(){

        return view('backend.lsp.indexPendaftaran');
    }

    public function detailLSPdiIkuti($id){

        $lsp = \App\PendaftarLsp::join('lsp','lsp.id_lsp','=','pendaftar_lsp.id_lsp')
                                    ->select(\DB::raw('lsp.nama_lsp, lsp.description_lsp, lsp.harga,lsp.harga_pendaftaran, lsp.id_kategori, pendaftar_lsp.*'))
                                    ->where('id_pendaftar_lsp',$id)
                                    ->firstOrFail();
        return view('backend.lsp.detailPendaftarLsp',compact('lsp'));
    }


    public function uploadBukti(Request $request){
        $myfile = new MyFile();

        $pendaftar_lsp = \App\PendaftarLsp::where('id_pendaftar_lsp',$request->id_pendaftar_lsp)->firstOrFail();

        

        if($request->has('nama_file')){
            
            if($pendaftar_lsp->nama_file != NULL){

                 $myfile->deleteFile($pendaftar_lsp->nama_file);

            }
           
            $data['nama_file'] = $myfile->saveFile($request->file('nama_file'), $request->nama_file->getClientOriginalName());
            $data['status_pembayaran'] = 'Menunggu Validasi';

        }

        $update = \App\PendaftarLsp::where('id_pendaftar_lsp',$request->id_pendaftar_lsp)->update($data);

        Ghelp::pesan($update, '==', true, 'File Bukti Transfer berhasil disimpan', 'Gagal menyimpan data File Bukti Transfer');
        return back();


    }

    public function batalDaftar(Request $request){

        $target = \App\PendaftarLsp::where('id_pendaftar_lsp',$request->trigger);

        $pendaftar = $target->firstOrFail();
        if($pendaftar->nama_file != NULL){
            $myfile = new MyFile();
            $myfile->deleteFile($pendaftar->nama_file); 
        }

        $target->delete();

       


    }

    public function getPendaftar(){

        
        $table = $this->lspObj::join('pendaftar_lsp','pendaftar_lsp.id_lsp','=','lsp.id_lsp')
                       ->join('users','users.id','=','pendaftar_lsp.id_anggota')
                       ->select(\DB::raw('pendaftar_lsp.*,lsp.nama_lsp,lsp.id_kategori,users.nama_lengkap, users.email'))
                       ->orderBy('pendaftar_lsp.created_at','DSC')
                       ->get();
        return Datatables::of($table)->addColumn('action', function ($table) {
            $editLink = action('backend\LspC@detailBerkasTransfer', [$table->id_pendaftar_lsp]);
            return '<a href="'.$editLink.'" class="btn btn-info btn-xs"><i class="fa fa-file"></i>&nbsp;Detail </a>&nbsp;
                <button class="btn btn-danger btn-xs" id="deleteUser" data-remote="'.$table->id_pendaftar_lsp.':;'.$table->nama_lsp.'"><i class="fa fa-close"></i>&nbsp;Batal Daftar</button>';
        })
        ->addIndexColumn()
      
        ->addColumn('tanggal_pendaftaran', function($table){
             return \Carbon\Carbon::parse($table->created_at)->format('d M Y H:i:s');
        })

        ->addColumn('status',function($table){
            if($table->status_pembayaran == 'Belum Bayar'){
                return '<label class="label label-warning">'.$table->status_pembayaran.'</label>';
            }else if($table->status_pembayaran == 'Menunggu Validasi'){
                return '<label class="label label-info">'.$table->status_pembayaran.'</label>';
            }else{
                return '<label class="label label-success">'.$table->status_pembayaran.'</label>';

            }

        })
        ->rawColumns(['action','tanggal_pendaftaran','status'])
        ->make(true);

    }

    // admin controller

    public function tampilPendaftar(){

        return view('backend.lsp.pendaftarLsp');
    }

    // ubah status pendaftar
    public function detailBerkasTransfer($id){


        $lsp = \App\PendaftarLsp::join('users','users.id','=','pendaftar_lsp.id_anggota')
                                ->join('lsp','lsp.id_lsp','=','pendaftar_lsp.id_lsp')
                                ->where('id_pendaftar_lsp',$id)
                                ->firstOrFail();
        return view('backend.lsp.detailBerkasTransfer',compact('lsp'));
    }

    public function updateStatusBerkas(Request $request){
        $update = \App\PendaftarLsp::where('id_pendaftar_lsp',$request->id_pendaftar_lsp)->update([
                'status_pembayaran' => $request->status_pembayaran
        ]);
        Ghelp::pesan($update, '==', true, 'Data Status Pembayaran berhasil diupdate', 'Gagal update status pembayaran');
        return back();

    }


}
