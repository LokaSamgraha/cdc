<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\MyImage;
use App\Helpers\GlobalHelper as Ghelp;
use DataTables;
use App\Informasi;
use App\User;

class InformasiC extends Controller
{
	private $exceptFields = array();
    private $informasiObj;

    public function __construct()
    {
        $this->informasiObj = new Informasi;

        $this->exceptFields = [
            '_token',
            'id_informasi',
        ];
    }

    public function getData()
    {
        
        $table = Informasi::select(\DB::raw('informasi.*'));
        return Datatables::of($table)->addColumn('action', function ($table) {

          return '<a href="informasi/'.$table->id_informasi.'/edit" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i>&nbsp;Ubah </a>&nbsp;<button class="btn btn-danger btn-xs" id="deleteUser" data-remote="'.$table->id_informasi.':;'.$table->title.'"><i class="fa fa-close"></i>&nbsp;Hapus</button>';
        })
         ->addIndexColumn()
         ->addColumn('link_title', function($table){
          return '<a href="informasi/'.$table->id_informasi.'/edit">'.$table->title.'</a>';
        })


        ->addColumn('create_at', function($table){
          return \Carbon\Carbon::parse($table->created_at)->toDayDateTimeString();
        })
         ->rawColumns(['link_title','create_at','action'])
        ->make(true);
    }

    public function tampil()
    {
        return view('backend.informasi.index');
    }


    public function simpan(Request $request)
    {
        $user = User::getLoginInfo();

        if($request->has('image')){

            $myimage = new MyImage();
            $data['image'] = $myimage->saveImage($request->file('image'), $request->title);
        }

        $data['id_kategori']  = $request->id_kategori;
        $data['title']  = $request->title;
        $data['description'] = $request->description;
        $data['slug'] = str_slug($request->title);
        $data['user_id'] = $user['user_id'];
        $simpan = $this->informasiObj->create($data);
        Ghelp::pesan($simpan, '==', true, 'Data informasi berhasil disimpan', 'Gagal menyimpan data informasi');
        return back();
    }

    public function edit($id_informasi){
        $target = $this->informasiObj->where('id_informasi',$id_informasi)->firstOrFail();

        return view('backend.informasi.update',compact('target'));

    }

    public function update(Request $request){

        $user = User::getLoginInfo();
        
        $informasiFilter = $this->informasiObj->where('id_informasi', $request->id_informasi)->first();


        if ($request->hasFile('image')) {
            $myimage = new MyImage();
            if ($informasiFilter->image != NULL) {
                $myimage->deleteImage($informasiFilter->image);
            }
            $data['image'] = $myimage->saveImage($request->file('image'), $request->title);
        }


        $data['id_kategori']  = $request->id_kategori;
        $data['title']  = $request->title;
        $data['description'] = $request->description;
        $data['slug'] = str_slug($request->title);
        $data['user_id'] = $user['user_id'];

        $update = $this->informasiObj->where('id_informasi',$request->id_informasi)->update($data);
        Ghelp::pesan($update, '==', true, 'Data informasi berhasil diupdate', 'Gagal mengupdate data informasi');

        return redirect()->action('backend\InformasiC@tampil');
    }

    public function hapus(Request $request)
    {
       $hapus = $this->informasiObj->where('id_informasi',$request->id_informasi)->delete();

    }
    
}
