<?php

namespace App\Http\Controllers\backend;


use App\Helpers\GlobalHelper as Ghelp;
use App\Helpers\MyImage;

use App\Http\Controllers\Controller;
use DataTables;
use Illuminate\Http\Request;

class PerusahaanC extends Controller
{
    private $exceptFields = array();
    private $perusahaanObj;
    private $globalHelperObj;

    public function __construct()
    {
        $this->perusahaanObj   = new \App\Perusahaan;
        $this->globalHelperObj = new \App\Helpers\GlobalHelper;

        $this->exceptFields = [
            '_token',
            'id_perusahaan',
        ];
    }

    public function getData()
    {
        $no= 1 ;
        $table = $this->perusahaanObj->get();
        return DataTables::of($table)
            ->addIndexColumn()
            ->addColumn('logo', function ($table) {
                  return '<img height="50px" src="'.url('img').'/'.$table->image.'">';
                })
            ->addColumn('action', function ($table) {

                  return '<a href="perusahaan/'.$table->id_perusahaan.'/edit" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i>&nbsp;Ubah </a>&nbsp;<button class="btn btn-danger btn-xs" id="deleteUser" data-remote="'.$table->id_perusahaan.','.$table->name.'"><i class="fa fa-close"></i>&nbsp;Hapus</button>';
                })
            ->addColumn('ket_no_html',function($table){
                return str_limit(strip_tags($table->description),100);

            })

           
            ->rawColumns(['logo','action','ket_no_html'])
            ->make(true);
    }

    public function tampil()
    {
        return view('backend.perusahaan.index');
    }

    public function simpan(Request $request)
    {

        
        if($request->has('image')){

            $myimage = new MyImage();
            $data['image'] = $myimage->saveImage($request->file('image'), $request->name);
        }


        $data['name']  = $request->name;
        $data['description'] = $request->description;

        $simpan = $this->perusahaanObj->create($data);
        Ghelp::pesan($simpan, '==', true, 'Data perusahaan berhasil disimpan', 'Gagal menyimpan data perusahaan');

        return back();
    }

    public function edit($id_perusahaan){
        $target = $this->perusahaanObj->where('id_perusahaan',$id_perusahaan)->firstOrFail();

        return view('backend.perusahaan.update',compact('target'));

    }

    public function update(Request $request){
        
        $perusahanFilter = $this->perusahaanObj->where('id_perusahaan', $request->id_perusahaan)->first();


        if ($request->hasFile('image')) {
            $myimage = new MyImage();
            if ($perusahanFilter->image != NULL) {
                $myimage->deleteImage($perusahanFilter->image);
            }
            $data['image'] = $myimage->saveImage($request->file('image'), $request->name);
        }


        $data['name']  = $request->name;
        $data['description'] = $request->description;

        $update = $this->perusahaanObj->where('id_perusahaan',$request->id_perusahaan)->update($data);
        Ghelp::pesan($update, '==', true, 'Data perusahaan berhasil diupdate', 'Gagal mengupdate data perusahaan');

        return redirect()->action('backend\PerusahaanC@tampil');
    }

    public function hapus(Request $request)
    {

       $hapus = $this->perusahaanObj->where('id_perusahaan',$request->id_perusahaan)->delete();

    }
}
