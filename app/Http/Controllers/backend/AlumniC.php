<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\GlobalHelper as Ghelp;
use DataTables;
use App\Fakultas;
use App\Jurusan;

use Config;

class AlumniC extends Controller
{
    private $exceptFields = array();
    private $alumniObj;

    public function __construct()
    {
        $this->alumniObj = new \App\Alumni;

        $this->exceptFields = [
            '_token',
            'id_alumni',
        ];
    }

    public function getData()
    {
        $table = $this->alumniObj
        ->join('jurusan','jurusan.id_jurusan','alumni.id_jurusan')
		->select(\DB::raw('alumni.*,jurusan.nama as "nama_jurusan"'))
        ->orderBy('alumni.created_at','DSC')
        ->get();
        return DataTables::of($table)
            ->addIndexColumn()
            
            ->addColumn('action', function ($table) {

                  return '<a href="alumni/'.$table->id_alumni.'/edit" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i>&nbsp;Ubah </a>&nbsp;<button class="btn btn-danger btn-xs" id="deleteUser" data-remote="'.$table->id_alumni.','.$table->nama_lengkap.'"><i class="fa fa-close"></i>&nbsp;Hapus</button>';
                })

            ->addColumn('tanggal_lulus', function ($table) {
                 return \Carbon\Carbon::parse($table->tgl_lulus)->format('d M Y');

            })
            ->addColumn('status', function ($table) {
            if($table->is_active == 'need confirm'){
                $status = '<label class="label label-danger">Need Confirm</label>';
            }else{
                $status = '<label class="label label-success">Confirm</label>';

            }

            return $status;

            })
            ->rawColumns(['action','status'])
            ->make(true);
    }

    public function tampil()
    {
        $list_kelamin = Config::get('dummy.jenis_kelamin');
        $list_agama = Config::get('dummy.agama');
        $list_jenjang = Config::get('dummy.jenjang');
        $list_jurusan = Jurusan::pluck('nama','id_jurusan');
        $list_konfirmasi = Config::get('dummy.konfirmasi');
        $user = \App\User::getLoginInfo();

        return view('backend.alumni.index',compact('list_jurusan','list_kelamin','list_agama','list_jenjang','list_konfirmasi','user'));
    }

    public function simpan(Request $request)
    {

        $request->request->add(['password' => bcrypt($request->password)] );


        $simpan = $this->alumniObj->create($request->all());

//create anggota 
        if($simpan){

            $simpanAnggota = \App\User::create([
                'id_equal'       => $request->nim,
                'username'       => $request->username,
                'password'       => bcrypt($request->password),
                'email'          => $request->email,
                'nama_lengkap'   => $request->nama_lengkap,
                'tgl_lahir'      => $request->tgl_lahir,
                'tempat_lahir'   => $request->tempat_lahir,
                'jk'             => $request->jk,
                'no_telp'        => $request->no_telp,
                'no_selular'     => $request->no_selular,
                'is_active'      => $request->is_active,
                'level'          => 'alumni'

            ]);

        }
        Ghelp::pesan($simpan, '==', true, 'Data alumni berhasil disimpan', 'Gagal menyimpan data alumni');
        return back();
    }

    public function edit($id_alumni){

        $list_kelamin = Config::get('dummy.jenis_kelamin');
        $list_agama = Config::get('dummy.agama');
        $list_jenjang = Config::get('dummy.jenjang');
        $list_jurusan = Jurusan::pluck('nama','id_jurusan');
        $list_konfirmasi = Config::get('dummy.konfirmasi');
    	$list_fakultas = Fakultas::pluck('nama','id_fakultas');
        $user = \App\User::getLoginInfo();




        $target = $this->alumniObj->where('id_alumni',$id_alumni)->firstOrFail();

        $pekerjaan = \App\PekerjaanAlumni::where('nim',$target->nim)->get();

        return view('backend.alumni.update',compact('target','list_fakultas','list_kelamin','list_agama','list_jenjang','list_jurusan','list_konfirmasi','user','pekerjaan'));

    }

    public function update(Request $request){
        $user = \App\User::getLoginInfo();

        if($request->password == '' || $request->password == ''){

           $except = ['password','id_alumni','_token'];
        }else{
            $data['password'] = bcrypt($request->password);
            $except = ['id_alumni','_token'];
        }

            $data['username']       = $request->username;
            $data['email']          = $request->email;
            $data['nama_lengkap']   = $request->nama_lengkap;
            $data['tgl_lahir' ]     = $request->tgl_lahir;
            $data['tempat_lahir']   = $request->tempat_lahir;
            $data['jk']             = $request->jk;
            $data['no_telp']        = $request->no_telp;
            $data['no_selular']     = $request->no_selular;
            

        $update = $this->alumniObj->where('id_alumni',$request->id_alumni)->update($request->except($except));

        if($user['level'] == 'admin'){

                if($update){
                    $data['is_active']      = $request->is_active;

                    // dd($data);
                    $simpanAnggota = \App\User::where('id_equal',$request->nim)->update($data);
            }
        }
        
        
        if($user['level'] == 'alumni'){
            \App\User::where('id_equal',$user['nim'])->update($data);
        } 

        //level anggota di userC 
        Ghelp::pesan($update, '==', true, 'Data alumni berhasil diupdate', 'Gagal mengupdate data alumni');

        return back();
    }

    public function hapus(Request $request)
    {
        
        $id_equal = \App\Alumni::where('id_alumni',$request->id_alumni)->firstOrFail();

        if($id_equal){
             $hapus = $this->alumniObj->where('id_alumni',$request->id_alumni)->delete();

        }
      
       if($hapus){

        if($id_equal == '' || $id_equal == NULL ){
            $hapusAnggota = \App\User::where('id_equal',$id_equal->nim)->delete();

        }
       }

    }

    public function tampilProfilAlumni(){

        $level = Config::get('dummy.level');
        $list_kelamin = Config::get('dummy.jenis_kelamin');
        $list_agama = Config::get('dummy.agama');
        $list_jenjang = Config::get('dummy.jenjang');
        $list_jurusan = \App\Jurusan::pluck('nama','id_jurusan');
        $list_konfirmasi = Config::get('dummy.konfirmasi');
        $user = \App\User::getLoginInfo();
        $target = \App\Alumni::where('nim',$user['nim'])->first();
        return view('backend.alumni.indexAlumni', compact('level','target','list_agama','list_jenjang','list_jurusan','list_konfirmasi','list_kelamin','user'));

    }

    public function getPekerjaan(){
        $user = \App\User::getLoginInfo();

        $table = $this->alumniObj
            ->join('pekerjaan_alumni','pekerjaan_alumni.nim','alumni.nim')
            ->select(\DB::raw('pekerjaan_alumni.*'))
            ->where('pekerjaan_alumni.nim',$user['nim'])
            ->get();
            return DataTables::of($table)
                ->addIndexColumn()
                ->addColumn('tahun',function($table){
                    if($table->selesai_bekerja == NULL){
                        $tahun_selesai = 'sekarang';
                     }else{
                        $tahun_selesai = $table->selesai_bekerja;
                     }

                     return $table->mulai_bekerja.' sampai '.$tahun_selesai;
                    
                })
                ->addColumn('action', function ($table) {

                  return '<a href="pekerjaan/'.$table->id_pekerjaan_alumni.'/edit" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i>&nbsp;Ubah </a>&nbsp;<button class="btn btn-danger btn-xs" id="deleteUser" data-remote="'.$table->id_pekerjaan_alumni.','.$table->nama_pekerjaan.'"><i class="fa fa-close"></i>&nbsp;Hapus</button>';
                })
                
                ->make(true);

    }

    public function riwayatPekerjaan(){
        $user = \App\User::getLoginInfo();


        return view('backend.alumni.riwayatPekerjaan',compact('user'));
    }

     public function hapusPekerjaan(Request $request)
    {
       $hapus = \App\PekerjaanAlumni::where('id_pekerjaan_alumni',$request->id_pekerjaan_alumni)->delete();

    }

    public function storePekerjaan(Request $request){

        $simpan = \App\PekerjaanAlumni::create($request->all());

        Ghelp::pesan($simpan, '==', true, 'Data Pekerjaan berhasil disimpan', 'Gagal mengupdate data disimpan');

        return back();

    }

    public function editPekerjaan($id){

        $target = \App\PekerjaanAlumni::where('id_pekerjaan_alumni',$id)->firstOrFail();

        return view('backend.alumni.updatePekerjaan',compact('target'));

    }

    public function updatePekerjaan(Request $request){
        $update = \App\PekerjaanAlumni::where('id_pekerjaan_alumni',$request->id_pekerjaan_alumni)
                 ->update($request->except('_token'));

        Ghelp::pesan($update, '==', true, 'Data Pekerjaan berhasil diupdate', 'Gagal mengupdate data diupdate');

        return back();

    }



}
