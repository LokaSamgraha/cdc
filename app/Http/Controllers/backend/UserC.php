<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\GlobalHelper as Ghelp;
use DataTables;

class UserC extends Controller
{
    private $exludeFields = array();
    private $userObj;
    private $userLogin;
    private $userLevel;

    public function __construct()
    {
        $this->userObj = new \App\User;
        $this->userLogin = $this->userObj::getLoginInfo();
        $this->userLevel = \Config::get('dummy.level');

        $this->exludeFields = [
            '_token',
            'id',
        ];
    }

    public function getData()
    {
        $table = $this->userObj::select('users.*')->where('level','anggota')->orderBy('created_at','DSC');
        return Datatables::of($table)->addColumn('action', function ($table) {
            $editLink = action('backend\UserC@edit', [$table->id]);
            return '<a href="'.$editLink.'" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i>&nbsp;Ubah </a>&nbsp;<button class="btn btn-danger btn-xs" id="deleteUser" data-remote="'.$table->id.':;'.$table->name.'"><i class="fa fa-close"></i>&nbsp;Hapus</button>';
        })
        ->addIndexColumn()
        ->addColumn('created_at', function($table){
          return \Carbon\Carbon::parse($table->updated_at)->format('H:i:s, d M Y ');
        })
        ->addColumn('status',function($table){
            if($table->is_active == 'need confirm'){
                $status = '<label class="label label-danger">Need Confirm</label>';
            }else{
                $status = '<label class="label label-success">Confirm</label>';

            }

            return $status;
        })
         ->rawColumns(['updated_at','action','status'])
        ->make(true);
    }

    public function tampil()
    {
        $user = \App\User::getLoginInfo();
        $level = $this->userLevel;

        return view('backend.user.index', compact('level','user'));
       

    }

    public function simpan(Request $request)
    {
        $request['password'] = bcrypt($request['password']);
        $simpan = $this->userObj->create($request->except($this->exludeFields));
        Ghelp::pesan($simpan, '==', true, 'Data user berhasil disimpan', 'Gagal menyimpan data user');
        return back();
    }

    public function edit($id){
        $user = \App\User::getLoginInfo();

        $target = $this->userObj->findOrFail($id);
        $level = $this->userLevel;
        return view('backend.user.update',compact('target', 'level','user'));
    }



    public function update(Request $request){

        if($request->password != '' || $request->password != NULL){
            $except = ['_token','id'];


        }else{
            $except = ['_token','id','password'];
        }

        $request->request->add([
                            'password' => bcrypt($request->password)
                                ] );
        
        
        $update = $this->userObj->where('id', $request->id)->update($request->except($except));
        Ghelp::pesan($update, '==', true, 'Data user berhasil diupdate', 'Gagal mengupdate data user');
        $user = \App\User::getLoginInfo();


        return back();

      

        
    }

    public function hapus(Request $request)
    {
       $hapus = $this->userObj->findOrFail($request->trigger)->delete();
    }

    //kelola admin
     public function getAdmin()
    {
        $table = $this->userObj::select('users.*')->where('level','admin')->orderBy('created_at','DSC');
        return Datatables::of($table)->addColumn('action', function ($table) {
            $editLink = action('backend\UserC@edit', [$table->id]);
            return '<a href="'.$editLink.'" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i>&nbsp;Ubah </a>&nbsp;<button class="btn btn-danger btn-xs" id="deleteUser" data-remote="'.$table->id.':;'.$table->name.'"><i class="fa fa-close"></i>&nbsp;Hapus</button>';
        })
        ->addIndexColumn()
        ->addColumn('created_at', function($table){
          return \Carbon\Carbon::parse($table->updated_at)->format('H:i:s, d M Y ');
        })
        ->addColumn('status',function($table){
            if($table->is_active == 'need confirm'){
                $status = '<label class="label label-danger">Need Confirm</label>';
            }else{
                $status = '<label class="label label-success">Confirm</label>';

            }

            return $status;
        })
         ->rawColumns(['updated_at','action','status'])
        ->make(true);
    }

    public function tampilAdmin(){
        $user = \App\User::getLoginInfo();
        $level = $this->userLevel;

        return view('backend.user.indexAdmin', compact('level','user'));
    }

    //controller alumni

    public function updatePassword(Request $request){

        $user = \App\User::getLoginInfo();

        $update = \App\User::where('id',$user['user_id'])->update([
            'password'=>bcrypt($request->password)

        ]);

        Ghelp::pesan($update, '==', true, 'Data Password berhasil diupdate', 'Gagal mengupdate data password');
        return back();

    }

    //anggota controller

    public function tampilProfilAnggota(){
        $user = \App\User::getLoginInfo();
        $target = \App\User::where('id',$user['user_id'])->firstOrFail();
        return view('backend.user.profilAnggota',compact('target','user'));
    }


}
