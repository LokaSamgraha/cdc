<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\GlobalHelper as Ghelp;
use DataTables;
use App\Kontak;

class KontakC extends Controller
{
    private $exceptFields = array();
    private $informasiObj;

    public function __construct()
    {
        $this->kontakObj = new Kontak;

        $this->exceptFields = [
            '_token',
            'id_konatk',
        ];
    }

    public function getData()
    {
        
        
    }

    public function tampil()
    {
        $kontak = $this->kontakObj->where('id_kontak',1)->firstOrFail();


        return view('backend.kontak.index',compact('kontak'));
    }

    public function edit($id_kontak){

    }

    public function update (Request $request){

        $update = $this->kontakObj->where('id_kontak',$request->id_kontak)->update($request->except($this->exceptFields));
        Ghelp::pesan($update, '==', true, 'Data Konatk berhasil diperbarui', 'Gagal menyimpan data kontak');
        return back();


        
    }

    public function simpan(Request $request)
    {
        $simpan = $this->informasiObj->create($request->except($this->exceptFields));
        Ghelp::pesan($simpan, '==', true, 'Data lowongan berhasil disimpan', 'Gagal menyimpan data lowongan');
        return back();
    }

    public function hapus(Request $request)
    {

    }
}
