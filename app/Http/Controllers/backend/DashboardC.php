<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardC extends Controller
{
    public function tampil()
    {
       $year_now = (int)\Carbon\Carbon::parse(\Carbon\Carbon::now())->format('Y') - 4 ;



       $minus = 1;
       for ($i=0; $i < 5 ; $i++) { 
           $year[] = $year_now;
           $year_now = $year_now + $minus;

       }

       foreach ($year as $key => $value) {
          $countLowongan[] =  \App\Lowongan::where('tgl_buka','like',$value.'%')->get()->count();
       }

       $year_js=json_encode($year);
       $lowongan_js=json_encode($countLowongan);

       
       $Cbursa = \App\Lowongan::count();
       $Calumni = \App\Alumni::count();
       $Canggota = \App\User::where('level','anggota')->get()->count();
       $Cperusahaan = \App\Perusahaan::count();
    	return view('backend.dashboard.index',compact('year_js','lowongan_js','Cbursa','Calumni','Canggota','Cperusahaan'));
    }

    public function tampilAlumni(){

        $login = \App\User::getLoginInfo();
	    $alumni = \App\Alumni::join('jurusan','jurusan.id_jurusan','=','alumni.id_jurusan')
	   						->where('alumni.nim',$login['nim'])->first();

    	return view('backend.dashboard.alumni',compact('alumni'));
    }

    public function tampilAnggota(){
        
        return view('backend.dashboard.anggota');

    }
}
