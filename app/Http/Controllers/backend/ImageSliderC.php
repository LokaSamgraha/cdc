<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\GlobalHelper as Ghelp;
use App\Helpers\MyImage;
use DataTables;

class ImageSliderC extends Controller
{
    

    public function __construct()
    {
        
    }

    public function getData()
    {
       
        $table = \App\ImageSlider::orderBy('created_at','ASC')->get();
        return DataTables::of($table)
            ->addIndexColumn()
            ->addColumn('logo', function ($table) {
                  return '<img height="50px" src="'.url('img').'/'.$table->image.'">';
                })
            ->addColumn('action', function ($table) {

                  return '<a href="kelola-image/'.$table->id_image_slider.'/edit" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i>&nbsp;Ubah </a>&nbsp;<button class="btn btn-danger btn-xs" id="deleteUser" data-remote="'.$table->id_image_slider.','.$table->title.'"><i class="fa fa-close"></i>&nbsp;Hapus</button>';
                })
            
            ->rawColumns(['logo','action'])
            ->make(true);
    }

    public function tampil()
    {
        return view('backend.image_slider.index');
    }

    public function simpan(Request $request)
    {

        
        if($request->has('image')){

            $myimage = new MyImage();
            $data['image'] = $myimage->saveImage($request->file('image'), $request->title);
        }


        $data['title']  = $request->title;
        $data['subtitle'] = $request->subtitle;
        $data['theme_color'] = $request->theme_color;

        $simpan = \App\ImageSlider::create($data);
        Ghelp::pesan($simpan, '==', true, 'Data Image Slider berhasil disimpan', 'Gagal menyimpan data image slider');

        return back();
    }

    public function edit($id_image_slider){
        $target =\App\ImageSlider::where('id_image_slider',$id_image_slider)->firstOrFail();

        return view('backend.image_slider.update',compact('target'));

    }

    public function update(Request $request){
        
        $imageFilter =\App\ImageSlider::where('id_image_slider',$request->id_image_slider)->first();


        if ($request->hasFile('image')) {
            $myimage = new MyImage();
            if ($imageFilter->image != NULL) {
                $myimage->deleteImage($imageFilter->image);
            }
            $data['image'] = $myimage->saveImage($request->file('image'), $request->title);
        }


        $data['title']  = $request->title;
        $data['subtitle'] = $request->subtitle;
        $data['theme_color'] = $request->theme_color;

        $update = \App\ImageSlider::where('id_image_slider',$request->id_image_slider)->update($data);
        Ghelp::pesan($update, '==', true, 'Data Image Slider berhasil diupdate', 'Gagal mengupdate data image slider');

        return redirect()->action('backend\ImageSliderC@tampil');
    }

    public function hapus(Request $request)
    {

       $imageFilter =\App\ImageSlider::where('id_image_slider',$request->id_image_slider)->first();

       $myimage = New MyImage();

       if ($imageFilter->image != NULL) {
                $myimage->deleteImage($imageFilter->image);

             $hapus = \App\ImageSlider::where('id_image_slider',$request->id_image_slider)->delete();

         }



    }
}
