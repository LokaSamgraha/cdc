<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FInfoC extends Controller
{
    public function index(){

    	$informasi = \App\Informasi::orderBy('created_at','DSC')->paginate(6);

        $lowongan = \App\Lowongan::orderBy('lowongan.created_at','DSC')->take(4)->get();

        $kategori = \App\kategori::where('jenis','informasi')->get();


    	return view('frontend.informasi',compact('informasi','kategori','lowongan'));
    }

    public function infoBy($id_kategori){
    	$informasi = \App\Informasi::where('id_kategori',$id_kategori)->orderBy('created_at','DSC')->paginate(6);

    	return view('frontend.informasiByKategori',compact('informasi'));
    }

    public function infoDetail($id_informasi,$slug){
    	$informasi = \App\Informasi::where('id_informasi',$id_informasi)->firstOrFail();
    	$kategori = \App\kategori::where('jenis','informasi')->get();
    	
    	return view('frontend.informasiDetail',compact('informasi','kategori'));


    }


}
