<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FBursaC extends Controller
{

	public function __construct(){
		\Carbon\Carbon::setLocale('id');
	}
    public function index(){
    	$bursa_kerja = \App\Lowongan::join('perusahaan','perusahaan.id_perusahaan','=','lowongan.id_perusahaan')->select(\DB::raw('lowongan.*, perusahaan.name, perusahaan.description'))->orderBy('lowongan.created_at','DSC')->paginate(10);

    	$datenow = \Carbon\Carbon::now()->format('Y-m-d');

    	

    	foreach ($bursa_kerja as $key => $value) {

    		$publish_date [$value->id_lowongan]  = \Carbon\Carbon::parse($value->created_at)->format('d M Y');
    		$count_posisi[$value->id_perusahaan] = \App\perusahaan::where('id_perusahaan',$value->id_perusahaan)->count();
    		if(($value->tgl_tutup == $datenow ) || ($value->tgl_tutup < $datenow )){
    			$tgl_tutup_human[$value->id_lowongan] = 'sudah ditutup';
    		}

    		else{
    			$tgl_tutup_human[$value->id_lowongan] = \Carbon\Carbon::parse($value->tgl_tutup)->diffForHumans();
    		}
    	}

    	$informasi = \App\Informasi::orderBy('created_at','DSC')->take(4)->get();

        $perusahaan=\App\Perusahaan::orderBy('created_at','DSC')->take(10)->get();

        $lowongan = \App\Lowongan::orderBy('lowongan.created_at','DSC')->take(4)->get();




    	return view('frontend.bursa',compact('bursa_kerja','tgl_tutup_human','count_posisi','publish_date','informasi','perusahaan','lowongan'));
    }

    public function detail($id_lowongan,$slug){
    	$lowongan = \App\Lowongan::join('perusahaan','perusahaan.id_perusahaan','=','lowongan.id_perusahaan')->where('id_lowongan',$id_lowongan)->select(\DB::raw('lowongan.*, perusahaan.name, perusahaan.description'))->firstOrFail();
    	$informasi = \App\Informasi::orderBy('created_at','DSC')->take(4)->get();

    	$datenow = \Carbon\Carbon::now()->format('Y-m-d');

        $publish_date  = \Carbon\Carbon::parse($lowongan->created_at)->format('d M Y');
    	$tutup_date  = \Carbon\Carbon::parse($lowongan->tgl_tutup)->format('d M Y');
    	$count_posisi = \App\perusahaan::where('id_perusahaan',$lowongan->id_perusahaan)->count();

    		if(($lowongan->tgl_tutup == $datenow ) || ($lowongan->tgl_tutup < $datenow )){
    			$tgl_tutup_human = 'sudah ditutup';
    		}

    		else{
    			$tgl_tutup_human= \Carbon\Carbon::parse($lowongan->tgl_tutup)->diffForHumans();
    		}

        $jurusan_ar = explode(', ', $lowongan->nama_jurusan);

        $lowonganTerkaitPerusahaan = \App\Lowongan::join('perusahaan','perusahaan.id_perusahaan','=','lowongan.id_perusahaan')->where('lowongan.id_perusahaan',$lowongan->id_perusahaan)->where('id_lowongan','!=',$lowongan->id_lowongan)->select(\DB::raw('lowongan.*, perusahaan.name, perusahaan.description'))->take(5)->get();
        $lowonganTerkaitJurusan = \App\Lowongan::join('perusahaan','perusahaan.id_perusahaan','=','lowongan.id_perusahaan')->whereIn('lowongan.nama_jurusan',$jurusan_ar)->where('lowongan.id_lowongan','!=',$lowongan->id_lowongan)->select(\DB::raw('lowongan.*, perusahaan.name, perusahaan.description'))->take(5)->get();



    	return view('frontend.bursa_detail',compact('lowongan','informasi','publish_date','count_posisi','tgl_tutup_human','tutup_date','lowonganTerkaitJurusan','lowonganTerkaitPerusahaan'));
    }
}
