<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;

class FAlumniC extends Controller
{
    public function index(){

    	
    	return view('frontend.alumni',compact('alumni'));
    }

    public function getData(){
    	$table = \App\Alumni::join('jurusan','jurusan.id_jurusan','=','alumni.id_jurusan')
    	->join('fakultas','fakultas.id_fakultas','=','jurusan.id_fakultas')
		->select(\DB::raw('alumni.*,jurusan.nama as "nama_jurusan",fakultas.nama as "nama_fakultas"'))
		->orderBy('alumni.created_at','DSC')
        ->get();
        return DataTables::of($table)
            ->addIndexColumn()
           
            ->addColumn('tanggal_lulus', function ($table) {
                 return \Carbon\Carbon::parse($table->tgl_lulus)->format('d M Y');
            })
            ->rawColumns(['action'])
            ->make(true);

    }
}
