<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FTracerStudyC extends Controller
{
    public function index()
    {

    	$tracer = \App\PekerjaanAlumni::join('alumni','alumni.nim','=','pekerjaan_alumni.nim')->select('nama_lengkap','alamat_instansi','nama_instansi','nama_pekerjaan','latitude','longitude')->get();
    	$a['map'] = $tracer;

    	$data = json_encode($tracer);

    	

    	return view('frontend.tracer',compact('data'));
    }
}
