<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\GlobalHelper as Ghelp;

class FLspC extends Controller
{
    public function index(){

    	$informasi = \App\Informasi::orderBy('created_at','DSC')->take(4)->get();

        $lowongan = \App\Lowongan::orderBy('lowongan.created_at','DSC')->take(4)->get();

        $kategori = \App\kategori::where('jenis','lsp')->get();

        $lsp = \App\Lsp::orderBy('created_at','DSC')->paginate(6);

    	return view('frontend.lsp',compact('lsp','informasi','kategori','lowongan'));
    }

    public function lspBy($id_kategori){
    	$lsp = \App\Lsp::where('id_kategori',$id_kategori)->orderBy('created_at','DSC')->paginate(6);

    	$informasi = \App\Informasi::orderBy('created_at','DSC')->take(4)->get();

        $lowongan = \App\Lowongan::orderBy('lowongan.created_at','DSC')->take(4)->get();

        $kategori = \App\kategori::where('jenis','lsp')->get();


    	return view('frontend.lspByKategori',compact('lsp','id_kategori','lowongan','informasi','kategori'));
    }

    public function lspDetail($id_lsp,$slug){
    	$lsp = \App\Lsp::where('id_lsp',$id_lsp)->firstOrFail();
    	$kategori = \App\kategori::where('jenis','lsp')->get();
    	
    	return view('frontend.lspDetail',compact('lsp','kategori'));


    }

    public function showDaftarDetail($id){

        $user = \App\User::getLoginInfo();

        $lsp = \App\lsp::where('id_lsp',$id)->firstOrFail();

        return view('frontend.daftarLspDetail',compact('lsp','user'));
    }

    public function storeDaftar(Request $request){

        $pendaftar_lsp = \App\PendaftarLsp::where([
            ['id_lsp' ,'=', $request->id_lsp],
            ['id_anggota' ,'=', $request->id_anggota]
        ])->get();

        $user = \App\User::getLoginInfo();

        if(sizeof($pendaftar_lsp) < 1){
            $simpan = \App\PendaftarLsp::create($request->all());
            Ghelp::pesan($simpan, '==', true, '<strong>Pendaftaran berhasil </strong>, silahkan melakukan pembayaran dan mengupload bukti tranfer di  <a href="'.url('dashboard/'.$user['level']).'"> <i>User Panel </i> </a> ', 'Gagal melakukan pendaftaran');
             return back();
        }else{
             Ghelp::pesan(false, '==', true, '<strong>Pendaftaran berhasil </strong>, silahkan melakukan pembayaran dan mengupload bukti tranfer di  <a href="'.url('dashboard/'.$user['level']).'"> <i>User Panel </i> <a> ', '<strong> Gagal melakukan pendaftaran </strong>. Anda sudah terdaftar pada LSP ini, silahkan lihat detail di <a href=""> <i> User Panel </i> </a>');
             return back();


        }

    }

}
