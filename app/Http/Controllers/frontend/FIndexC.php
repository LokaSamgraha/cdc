<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FIndexC extends Controller
{
    public function __construct(){
        \Carbon\Carbon::setLocale('id');
    }

     public function index()
    {
        $bursa_kerja = \App\Perusahaan::orderBy('perusahaan.created_at','DSC')->take(3)->get();
    	$Lsp = \App\Lsp::orderBy('lsp.created_at','DSC')->take(3)->get();

    	foreach ($bursa_kerja as $key => $value) {
    		$count_low[$value->id_perusahaan] = \App\Lowongan::where('id_perusahaan',$value->id_perusahaan)->count();
    	}


    	$informasi = \App\Informasi::orderBy('informasi.created_at','DSC')->take(4)->get();
    	$CountPerusahaan = \App\Perusahaan::count();
    	$CountLowongan = \App\Lowongan::count();
    	$CountAlumni = \App\Alumni::count();
    	$CountLsp = \App\Lsp::count();

        $lowongan = \App\Lowongan::orderBy('lowongan.created_at','DSC')->take(4)->get();

        $kategori = \App\kategori::where('jenis','informasi')->get();

        $kontak = \App\Kontak::where('id_kontak',1)->firstOrFail();


        $image_slider = \App\ImageSlider::orderBy('created_at','ASC')->get();

    	return view('frontend.index',compact('bursa_kerja','count_low','informasi','CountPerusahaan','CountLowongan','CountAlumni','CountLsp','kontak','Lsp','kategori','lowongan','image_slider'));
    }

    public function lowonganBy($id_perusahaan){

        $bursa_kerja = \App\Lowongan::join('perusahaan','perusahaan.id_perusahaan','=','lowongan.id_perusahaan')->where('lowongan.id_perusahaan',$id_perusahaan)->orderBy('lowongan.created_at','DSC')->paginate(10);
        $perusahaan = \App\Perusahaan::orderBy('perusahaan.created_at','DSC')->take(5)->get();

        $perusahaandata = \App\Perusahaan::where('id_perusahaan',$id_perusahaan)->firstOrFail();

        return view('frontend.bursa_by_perusahaan',compact('bursa_kerja','informasi','perusahaan','perusahaandata'));

    }


    
}
