<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fakultas extends Model
{
    protected $table = 'fakultas';
    protected $primarykey = 'id_fakultas';
    protected $guarded = [];
    protected $dates = [];
}
