<?php

namespace App\Helpers;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;

class MyFile
{
    public static function saveFile(UploadedFile $photo, $title)
    {
        $fileName = str_slug($title).'-at-cdc-undiksha-'.str_random(3).'.' . $photo->guessClientExtension();
        $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'files' ;
        $photo->move($destinationPath, $fileName);
        return $fileName;
    }

    public static function deleteFile($filename)
    {
        $path = public_path() . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR .$filename;
        return File::delete($path);
    }
}