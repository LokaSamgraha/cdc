<?php

namespace App\Helpers;

use Carbon\Carbon;
use GuzzleHttp\Client;

class GlobalHelper
{
    public static function salam()
    {
        $b    = time();
        $hour = date("g", $b);
        $m    = date("A", $b);

        $salam = '';

        if ($m == "AM") {
            if ($hour == 12) {
                $salam = "Selamat Malam, ";
            } elseif ($hour < 4) {
                $salam = "Selamat Malam, ";
            } elseif ($hour > 3) {
                $salam = "Selamat Pagi, ";
            }
        } elseif ($m == "PM") {
            if ($hour == 12) {
                $salam = "Selamat Siang, ";
            } elseif ($hour < 6) {
                $salam = "Selamat Sore, ";
            } elseif ($hour > 5) {
                $salam = "Selamat Malam, ";
            }
        }
        return $salam;
    }

    public static function pesan($value, $operation, $param, $success, $fail)
    {
        $result = array();
        switch ($operation) {
            case '==':
                if ($value == $param) {
                    $result['tipe']  = 'success';
                    $result['pesan'] = $success;
                } else {
                    $result['tipe']  = 'danger';
                    $result['pesan'] = $fail;
                }
                break;

            case '!=':
                if ($value != $param) {
                    $result['tipe']  = 'success';
                    $result['pesan'] = $success;
                } else {
                    $result['tipe']  = 'danger';
                    $result['pesan'] = $fail;
                }
                break;

            default:
                $result = null;
                break;
        }

        $session          = array();
        $session['pesan'] = \Session::flash('pesan', $result['pesan']);
        $session['tipe']  = \Session::flash('tipe', $result['tipe']);

        return $session;
    }

    public static function MakeDBId($yourTimestamp = null)
    {
        static $inc = 0;
        $id = '';

        if ($yourTimestamp === null) {
            $yourTimestamp = time();
        }
        $ts = pack('N', $yourTimestamp);
        $m = substr(md5(gethostname()), 0, 3);
        $pid = pack('n', getmypid());
        $trail = substr(pack('N', $inc++), 1, 3);
        $bin = sprintf("%s%s%s%s", $ts, $m, $pid, $trail);

        for ($i = 0; $i < 12; $i++) {
            $id .= sprintf("%02X", ord($bin[$i]));
        }

        return $id;
    }
}
