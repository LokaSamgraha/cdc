<?php

namespace App\Helpers;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;

class MyImage
{
    public static function saveImage(UploadedFile $photo, $title)
    {
        $fileName = str_slug($title).'-at-cdc-undiksha-'.str_random(3).'.' . $photo->guessClientExtension();
        $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img' ;
        $photo->move($destinationPath, $fileName);
        return $fileName;
    }

    public static function deleteImage($filename)
    {
        $path = public_path() . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR .$filename;
        return File::delete($path);
    }
}