<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageSlider extends Model
{
    protected $table = 'image_slider';
    protected $primarykey = 'id_image_slider';
    protected $guarded = [];
    protected $dates = [];
}
