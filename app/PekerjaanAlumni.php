<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PekerjaanAlumni extends Model
{
    protected $table = 'pekerjaan_alumni';
    protected $primarykey = 'id_pekerjaan_alumni';
    protected $guarded = [];
    protected $dates = [];
}
