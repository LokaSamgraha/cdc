<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jurusan extends Model
{
    protected $table = 'jurusan';
    protected $primarykey = 'id_jurusan';
    protected $guarded = [];
    protected $dates = [];
}
