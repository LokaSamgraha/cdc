<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alumni extends Model
{
    protected $table = 'alumni';
    protected $primarykey = 'id_alumni';
    protected $guarded = [];
    protected $dates = [];  
}
