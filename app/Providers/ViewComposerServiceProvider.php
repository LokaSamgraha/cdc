<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    public function boot()
    {
        view()->composer(["partials.backend.master", "backend.login","partials.frontend.front"], "App\Http\ViewComposer\SidebarViewComposer");
        view()->composer(["partials.frontend.front", "frontend.kontak", "frontend.daftarLspDetail"], "App\Http\ViewComposer\KontakFrontComposer");
    }

    public function register()
    {
        
    }
}
