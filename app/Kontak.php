<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kontak extends Model
{
    protected $table = 'kontak';
    protected $primarykey = 'id_kontak';
    protected $guarded = [];
    protected $dates = [];
}
