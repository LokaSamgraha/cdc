<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PendaftarLsp extends Model
{
    protected $table = 'pendaftar_lsp';
    protected $primarykey = 'id_pendaftar_lsp';
    protected $guarded = [];
    protected $dates = [];
}
