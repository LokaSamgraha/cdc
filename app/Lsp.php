<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lsp extends Model
{
    protected $table = 'lsp';
    protected $primaryKey = 'id_lsp';
    protected $guarded = [];
    protected $dates = [];
}
