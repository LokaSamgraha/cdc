<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Informasi extends Model
{
    protected $table = 'informasi';
    protected $primarykey = 'id_informasi';
    protected $guarded = [];
    protected $dates = [];
}
